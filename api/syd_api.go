// Start-up of the SYD API.
package main

import (
	"log"
	"net/http"
	"os"

	"gitlab.com/syd-eip/server-web/api/v1/router"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	serverConst "gitlab.com/syd-eip/server-web/api/v1/constant/server"
)

// Description :
//		Initialization and definition of the server :
//		- Definition of the server's port.
//		- Definition of the server's routes.
//		- Definition of the server's security cors.
//		- Definition of the server HTTP mod : "http" or "https".
func main() {
	serverPort := serverPortDefinition()
	serverMux := router.SetServerRoutes()
	serverCors := security.SetupCors()
	handler := serverCors.Handler(serverMux)

	serverLaunch(serverPort, handler)
}

// Description :
//		Definition of the server's port.
func serverPortDefinition() string {
	serverPort := os.Getenv(serverConst.ServerPort)
	if serverPort == "" {
		log.Fatal("Cannot run the server, no server port defined")
	}
	log.Println("Server run on server port: {" + serverPort + "}")

	return serverPort
}

// Description :
//		- Definition of the server's HTTP mod :
//			- "http" if the SslStatus is set to false.
//				- In that case, we just listen and serve the routes on the server port.
//			- "https" if the SslStatus is set to true.
//				- In that case we recover the SSL certificates for the security.
//				- We use them to listen and serve the routes on the server port.
func serverLaunch(serverPort string, handler http.Handler) {
	sslStatus := os.Getenv(serverConst.SslStatus)
	log.Println("SSL status: {" + sslStatus + "}")

	if sslStatus == "true" {
		key := os.Getenv(serverConst.SslKey)
		cert := os.Getenv(serverConst.SslCert)
		log.Println("Server trying to get SSL certificates: key{" + key + "} {" + cert + "}")

		if key == "" || cert == "" {
			log.Fatal("Cannot run server in HTTPS, no SSL certificates found")
		}

		log.Println("SSL certificates acquired")
		log.Println("Server run in HTTPS")

		if err := http.ListenAndServeTLS(serverPort, cert, key, handler); err != nil {
			log.Fatal(err)
		}
	} else {
		log.Println("Server run in HTTP")
		if err := http.ListenAndServe(serverPort, handler); err != nil {
			log.Fatal(err)
		}
	}
}
