package model

// This model represents the structure used to upload an image
type UploadRequestImage struct {
	Name         string `bson:"name"`
	Content      string `bson:"content"`
	CreationDate string `json:"creationDate"`
	UpdateDate   string `json:"updateDate"`
}
