// This package provides all the models used in SYD API.
package model

import (
	"gopkg.in/mgo.v2/bson"
)

// This model represents the structure stored in the "syd" database of an JwtToken.
type JwtToken struct {
	Id           bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Token        string        `json:"token"`
	CreationDate string        `json:"creationDate"`
	UpdateDate   string        `json:"updateDate"`
}
