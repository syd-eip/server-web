package model

import "gopkg.in/mgo.v2/bson"

// This model represents the structure stored in the "syd" database of a Purchase.
type Purchase struct {
	Id    bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Amout int64         `json:"wallet"`
}
