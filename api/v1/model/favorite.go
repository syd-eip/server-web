package model

import "gopkg.in/mgo.v2/bson"

// This model represents the structure stored in the "syd" database of a user's Favorite.
type Favorite struct {
	Tags         []string        `json:"tags"`
	Users        []bson.ObjectId `json:"users"`
	Associations []bson.ObjectId `json:"associations"`
}
