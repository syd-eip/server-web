package model

import "gopkg.in/mgo.v2/bson"

// This model represents the structure stored in the "syd" database of a Tag.
type Tag struct {
	Id           bson.ObjectId     `bson:"_id" json:"id,omitempty"`
	Name         string            `bson:"name" json:"name"`
	TotalEvent   int               `bson:"totalEvents" json:"totalEvents"`
	ActualEvent  int               `bson:"actualEvents" json:"actualEvents"`
	Images       map[string]string `bson:"images" json:"images"`
	CreationDate string            `json:"creationDate"`
	UpdateDate   string            `json:"updateDate"`
}

// This model represents the structure of a CreateTag send when a user wants to create a tag.
type TagRequestBody struct {
	Name string `json:"name"`
}
