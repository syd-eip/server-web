package model

import "gopkg.in/mgo.v2/bson"

// This model represents the structure stored in the "syd" database of a Participation to a lottery.
type Participation struct {
	UserId     bson.ObjectId `bson:"userId" json:"userId,omitempty"`
	Value      int64         `json:"value"`
	SplitRatio float32       `json:"splitRatio"`
}
