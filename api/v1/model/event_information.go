package model

/**
**   Format de date attendu : 2019-04-23T19:25:43Z
**   Norme de temps : time.RFC3339
**/

// This model represents the structure of events information stored in the "syd" database of an Event.
type EventInformation struct {
	Name        string   `bson:"name" json:"name"`
	DrawDate    string   `bson:"drawDate" json:"drawDate"`
	EventDate   string   `bson:"eventDate" json:"eventDate"`
	Tags        []string `bson:"tags" json:"tags"`
	Description string   `bson:"description" json:"description"`
}
