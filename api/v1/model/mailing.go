package model

// This model represents the structure of an email sent by SYD.
type MailingStructure struct {
	Subject           string   `json:"subject"`
	Body              string   `json:"body"`
	PolitenessFormula string   `json:"politenessFormula"`
	Signature         string   `json:"signature"`
	Receivers         []string `json:"receivers"`
}

// This model represents the structure of an email response when a mailing is done.
type MailingResponse struct {
	Mail    string `json:"mail"`
	Message string `json:"message"`
}
