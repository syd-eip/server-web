package model

// This model represents the structure stored in the "syd" database of a Lottery.
type Lottery struct {
	TotalValue			int64			`json:"totalValue"`
	Participations	[]Participation	`json:"participations"`
}
