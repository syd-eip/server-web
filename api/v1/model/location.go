package model

// This model represents the structure stored in the "syd" database of a Location.
type Location struct {
	Country       string `json:"country"`
	CountryCode   string `json:"countryCode"`
	City          string `json:"city"`
	ZipCode       string `json:"zipCode"`
	StreetAddress string `json:"streetAddress"`
}
