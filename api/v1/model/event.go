package model

import (
	"gopkg.in/mgo.v2/bson"
)

/**
**   Format de date attendu : 2019-04-23T19:25:43Z
**   Norme de temps : time.RFC3339
**/

// This model represents the structure stored in the "syd" database of an Event.
type Event struct {
	Id            bson.ObjectId     `bson:"_id" json:"id,omitempty"`
	UserId        bson.ObjectId     `bson:"userId" json:"userId,omitempty"`
	AssociationId bson.ObjectId     `bson:"associationId" json:"associationId,omitempty"`
	Information   EventInformation  `bson:"information" json:"information,omitempty"`
	LotteriesSize int               `bson:"lotteriesSize" json:"lotteriesSize"`
	Lotteries     []Lottery         `bson:"lotteries" json:"lotteries"`
	Images        map[string]string `bson:"images" json:"images"`
	CreationDate  string            `json:"creationDate"`
	UpdateDate    string            `json:"updateDate"`
}

type EventCreateEvent struct {
	Name          string   `bson:"name" json:"name"`
	Association   string   `json:"association"`
	DrawDate      string   `json:"drawDate"`
	EventDate     string   `json:"eventDate"`
	Description   string   `json:"description"`
	Tags          []string `bson:"tags" json:"tags"`
	LotteriesSize int      `json:"lotteriesSize"`
}

type EventDates struct {
	DrawDate  string `json:"drawDate"`
	EventDate string `json:"eventDate"`
}

type EventLotteriesSize struct {
	LotteriesSize int `json:"lotteriesSize"`
}

// This model represents the structure of a PatchEventName send when an Influencer or an Admin wants to update the name of an event
type EventName struct {
	Name string `bson:"name" json:"name"`
}

// This model represents the structure of a PatchEventDescription send when an Influencer or an Admin wants to update the descritpion of an event
type EventDescription struct {
	Description string `json:"description"`
}

// This model represents the structure of a PatchEventName send when an Influencer or an Admin wants to update the tags associated with an event
type EventTags struct {
	Tags []string `bson:"tags" json:"tags"`
}

// This model represents the structure of a AddBet send when an User wants to add a bet to an event
type AddBetRequestModel struct {
	LotteryId int   `bson:"lotteryId" json:"lotteryId"`
	Value     int64 `bson:"value" json:"value"`
}

// This model represents the structure of a AddBet send when an User wants to add a bet to an event
type EventIdModel struct {
	EventId string `bson:"eventId" json:"eventId"`
}
