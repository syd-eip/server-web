package model

import (
	"gopkg.in/mgo.v2/bson"
)

/**
**   Format de date attendu pour Birthday : 2019-04-23T19:25:43Z
**   Norme de temps : time.RFC3339
**/

// This model represents the structure stored in the "syd" database of a User.
type User struct {
	Id                 bson.ObjectId     `bson:"_id" json:"id,omitempty"`
	Role               []string          `json:"role"`
	FirstName          string            `json:"firstName"`
	LastName           string            `json:"lastName"`
	Username           string            `json:"username"`
	Email              string            `json:"email"`
	Password           string            `json:"password"`
	Birthday           string            `json:"birthday"`
	Phone              Phone             `json:"phone"`
	Location           Location          `json:"location"`
	CguValidated       bool              `json:"cguValidated"`
	WantToBeInfluencer bool              `json:"wantToBeInfluencer"`
	Wallet             int64             `json:"wallet"`
	WalletId           bson.ObjectId     `bson:"walletId,omitempty"`
	Favorites          Favorite          `json:"favorites"`
	OldEvents          []Event           `json:"oldEvents"`
	CurrentEvents      []Event           `json:"currentEvents"`
	Images             map[string]string `json:"images"`
	CreationDate       string            `json:"creationDate"`
	UpdateDate         string            `json:"updateDate"`
}

// This model represents the structure of the user's detailed data, when the user's detailed data are sent.
type UserDetailed struct {
	Id                 bson.ObjectId     `bson:"_id" json:"id,omitempty"`
	Role               []string          `json:"role"`
	FirstName          string            `json:"firstName"`
	LastName           string            `json:"lastName"`
	Username           string            `json:"username"`
	Email              string            `json:"email"`
	Birthday           string            `json:"birthday"`
	Phone              Phone             `json:"phone"`
	Location           Location          `json:"location"`
	CguValidated       bool              `json:"cguValidated"`
	WantToBeInfluencer bool              `json:"wantToBeInfluencer"`
	Wallet             int64             `json:"wallet"`
	Favorites          Favorite          `json:"favorites"`
	OldEvents          []Event           `json:"oldEvents"`
	CurrentEvents      []Event           `json:"currentEvents"`
	Images             map[string]string `json:"images"`
	CreationDate       string            `json:"creationDate"`
	UpdateDate         string            `json:"updateDate"`
}

// This model represents the structure of the user's public data, when the user's public data are sent.
type UserPublic struct {
	Id            bson.ObjectId     `bson:"_id" json:"id,omitempty"`
	Username      string            `json:"username"`
	Country       string            `json:"country"`
	Favorites     Favorite          `json:"favorites"`
	OldEvents     []Event           `json:"oldEvents"`
	CurrentEvents []Event           `json:"currentEvents"`
	Images        map[string]string `json:"images"`
	CreationDate  string            `json:"creationDate"`
	UpdateDate    string            `json:"updateDate"`
}

// This model represents the structure of the user's creation data.
// Sent when a user wants to register an account.
type UserSignUp struct {
	FirstName          string   `json:"firstName"`
	LastName           string   `json:"lastName"`
	Username           string   `json:"username"`
	Email              string   `json:"email"`
	Password           string   `json:"password"`
	Birthday           string   `json:"birthday"`
	Phone              Phone    `json:"phone"`
	Location           Location `json:"location"`
	CguValidated       bool     `json:"cguValidated"`
	WantToBeInfluencer bool     `json:"wantToBeInfluencer"`
}

// This model represents the structure of the user's authentication, when the user's credentials are sent.
type UserSignIn struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

// This model represents the structure of the user's name, when the user's names are sent.
type UserName struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

// This model represents the structure of the user's birthday, when the user's birthday is sent.
type UserBirthday struct {
	Birthday string `json:"birthday"`
}

// This model represents the structure of the user's username, when the user's username is sent.
type UserUsername struct {
	Username string `json:"username"`
}

// This model represents the structure of the user's email, when the user's email is sent.
type UserEmail struct {
	Email string `json:"email"`
}

// This model represents the structure of the user's password, when the user's password and confirm are sent.
type UserPassword struct {
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
}

// This model represents the structure of the user's roles, when the user's roles are sent.
type UserRole struct {
	Role string `json:"role"`
}

// This model represents the structure of the user's tokens modification when an amount of token is sent.
type UserUpdateToken struct {
	Value int64 `json:"value"`
}

// This model represents the structure of the user's wallet, when the user's waller is sent.
type UserWallet struct {
	Wallet int64 `json:"wallet"`
}
