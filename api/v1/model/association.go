// This package provides all the models used in SYD API.
package model

import (
	"gopkg.in/mgo.v2/bson"
)

// This model represents the structure stored in the "syd" database of an Association.
type Association struct {
	Id                   bson.ObjectId        `bson:"_id" json:"id,omitempty"`
	Name                 string               `json:"name"`
	Description          string               `json:"description"`
	Email                string               `json:"email"`
	Location             Location             `json:"location"`
	Phone                Phone                `json:"phone"`
	Images               map[string]string    `json:"images"`
	AssociationStatistic AssociationStatistic `json:"associationStatistic"`
	CreationDate         string               `json:"creationDate"`
	UpdateDate           string               `json:"updateDate"`
}

// This model represents the structure of the association's creation data.
// Sent when a SUPER ADMIN wants to register an association.
type AssociationSignUp struct {
	Name        string   `json:"name"`
	Description string   `json:"description"`
	Email       string   `json:"email"`
	Location    Location `json:"location"`
	Phone       Phone    `json:"phone"`
}

// This model represents the structure of the association's name.
// Sent when a SUPER ADMIN wants to update the association's name.
type AssociationName struct {
	Name string `json:"name"`
}

// This model represents the structure of the association's description.
// Sent when a SUPER ADMIN wants to update the association's description.
type AssociationDescription struct {
	Description string `json:"description"`
}

// This model represents the structure of the association's email.
// Sent when a SUPER ADMIN wants to update the association's email.
type AssociationEmail struct {
	Email string `json:"email"`
}

// This model represents the structure stored in the "syd" database of an association's statistics.
type AssociationStatistic struct {
	Gains       int64                 `json:"gains"`
	Events      int64                 `json:"events"`
	Influencers []InfluencerStatistic `json:"influencers"`
	Users       []UserStatistic       `json:"users"`
}

// This model represents the structure stored in the "syd" database of the influencers' statistics for an association.
type InfluencerStatistic struct {
	Name           string `json:"name"`
	EventsCreated  int64  `json:"eventsCreated"`
	CollectedMoney int64  `json:"collectedMoney"`
}

// This model represents the structure stored in the "syd" database of the users' statistics for an association.
type UserStatistic struct {
	Name               string `json:"name"`
	EventsParticipated int64  `json:"eventsParticipated"`
	GivenMoney         int64  `json:"givenMoney"`
}
