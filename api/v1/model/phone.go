package model

// This model represents the structure stored in the "syd" database of a Phone number.
type Phone struct {
	CountryCode         string `json:"countryCode"`
	NationalNumber      string `json:"nationalNumber"`
	InternationalNumber string `json:"internationalNumber"`
	Number              string `json:"number"`
}
