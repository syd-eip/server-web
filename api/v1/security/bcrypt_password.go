// This package provides the methods necessary to handle security on SYD api : token password, CORS...
package security

import (
	"errors"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"golang.org/x/crypto/bcrypt"
	"unicode"
)

// Description :
//		Check if the password has got : at least 8 characters or max 50, one lower case, one upper case, one special.
// Parameters :
//		s string :
//			Password to check.
// Returns :
//		error :
//			If the password is not valid, an error is returned.
func ValidPassword(s string) error {
	switch {
	case len(s) < 8:
		return errors.New(errormessage.PasswordMinLength)
	case len(s) > 50:
		return errors.New(errormessage.PasswordMaxLength)
	}
next:
	for _, classes := range map[string][]*unicode.RangeTable{
		"UpperCase": {unicode.Upper, unicode.Title},
		"LowerCase": {unicode.Lower},
		"Numeric":   {unicode.Number, unicode.Digit},
		"Special":   {unicode.Space, unicode.Symbol, unicode.Punct, unicode.Mark},
	} {
		for _, r := range s {
			if unicode.IsOneOf(classes, r) {
				continue next
			}
		}
		return errors.New(errormessage.PasswordComposition)
	}
	return nil
}

// Description :
//		Hash a password thanks to the bcrypt method.
// Parameters :
//		password string :
//			Password to hash.
// Returns :
//		string :
//			The hashed password.
//		error :
//			If the generation of the password failed, an error is returned.
func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// Description :
//		Compare a password and an hashed password stored in database.
// Parameters :
//		password string :
//			Raw password.
//		bddPassword string :
//			Hashed password stored in database.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the raw password and the stored password matched.
//			- false if not.
func CheckPasswordHash(password string, bddPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(bddPassword), []byte(password))
	return err == nil
}
