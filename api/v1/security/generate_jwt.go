package security

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/token"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Represents the signing key necessary to create the user's token.
var signingKey = []byte(os.Getenv(token.SigningKeyName))

// Represents the key necessary to create the reset password's token.
var passwordKey = []byte(os.Getenv(token.PasswordKeyName))

// Description :
//		This function generates a JWToken for the user's token.
// Parameters :
//		user model.User :
//			User used to generate JWToken for the user's token.
//			Data are taken and put in the token to be retrieved later in the claims.
// Returns :
//		string :
//			Generated JWToken.
//		error :
//			If the generation failed, an error is returned.
func GenerateJWTUserToken(user model.User) (string, error) {
	end := time.Now().Add(token.UserTokenExpire)

	key := jwt.New(jwt.SigningMethodHS256)
	claims := key.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["id"] = user.Id
	claims["role"] = user.Role
	claims["exp"] = end.Unix()

	tokenString, err := key.SignedString(signingKey)
	if err != nil {
		return "", err
	}
	tokenString = "Bearer " + tokenString

	return tokenString, nil
}

// Description :
//		This function generates a JWToken for the password's token.
// Parameters :
//		user model.User :
//			User used to generate JWToken for the password's token.
//			Data are taken and put in the token to be retrieved later in the claims.
// Returns :
//		string :
//			Generated JWToken.
//		error :
//			If the generation failed, an error is returned.
func GenerateJWTPasswordToken(user model.User) (string, error) {
	end := time.Now().Add(token.PasswordTokenExpire)

	key := jwt.New(jwt.SigningMethodHS256)
	claims := key.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["id"] = user.Id
	claims["email"] = user.Email
	claims["exp"] = end.Unix()

	tokenString, err := key.SignedString(passwordKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}
