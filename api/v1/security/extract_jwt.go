package security

import (
	"errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/constant/cookie"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
	tokenConst "gitlab.com/syd-eip/server-web/api/v1/constant/token"
)

func ExtractJwt(r *http.Request) (*jwt.Token, error) {
	var head string

	if r.Header[headerConst.Authorization] != nil {
		head = r.Header[headerConst.Authorization][0]
	} else {
		c, err := r.Cookie(cookie.CookieTokenUserName)
		if err != nil {
			return nil, errors.New("MISSING_TOKEN")
		}
		head = c.Value
	}
	key := strings.Split(head, " ")
	if len(key) != 2 {
		return nil, errors.New("BAD_TOKEN")
	}
	token, err := jwt.Parse(key[1], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("PARSING_FAILED")
		}
		return tokenConst.SigningKey, nil
	})

	return token, err
}
