package security

import (
	"log"
	"net/http"
	"os"

	"github.com/rs/cors"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
	servconst "gitlab.com/syd-eip/server-web/api/v1/constant/server"
)

// Description :
//		This method setup the Allowed origins, Methods, Headers depending on the status of the environment DEV.
//		If DEV is true, we are in development so the API is accessible by localhost:8081 and the debug is activated.
//		If DEV is false, we aren't in development so the API is accessible by www.shareyourdreams.fr.
// Returns :
//		*cors.Cors :
//			These are the configured Cors setup for the API.
func SetupCors() *cors.Cors {
	log.Println("Server setting up CORS")

	origins := []string{"https://www.shareyourdreams.fr*", "https://shareyourdreams.fr*"}
	debug := false

	if os.Getenv(servconst.Dev) == "true" {
		log.Println("Environment of development is DEV, AllowedOrigins: ")
		log.Println(origins)

		origins = []string{"http://local.shareyourdreams.fr:8082*", "https://qualif.shareyourdreams.fr"}
		debug = true
	} else {
		log.Println("Environment of development is not DEV, AllowedOrigins: ")
		log.Println(origins)
	}

	log.Println("-> If any issue with the CORS Policy, look in AllowedHeaders/ExposedHeaders of \"setup_cors.go\"")
	log.Println("CORS are set up")

	return cors.New(cors.Options{
		AllowedOrigins: origins,
		AllowedMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions},
		AllowedHeaders: []string{
			headerConst.XContentTypeOptions,
			headerConst.XXSSProtection,
			headerConst.XFrameOptions,
			headerConst.StrictTransportSecurity,
			headerConst.ContentType,
			headerConst.ContentLength,
			headerConst.AcceptEncoding,
			headerConst.XCSRFToken,
			headerConst.Origin,
			headerConst.OriginRequestFront,
			headerConst.ResetToken,
			headerConst.Accept,
			headerConst.Authorization,
			headerConst.XRequestedWith,
			headerConst.DNT,
			headerConst.XCustomHeader,
			headerConst.KeepAlive,
			headerConst.UserAgent,
			headerConst.IfModifiedSince,
			headerConst.CacheControl,
			headerConst.ContentRange,
			headerConst.Range,
		},
		ExposedHeaders: []string{
			headerConst.ContentType,
		},
		AllowCredentials:   true,
		OptionsPassthrough: true,
		Debug:              debug,
	})
}
