// This package provides functions to convert a response into a specific format.
package response

import (
	"encoding/json"
	"net/http"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
)

// This structure represents the format of every response sent to by the API.
type ResponseStruct struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// Description :
//		Write a JSON response with the ResponseWriter.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		message string :
//			Represents the message include in the response.
//		status int :
//			Represents the HTTP Status of the response.
//		payload interface{} :
//			Interface include in the response.
func RespondWithJson(w http.ResponseWriter, status int, message string, payload interface{}) {
	w.Header().Set(headerConst.ContentType, "application/json")
	w.WriteHeader(status)

	resp := &ResponseStruct{Code: status, Message: message, Data: payload}
	r, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
	}

	_, _ = w.Write(r)
}
