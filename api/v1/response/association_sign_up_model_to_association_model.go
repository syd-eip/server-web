package response

import (
	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Converts a model AssociationSignUp into a Association model.
// Parameters :
//		association model.AssociationSignUp :
//			AssociationSignUp to be converted.
// Returns :
//		response.Association :
//			AssociationSignUp converted into a Association.
func ConvertAssociationSignUpToAssociation(association model.AssociationSignUp) model.Association {
	return model.Association{
		Name:        association.Name,
		Description: association.Description,
		Location:    association.Location,
		Phone:       association.Phone,
		Email:       association.Email,
	}
}
