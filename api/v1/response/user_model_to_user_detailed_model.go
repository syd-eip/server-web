// This packages provides functions to convert a model into an other.
package response

import (
	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Converts a model User into a User detailed model.
// Parameters :
//		user model.User :
//			User to be converted.
// Returns :
//		response.UserDetailed :
//			User converted into a UserDetailed.
func ConvertUserToUserDetailed(user model.User) model.UserDetailed {
	return model.UserDetailed{
		Id:                 user.Id,
		Role:               user.Role,
		Email:              user.Email,
		FirstName:          user.FirstName,
		LastName:           user.LastName,
		Username:           user.Username,
		Birthday:           user.Birthday,
		Phone:              user.Phone,
		Location:           user.Location,
		CguValidated:       user.CguValidated,
		WantToBeInfluencer: user.WantToBeInfluencer,
		Wallet:             user.Wallet,
		Favorites:          user.Favorites,
		OldEvents:          user.OldEvents,
		CurrentEvents:      user.CurrentEvents,
		Images:             user.Images,
		CreationDate:       user.CreationDate,
		UpdateDate:         user.UpdateDate,
	}
}
