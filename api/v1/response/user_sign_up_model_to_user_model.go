package response

import (
	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Converts a model UserSignUp into a User model.
// Parameters :
//		user model.UserSignUp :
//			UserSignUp to be converted.
// Returns :
//		response.User :
//			UserSignUp converted into a User.
func ConvertUserSignUpToUser(user model.UserSignUp) model.User {
	return model.User{
		FirstName:          user.FirstName,
		LastName:           user.LastName,
		Username:           user.Username,
		Email:              user.Email,
		Password:           user.Password,
		Birthday:           user.Birthday,
		Phone:              user.Phone,
		Location:           user.Location,
		CguValidated:       user.CguValidated,
		WantToBeInfluencer: user.WantToBeInfluencer,
	}
}
