// This packages provides functions to convert a model into an other.
package response

import (
	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Converts a model User into a User public model.
// Parameters :
//		user model.User :
//			User to be converted.
// Returns :
//		response.UserPublic :
//			User converted into a UserPublic.
func ConvertUserToUserPublic(user model.User) model.UserPublic {
	return model.UserPublic{
		Id:            user.Id,
		Username:      user.Username,
		Country:       user.Location.Country,
		Favorites:     user.Favorites,
		OldEvents:     user.OldEvents,
		CurrentEvents: user.CurrentEvents,
		Images:        user.Images,
		CreationDate:  user.CreationDate,
		UpdateDate:    user.UpdateDate,
	}
}
