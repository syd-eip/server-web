package event

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCheckFieldsCreateEvent(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Good request",
			body: "{" +
				"\"name\": \"CreateEvent number 1 : Ok! <3\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "Good request with lotteriessize to 1",
			body: "{" +
				"\"name\": \"CreateEventOk\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"," +
				"\"lotteriessize\": 1" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "Not enough Lotteries asked",
			body: "{" +
				"\"name\": \"CreateEventOk\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"," +
				"\"lotteriessize\": -1" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventLotteriesSizeMinLength,
			err:     "",
		},
		{
			name: " name missing",
			body: "{" +
				"\"name\": \"\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameMissing,
			err:     "",
		},
		{
			name: "Event Name bad format",
			body: "{" +
				"\"name\": \"ls;cd/\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameBadFormat,
			err:     "",
		},
		{
			name: "wrong name length",
			body: "{" +
				"\"name\": \"NO\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameMinLength,
			err:     "",
		},
		{
			name: "Event Date missing",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventEventDateMissing,
			err:     "",
		},
		{
			name: "Draw Date missing",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventDrawDateMissing,
			err:     "",
		},
		{
			name: "Event Name already used",
			body: "{" +
				"\"name\": \"test\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.EventNameAlreadyUsed,
			err:     "",
		},
		{
			name: "Event Event Date bad format",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025--235:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventEventDateBadFormat,
			err:     "",
		},
		{
			name: "Event Draw Date bad format",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"20--2:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventDrawDateBadFormat,
			err:     "",
		},
		{
			name: "Draw Date is to close to the Event Date",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T18:20:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventEventDateTooEarly,
			err:     "",
		},
		{
			name: "Event is created less then 24 hours before it takes place",
			body: "{" +
				"\"name\": \"Fail\"," +
				"\"association\": \"Test\"," +
				"\"drawDate\": \"2015-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2016-06-23T18:20:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventDrawDateTooEarly,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(CheckFieldsCreateEvent)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
