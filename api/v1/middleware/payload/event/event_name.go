package event

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"io/ioutil"
	"net/http"
	"strings"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/event"

	eventconst "gitlab.com/syd-eip/server-web/api/v1/constant/event"
	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a EventName model.
//		It also checks if the required fields are filled and valid for the user EventName function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldNameEvent(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var event model.EventName

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&event); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingNameEvent(event); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatAndAvailabilityNameEvent(event); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the event EventName function.
// Parameters :
//		eventCreateEvent model.EventName :
//			Event containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingNameEvent(updateEvent model.EventName) (int, string) {
	switch {
	case updateEvent.Name == "":
		return http.StatusBadRequest, errormessage.EventNameMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format and available for the event EventName function.
// Parameters :
//		eventCreateEvent model.EventName :
//			Event containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatAndAvailabilityNameEvent(updateEvent model.EventName) (int, string) {
	eventName := strings.TrimSpace(updateEvent.Name)
	switch {
	case len(eventName) < eventconst.MinLengthEventName:
		return http.StatusBadRequest, errormessage.EventNameMinLength
	case !checkformat.CheckEventName(eventName):
		return http.StatusBadRequest, errormessage.EventNameBadFormat
	case DaoEvent.FindByName(eventName) == true:
		return http.StatusConflict, errormessage.EventNameAlreadyUsed
	}
	return 0, ""
}
