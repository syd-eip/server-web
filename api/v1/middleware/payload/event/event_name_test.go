package event

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestCheckFieldsNameEvent(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Good request",
			body: "{" +
				"\"name\": \"CreateEvent number 1 : Ok! <3\"" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: " name missing",
			body: "{" +
				"\"name\": \"\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameMissing,
			err:     "",
		},
		{
			name: "Event Name bad format",
			body: "{" +
				"\"name\": \"ls;cd/\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameBadFormat,
			err:     "",
		},
		{
			name: "wrong name length",
			body: "{" +
				"\"name\": \"NO\"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameMinLength,
			err:     "",
		},
		{
			name: "wrong name length with trim",
			body: "{" +
				"\"name\": \"             NO           \"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EventNameMinLength,
			err:     "",
		},
		{
			name: "Event Name already used",
			body: "{" +
				"\"name\": \"     test      \"," +
				"\"drawDate\": \"2025-06-23T18:20:43Z\"," +
				"\"eventDate\": \"2025-06-23T19:25:43Z\"" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.EventNameAlreadyUsed,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(CheckFieldNameEvent)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
