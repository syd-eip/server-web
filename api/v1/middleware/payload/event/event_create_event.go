package event

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/event"

	eventconst "gitlab.com/syd-eip/server-web/api/v1/constant/event"
	associationController "gitlab.com/syd-eip/server-web/api/v1/controller/association"
	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a EventCreateEvent model.
//		It also checks if the required fields are filled and valid for the user CreateEvent function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsCreateEvent(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userCreateEvent model.EventCreateEvent

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userCreateEvent); err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingCreateEvent(userCreateEvent); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatAndAvailabilityCreateEvent(userCreateEvent); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the event CreateEvent function.
// Parameters :
//		eventCreateEvent model.EventCreateEvent :
//			Event containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingCreateEvent(eventCreateEvent model.EventCreateEvent) (int, string) {
	switch {
	case eventCreateEvent.Name == "":
		return http.StatusBadRequest, errormessage.EventNameMissing
	case eventCreateEvent.DrawDate == "":
		return http.StatusBadRequest, errormessage.EventDrawDateMissing
	case eventCreateEvent.EventDate == "":
		return http.StatusBadRequest, errormessage.EventEventDateMissing
	case eventCreateEvent.Association == "":
		return http.StatusBadRequest, errormessage.AssociationMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format and available for the event CreateEvent function.
// Parameters :
//		eventCreateEvent model.EventCreateEvent :
//			Event containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatAndAvailabilityCreateEvent(eventCreateEvent model.EventCreateEvent) (int, string) {
	eventDate, errEventDate := time.Parse(time.RFC3339, eventCreateEvent.EventDate)
	drawDate, errDrawDate := time.Parse(time.RFC3339, eventCreateEvent.DrawDate)
	eventName := strings.TrimSpace(eventCreateEvent.Name)
	associationName := strings.TrimSpace(eventCreateEvent.Association)

	switch {
	case eventCreateEvent.LotteriesSize != 0:
		if eventCreateEvent.LotteriesSize < 0 {
			return http.StatusBadRequest, errormessage.EventLotteriesSizeMinLength
		}
	case len(eventName) < eventconst.MinLengthEventName:
		return http.StatusBadRequest, errormessage.EventNameMinLength
	case !checkformat.CheckEventName(eventName):
		return http.StatusBadRequest, errormessage.EventNameBadFormat
	case DaoEvent.FindByName(eventName) == true:
		return http.StatusConflict, errormessage.EventNameAlreadyUsed
	case associationController.DaoAssociation.FindByName(associationName) == false:
		if associationController.DaoAssociation.FindById(associationName) == false {
			return http.StatusNotFound, errormessage.AssociationNotFound
		}
	case errEventDate != nil:
		return http.StatusBadRequest, errormessage.EventEventDateBadFormat
	case errDrawDate != nil:
		return http.StatusBadRequest, errormessage.EventDrawDateBadFormat
	}

	minDrawTime := time.Now().Add(eventconst.MinDiffEventCreationDate)
	minEventDate := drawDate.Add(eventconst.MinDiffDrawAndEventDate)

	switch {
	case minDrawTime.After(drawDate):
		return http.StatusBadRequest, errormessage.EventDrawDateTooEarly
	case minEventDate.After(eventDate):
		return http.StatusBadRequest, errormessage.EventEventDateTooEarly
	}
	return 0, ""
}
