package mailing

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsSendMail(t *testing.T) {

	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Good request",
			body: "{" +
				"\"subject\": \"Test\"," +
				"\"body\": \"Test\"," +
				"\"politenessFormula\": \"Test\"," +
				"\"signature\": \"Test\"," +
				"\"receivers\": " + "[" +
				"\"test1.test1@test1.com\"," +
				"\"test2.test2@test2.com\"" +
				"]" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "Subject missing",
			body: "{" +
				"\"subject\": \"\"," +
				"\"body\": \"Test\"," +
				"\"politenessFormula\": \"Test\"," +
				"\"signature\": \"Test\"," +
				"\"receivers\": " + "[" +
				"\"test1.test1@test1.com\"," +
				"\"test2.test2@test2.com\"" +
				"]" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.SubjectMissing,
			err:     "",
		},
		{
			name: "Body missing",
			body: "{" +
				"\"subject\": \"Test\"," +
				"\"body\": \"\"," +
				"\"politenessFormula\": \"Test\"," +
				"\"signature\": \"Test\"," +
				"\"receivers\": " + "[" +
				"\"test1.test1@test1.com\"," +
				"\"test2.test2@test2.com\"" +
				"]" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.BodyMissing,
			err:     "",
		},
		{
			name: "Politeness formula missing",
			body: "{" +
				"\"subject\": \"Test\"," +
				"\"body\": \"Test\"," +
				"\"politenessFormula\": \"\"," +
				"\"signature\": \"Test\"," +
				"\"receivers\": " + "[" +
				"\"test1.test1@test1.com\"," +
				"\"test2.test2@test2.com\"" +
				"]" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PolitenessFormulaMissing,
			err:     "",
		},
		{
			name: "Signature missing",
			body: "{" +
				"\"subject\": \"Test\"," +
				"\"body\": \"Test\"," +
				"\"politenessFormula\": \"Test\"," +
				"\"signature\": \"\"," +
				"\"receivers\": " + "[" +
				"\"test1.test1@test1.com\"," +
				"\"test2.test2@test2.com\"" +
				"]" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.SignatureMissing,
			err:     "",
		},
		{
			name: "Receivers missing",
			body: "{" +
				"\"subject\": \"Test\"," +
				"\"body\": \"Test\"," +
				"\"politenessFormula\": \"Test\"," +
				"\"signature\": \"Test\"," +
				"\"receivers\": " + "[" +
				"]" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.ReceiversMissing,
			err:     "",
		},
	}

	for _, tc := range tt {

		middles := sydMiddleware.ChainMiddlewares(CheckFieldsSendMail)
		//Lambda defining a fake route for the test.
		handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNoContent)
		}))

		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				println(tc.body)
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
