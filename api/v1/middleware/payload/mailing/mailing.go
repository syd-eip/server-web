// This package provides middlewares to check the mailing's controllers payloads.
package mailing

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a MailingStructure model.
//		It also checks if the required fields are filled for the SendMail function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsSendMail(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var mailStructure model.MailingStructure

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&mailStructure); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingSendMail(mailStructure); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the SendMail function.
// Parameters :
//		mail model.mailStructure :
//			Mail containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingSendMail(mail model.MailingStructure) (int, string) {
	switch {
	case mail.Subject == "":
		return http.StatusBadRequest, errormessage.SubjectMissing
	case mail.Body == "":
		return http.StatusBadRequest, errormessage.BodyMissing
	case mail.PolitenessFormula == "":
		return http.StatusBadRequest, errormessage.PolitenessFormulaMissing
	case mail.Signature == "":
		return http.StatusBadRequest, errormessage.SignatureMissing
	case len(mail.Receivers) == 0:
		return http.StatusBadRequest, errormessage.ReceiversMissing
	}
	return 0, ""
}
