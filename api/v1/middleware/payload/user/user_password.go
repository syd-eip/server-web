package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserPassword model.
//		It also checks if the required field is filled and valid when a user send his password.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsPassword(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userPassword model.UserPassword

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userPassword); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingPassword(userPassword); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatPassword(userPassword); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his password.
// Parameters :
//		userPassword model.UserPassword :
//			Password containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingPassword(userPassword model.UserPassword) (int, string) {
	if userPassword.Password == "" {
		return http.StatusBadRequest, errormessage.PasswordMissing
	}
	if userPassword.ConfirmPassword == "" {
		return http.StatusBadRequest, errormessage.ConfirmPasswordMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his password.
// Parameters :
//		userPassword model.Password :
//			Password containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatPassword(userPassword model.UserPassword) (int, string) {
	if userPassword.Password != userPassword.ConfirmPassword {
		return http.StatusBadRequest, errormessage.ConfirmPasswordMatch
	}

	errPwd := security.ValidPassword(userPassword.Password)

	if errPwd != nil {
		return http.StatusBadRequest, errPwd.Error()
	}
	return 0, ""
}
