// This package provides middlewares to check the user's controllers payloads.
package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"

	checkRole "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserSignIn model.
//		It also checks if the required fields are filled for the user SignIn function.
//		 username/email, password
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsSignIn(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userSignIn model.UserSignIn

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userSignIn); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userSignIn.Login = strings.TrimSpace(userSignIn.Login)

		if status, mess := checkFieldsMissingSignIn(userSignIn); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatSignIn(userSignIn); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the user SignIn function.
// Parameters :
//		userSignIn model.UserSignIn :
//			User containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingSignIn(userSignIn model.UserSignIn) (int, string) {
	switch {
	case userSignIn.Login == "":
		return http.StatusBadRequest, errormessage.LoginMissing
	case userSignIn.Password == "":
		return http.StatusBadRequest, errormessage.PasswordMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format for the user SignIn function.
// Parameters :
//		userSignIn model.UserSignIn :
//			User containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatSignIn(userSignIn model.UserSignIn) (int, string) {
	var user model.User

	bddUserUsername, errUsername := DaoUser.FindByUsernameSendBackUser(userSignIn.Login)
	if errUsername != nil {
		bddUserEmail, errEmail := DaoUser.FindByEmailSendBackUser(userSignIn.Login)
		if errEmail != nil {
			return http.StatusNotFound, errormessage.LoginBad
		}
		user = bddUserEmail
	} else {
		user = bddUserUsername
	}
	if checkRole.ContainsRole(user.Role, role.GuestRole) != -1 {
		return http.StatusForbidden, errormessage.UserIsGuest
	}
	if checkRole.ContainsRole(user.Role, role.UserRole) == -1 {
		return http.StatusForbidden, errormessage.UserNotUser
	}
	if ok := security.CheckPasswordHash(userSignIn.Password, user.Password); ok == false {
		return http.StatusBadRequest, errormessage.PasswordBad
	}
	return 0, ""
}
