package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsPassword(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name:    "Password OK",
			body:    "{\"password\": \"Testtt1*\", \"confirmPassword\": \"Testtt1*\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name:    "Password missing",
			body:    "{\"password\": \"\", \"confirmPassword\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMissing,
			err:     "",
		},
		{
			name:    "Confirm Password missing",
			body:    "{\"password\": \"Testtt1*\", \"confirmPassword\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.ConfirmPasswordMissing,
			err:     "",
		},
		{
			name:    "Password and confirm don't match",
			body:    "{\"password\": \"Testtt1*\", \"confirmPassword\": \"Testtt1***\"}",
			status:  http.StatusBadRequest,
			message: errormessage.ConfirmPasswordMatch,
			err:     "",
		},
		{
			name:    "Password bad minimum length",
			body:    "{\"password\": \"Test\", \"confirmPassword\": \"Test\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMinLength,
			err:     "",
		},
		{
			name:    "Password bad maximum length",
			body:    "{\"password\": \"Testttttttttttttttttttttttttttttttttttttttttttttttt\", \"confirmPassword\": \"Testttttttttttttttttttttttttttttttttttttttttttttttt\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMaxLength,
			err:     "",
		},
		{
			name:    "Password bad missing lower case",
			body:    "{\"password\": \"TESTTT1*\", \"confirmPassword\": \"TESTTT1*\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name:    "Password bad missing upper case",
			body:    "{\"password\": \"testttt1*\", \"confirmPassword\": \"testttt1*\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name:    "Password bad missing digit",
			body:    "{\"password\": \"Testttt*\", \"confirmPassword\": \"Testttt*\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name:    "Password bad missing special character",
			body:    "{\"password\": \"Testttt1\", \"confirmPassword\": \"Testttt1\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddleware.ChainMiddlewares(CheckFieldsPassword)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
