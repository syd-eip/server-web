package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserRole model.
//		It also checks if the required field is filled and valid when a user send his role.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsRole(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userRole model.UserRole

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userRole); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userRole.Role = strings.TrimSpace(userRole.Role)

		if status, mess := checkFieldsMissingRole(userRole); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatRole(userRole); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his role.
// Parameters :
//		userRole model.UserRole :
//			userRole containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingRole(userRole model.UserRole) (int, string) {
	if userRole.Role == "" {
		return http.StatusBadRequest, errormessage.RoleMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his role.
// Parameters :
//		userRole model.UserRole :
//			Role containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatRole(userRole model.UserRole) (int, string) {
	set := make(map[string]bool)
	for _, v := range role.RolesTab {
		set[v] = true
	}

	for range set {
		if !set[userRole.Role] {
			return http.StatusNotFound, errormessage.RoleNotExist
		}
	}
	return 0, ""
}
