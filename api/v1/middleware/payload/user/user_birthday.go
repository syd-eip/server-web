package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/constant/legal"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserBirthday model.
//		It also checks if the required field is filled and valid when a user send his birthday.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsBirthday(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userBirthday model.UserBirthday

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userBirthday); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userBirthday.Birthday = strings.TrimSpace(userBirthday.Birthday)

		if status, mess := checkFieldsMissingBirthday(userBirthday); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatBirthday(userBirthday); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his birthday.
// Parameters :
//		userBirthday model.UserBirthday :
//			Birthday containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingBirthday(userBirthday model.UserBirthday) (int, string) {
	switch {
	case userBirthday.Birthday == "":
		return http.StatusBadRequest, errormessage.BirthdayMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his birthday.
// Parameters :
//		userBirthday model.Location :
//			Birthday containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatBirthday(userBirthday model.UserBirthday) (int, string) {
	date, errDate := time.Parse(time.RFC3339, userBirthday.Birthday)
	if errDate != nil {
		return http.StatusBadRequest, errormessage.BirthdayBadFormat
	}
	tmpBirth := time.Now().AddDate(-legal.LegalAgeConst, 0, 0)
	if date.After(tmpBirth) {
		return http.StatusUnauthorized, errormessage.BirthdayNotLegalAge
	}
	return 0, ""
}
