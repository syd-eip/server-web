package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	checkFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserName model.
//		It also checks if the required fields are filled and valid when a user send his name.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsName(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userName model.UserName

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userName); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userName.FirstName = strings.TrimSpace(userName.FirstName)
		userName.LastName = strings.TrimSpace(userName.LastName)

		if status, mess := checkFieldsMissingName(userName); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatName(userName); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a user send his name.
// Parameters :
//		userName model.UserName :
//			Name containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingName(userName model.UserName) (int, string) {
	switch {
	case userName.FirstName == "":
		return http.StatusBadRequest, errormessage.FirstNameMissing
	case userName.LastName == "":
		return http.StatusBadRequest, errormessage.LastNameMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format when a user send his name.
// Parameters :
//		userName model.UserName :
//			Name containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatName(userName model.UserName) (int, string) {
	switch {
	case !checkFormat.CheckName(userName.FirstName):
		return http.StatusBadRequest, errormessage.FirstNameBadFormat
	case !checkFormat.CheckName(userName.LastName):
		return http.StatusBadRequest, errormessage.LastNameBadFormat
	}
	return 0, ""
}
