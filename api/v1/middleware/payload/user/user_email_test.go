package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsEmail(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name:    "Email OK",
			body:    "{\"email\": \"test_mid@test.com\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name:    "Field email missing",
			body:    "{\"email\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailMissing,
			err:     "",
		},
		{
			name:    "Email bad format",
			body:    "{\"email\": \"user\"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailBadFormat,
			err:     "",
		},
		{
			name:    "Email already used",
			body:    "{\"email\": \"test.test@test.com\"}",
			status:  http.StatusConflict,
			message: errormessage.EmailAlreadyUsed,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddleware.ChainMiddlewares(CheckFieldsEmailUnique)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
