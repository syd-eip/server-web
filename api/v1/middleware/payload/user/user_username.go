package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserUsername model.
//		It also checks if the required field is filled and valid when a user send his username.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsUsername(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userUsername model.UserUsername

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userUsername); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userUsername.Username = strings.TrimSpace(userUsername.Username)

		if status, mess := checkFieldsMissingUsername(userUsername); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatUsername(userUsername); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his username.
// Parameters :
//		userUsername model.UserUsername :
//			Username containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingUsername(userUsername model.UserUsername) (int, string) {
	if userUsername.Username == "" {
		return http.StatusBadRequest, errormessage.UsernameMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his username.
// Parameters :
//		userUsername model.UserUsername :
//			Username containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatUsername(userUsername model.UserUsername) (int, string) {
	if DaoUser.FindByUsername(userUsername.Username) {
		return http.StatusConflict, errormessage.UsernameAlreadyUsed
	}

	return 0, ""
}
