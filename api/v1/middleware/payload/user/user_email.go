package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"

	checkFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserEmail model.
//		It also checks if the required field is filled and valid when a user send his email.
//		It checked if the user email DOES NOT exist in SYD database.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsEmailUnique(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userEmail model.UserEmail

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userEmail); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userEmail.Email = strings.TrimSpace(userEmail.Email)

		if status, mess := checkFieldsMissingEmail(userEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatEmailUnique(userEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the payload in the request is valid and correspond to a UserEmail model.
//		It also checks if the required field is filled and valid when a user send his email.
//		It checked if the user email DOES exist in SYD database.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsEmailExists(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userEmail model.UserEmail

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userEmail); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userEmail.Email = strings.TrimSpace(userEmail.Email)

		if status, mess := checkFieldsMissingEmail(userEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatEmailExists(userEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his email.
// Parameters :
//		userEmail model.UserEmail :
//			Email containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingEmail(userEmail model.UserEmail) (int, string) {
	if userEmail.Email == "" {
		return http.StatusBadRequest, errormessage.EmailMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his email.
//		It also checked if the email of the user DOES NOT exist in SYD database.
// Parameters :
//		userEmail model.Email :
//			Email containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatEmailUnique(userEmail model.UserEmail) (int, string) {
	switch checkFormat.CheckEmail(userEmail.Email) {
	case "bad_format":
		return http.StatusBadRequest, errormessage.EmailBadFormat
	case "bad_mail":
		return http.StatusNotFound, errormessage.EmailNotExist
	}
	if DaoUser.FindByEmail(userEmail.Email) {
		return http.StatusConflict, errormessage.EmailAlreadyUsed
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a user send his email.
//		It also checked if the email of the user DOES exist in SYD database.
// Parameters :
//		userEmail model.Email :
//			Email containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatEmailExists(userEmail model.UserEmail) (int, string) {
	switch checkFormat.CheckEmail(userEmail.Email) {
	case "bad_format":
		return http.StatusBadRequest, errormessage.EmailBadFormat
	case "bad_mail":
		return http.StatusNotFound, errormessage.EmailNotExist
	}
	if !DaoUser.FindByEmail(userEmail.Email) {
		return http.StatusNotFound, errormessage.UserNotFound
	}
	return 0, ""
}
