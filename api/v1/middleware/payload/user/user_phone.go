package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"

	checkFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a User Phone model.
//		It also checks if the required field is filled and valid when a user send his phone number.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsPhone(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userPhone model.Phone

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userPhone); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userPhone.CountryCode = strings.TrimSpace(userPhone.CountryCode)
		userPhone.NationalNumber = strings.TrimSpace(userPhone.NationalNumber)

		if status, mess := checkFieldsMissingPhone(userPhone); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatPhone(userPhone); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a user send his phone.
// Parameters :
//		userPhone model.Phone :
//			Phone containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingPhone(userPhone model.Phone) (int, string) {
	if userPhone.CountryCode == "" {
		return http.StatusBadRequest, errormessage.PhoneCountryCodeMissing
	}
	if userPhone.NationalNumber == "" {
		return http.StatusBadRequest, errormessage.PhoneNumberMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field are set to the good format when a user send his phone.
// Parameters :
//		userPhone model.Phone :
//			Phone containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatPhone(userPhone model.Phone) (int, string) {
	if checkFormat.CheckPhone(&userPhone) == false {
		return http.StatusBadRequest, errormessage.PhoneBadFormat
	}
	if DaoUser.FindByPhone(userPhone.InternationalNumber) {
		return http.StatusConflict, errormessage.PhoneAlreadyUsed
	}

	return 0, ""
}
