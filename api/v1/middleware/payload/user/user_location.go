package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	checkFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserLocation model.
//		It also checks if the required fields are filled and valid when a user send his location.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsLocation(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userLocation model.Location

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userLocation); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		userLocation.City = strings.TrimSpace(userLocation.City)
		userLocation.ZipCode = strings.TrimSpace(userLocation.ZipCode)
		userLocation.CountryCode = strings.TrimSpace(userLocation.CountryCode)
		userLocation.StreetAddress = strings.TrimSpace(userLocation.StreetAddress)

		if status, mess := checkFieldsMissingLocation(userLocation); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatLocation(userLocation); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a user send his location.
// Parameters :
//		userLocation model.Location :
//			Location containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingLocation(userLocation model.Location) (int, string) {
	switch {
	case userLocation.CountryCode == "":
		return http.StatusBadRequest, errormessage.LocationCountryCodeMissing
	case userLocation.City == "":
		return http.StatusBadRequest, errormessage.LocationCityMissing
	case userLocation.StreetAddress == "":
		return http.StatusBadRequest, errormessage.LocationStreetAddressMissing
	case userLocation.ZipCode == "":
		return http.StatusBadRequest, errormessage.LocationZipCodeMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format when a user send his location.
// Parameters :
//		userLocation model.Location :
//			Location containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatLocation(userLocation model.Location) (int, string) {
	if !checkFormat.CheckLocation(userLocation) {
		return http.StatusBadRequest, errormessage.LocationBadFormat
	}
	return 0, ""
}
