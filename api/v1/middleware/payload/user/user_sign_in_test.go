package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckRequiredFieldsInUser(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name:    "Sign in OK",
			body:    "{\"login\": \"Test\", \"password\": \"Testtt1*\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name:    "Login missing",
			body:    "{\"login\": \"\", \"password\": \"Testtt1*\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LoginMissing,
			err:     "",
		},
		{
			name:    "Password missing",
			body:    "{\"login\": \"Test\", \"password\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMissing,
			err:     "",
		},
		{
			name:    "User is guest",
			body:    "{\"login\": \"Guest\", \"password\": \"Guesttt1*\"}",
			status:  http.StatusForbidden,
			message: errormessage.UserIsGuest,
			err:     "",
		},
		{
			name:    "User not exist : login bad",
			body:    "{\"login\": \"LoginBad\", \"password\": \"Testtt1*\"}",
			status:  http.StatusNotFound,
			message: errormessage.LoginBad,
			err:     "",
		},
		{
			name:    "Password bad",
			body:    "{\"login\": \"Test\", \"password\": \"PasswordNotGood\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordBad,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddleware.ChainMiddlewares(CheckFieldsSignIn)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
