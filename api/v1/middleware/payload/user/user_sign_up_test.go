package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckRequiredFieldsUp(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Sign up OK",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "First name missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.FirstNameMissing,
			err:     "",
		},
		{
			name: "Last name missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LastNameMissing,
			err:     "",
		},
		{
			name: "Username missing",
			body: "{" +
				"\"username\": \"\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.UsernameMissing,
			err:     "",
		},
		{
			name: "Email missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailMissing,
			err:     "",
		},
		{
			name: "Password missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMissing,
			err:     "",
		},
		{
			name: "Birthday missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.BirthdayMissing,
			err:     "",
		},
		{
			name: "Phone country code missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneCountryCodeMissing,
			err:     "",
		},
		{
			name: "Phone national number missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneNumberMissing,
			err:     "",
		},
		{
			name: "Location country code missing",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationCountryCodeMissing,
			err:     "",
		},
		{
			name: "CGU must be validated",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": false, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.CguMustBeValidated,
			err:     "",
		},
		{
			name: "First name bad format",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"()\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.FirstNameBadFormat,
			err:     "",
		},
		{
			name: "Last name bad format",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"()\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LastNameBadFormat,
			err:     "",
		},
		{
			name: "Username already used",
			body: "{" +
				"\"username\": \"Test\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.UsernameAlreadyUsed,
			err:     "",
		},
		{
			name: "Email bad format",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailBadFormat,
			err:     "",
		},
		{
			name: "Email already used",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test.test@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.EmailAlreadyUsed,
			err:     "",
		},
		{
			name: "Phone bad format",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"Test\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneBadFormat,
			err:     "",
		},
		{
			name: "Phone already used",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0222222222\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.PhoneAlreadyUsed,
			err:     "",
		},
		{
			name: "Password bad minimum length",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Test\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMinLength,
			err:     "",
		},
		{
			name: "Password bad maximum length",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testttttttttttttttttttttttttttttttttttttttttttttttt\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordMaxLength,
			err:     "",
		},
		{
			name: "Password bad missing lower case",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"TESTTT1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name: "Password bad missing upper case",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name: "Password bad missing digit",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testttt*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name: "Password bad missing special character",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt11\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PasswordComposition,
			err:     "",
		},
		{
			name: "Location country code not exist",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"Test\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusNotFound,
			message: errormessage.LocationCountryCodeNotExist,
			err:     "",
		},
		{
			name: "Birthday bad format",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2000-01\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.BirthdayBadFormat,
			err:     "",
		},
		{
			name: "Birthday not legal age",
			body: "{" +
				"\"username\": \"Test_mid\", \"firstName\": \"Test\", \"lastName\": \"Test\"," +
				"\"birthday\": \"2100-01-01T00:00:00Z\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\" }," +
				"\"password\": \"Testtt1*\"," +
				"\"cguValidated\": true, \"wantToBeInfluencer\": false" +
				"}",
			status:  http.StatusUnauthorized,
			message: errormessage.BirthdayNotLegalAge,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddleware.ChainMiddlewares(CheckFieldsSignUp)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
