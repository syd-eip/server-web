package user

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddleware "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsName(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name:    "User Name OK",
			body:    "{\"firstName\": \"Test\", \"lastName\": \"Test\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name:    "First name missing",
			body:    "{\"firstName\": \"\", \"lastName\": \"Test\"}",
			status:  http.StatusBadRequest,
			message: errormessage.FirstNameMissing,
			err:     "",
		},
		{
			name:    "Last name missing",
			body:    "{\"firstName\": \"Test\", \"lastName\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LastNameMissing,
			err:     "",
		},
		{
			name:    "First name bad format",
			body:    "{\"firstName\": \"()\", \"lastName\": \"Test\"}",
			status:  http.StatusBadRequest,
			message: errormessage.FirstNameBadFormat,
			err:     "",
		},
		{
			name:    "Last name bad format",
			body:    "{\"firstName\": \"Test\", \"lastName\": \"()\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LastNameBadFormat,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddleware.ChainMiddlewares(CheckFieldsName)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
