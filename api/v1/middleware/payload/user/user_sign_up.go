package user

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"

	legalAge "gitlab.com/syd-eip/server-web/api/v1/constant/legal"
	checkFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a UserSignUp model.
//		It also checks if the required fields are filled and valid for the user SignUp function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsSignUp(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userSignUp model.UserSignUp

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userSignUp); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		TrimUserSignUp(&userSignUp)

		if status, mess := checkFieldsMissingSignUp(userSignUp); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatAndAvailabilitySignUp(userSignUp); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the user SignUp function.
// Parameters :
//		userSignUp model.UserSignUp :
//			User containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingSignUp(userSignUp model.UserSignUp) (int, string) {
	switch {
	case userSignUp.FirstName == "":
		return http.StatusBadRequest, errormessage.FirstNameMissing
	case userSignUp.LastName == "":
		return http.StatusBadRequest, errormessage.LastNameMissing
	case userSignUp.Username == "":
		return http.StatusBadRequest, errormessage.UsernameMissing
	case userSignUp.Email == "":
		return http.StatusBadRequest, errormessage.EmailMissing
	case userSignUp.Password == "":
		return http.StatusBadRequest, errormessage.PasswordMissing
	case userSignUp.Birthday == "":
		return http.StatusBadRequest, errormessage.BirthdayMissing
	case userSignUp.Phone.CountryCode == "":
		return http.StatusBadRequest, errormessage.PhoneCountryCodeMissing
	case userSignUp.Phone.NationalNumber == "":
		return http.StatusBadRequest, errormessage.PhoneNumberMissing
	case userSignUp.Location.CountryCode == "":
		return http.StatusBadRequest, errormessage.LocationCountryCodeMissing
	case !userSignUp.CguValidated:
		return http.StatusBadRequest, errormessage.CguMustBeValidated
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format and available for the user SignUp function.
// Parameters :
//		userSignUp model.UserSignUp :
//			User containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatAndAvailabilitySignUp(userSignUp model.UserSignUp) (int, string) {
	email := checkFormat.CheckEmail(userSignUp.Email)
	countryCodeExist := checkFormat.CheckCountryCodeSignUp(userSignUp.Location.CountryCode)
	date, errDate := time.Parse(time.RFC3339, userSignUp.Birthday)
	errPwd := security.ValidPassword(userSignUp.Password)

	switch {
	case !checkFormat.CheckName(userSignUp.FirstName):
		return http.StatusBadRequest, errormessage.FirstNameBadFormat
	case !checkFormat.CheckName(userSignUp.LastName):
		return http.StatusBadRequest, errormessage.LastNameBadFormat
	case DaoUser.FindByUsername(userSignUp.Username) == true:
		return http.StatusConflict, errormessage.UsernameAlreadyUsed
	case email == "bad_format":
		return http.StatusBadRequest, errormessage.EmailBadFormat
	case email == "bad_mail":
		return http.StatusNotFound, errormessage.EmailNotExist
	case DaoUser.FindByEmail(userSignUp.Email) == true:
		return http.StatusConflict, errormessage.EmailAlreadyUsed
	case checkFormat.CheckPhone(&userSignUp.Phone) == false:
		return http.StatusBadRequest, errormessage.PhoneBadFormat
	case DaoUser.FindByPhone(userSignUp.Phone.InternationalNumber) == true:
		return http.StatusConflict, errormessage.PhoneAlreadyUsed
	case errPwd != nil:
		return http.StatusBadRequest, errPwd.Error()
	case !countryCodeExist:
		return http.StatusNotFound, errormessage.LocationCountryCodeNotExist
	case errDate != nil:
		return http.StatusBadRequest, errormessage.BirthdayBadFormat
	case true:
		tmpBirth := time.Now().AddDate(-legalAge.LegalAgeConst, 0, 0)
		if date.After(tmpBirth) {
			return http.StatusUnauthorized, errormessage.BirthdayNotLegalAge
		}
	}
	return 0, ""
}
