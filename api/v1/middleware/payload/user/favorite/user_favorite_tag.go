package favorite

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to an array of string corresponding to tag names.
//		It also checks if the required field is filled and valid when a user send his favorite.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsFavoriteTag(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userFavoriteTag []string

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userFavoriteTag); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		for i, _ := range userFavoriteTag {
			userFavoriteTag[i] = strings.TrimSpace(userFavoriteTag[i])
		}

		if status, mess := checkFieldsMissingFavoriteTag(userFavoriteTag); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his favorite tags.
// Parameters :
//		userFavoriteTag []string :
//			Favorite containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingFavoriteTag(userFavoriteTag []string) (int, string) {
	switch {
	case len(userFavoriteTag) == 0:
		return http.StatusBadRequest, errormessage.FavoriteTagMissing
	}
	return 0, ""
}
