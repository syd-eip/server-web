package favorite

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to an array of string corresponding to association names.
//		It also checks if the required field is filled and valid when a user send his favorite.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsFavoriteAssociation(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userFavoriteAssociation []model.Association

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userFavoriteAssociation); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingFavoriteAssociation(userFavoriteAssociation); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a user send his favorite associations.
// Parameters :
//		userFavoriteAssociation []string :
//			Favorite containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingFavoriteAssociation(userFavoriteAssociation []model.Association) (int, string) {
	switch {
	case len(userFavoriteAssociation) == 0:
		return http.StatusBadRequest, errormessage.FavoriteAssociationMissing
	}
	return 0, ""
}
