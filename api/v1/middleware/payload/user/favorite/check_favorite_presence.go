package favorite

import (
	"gopkg.in/mgo.v2/bson"
)

// Description :
//		Checks if the tag given in parameter is present in the array.
// Parameters :
//		tags []string :
//			Array of tags checked to see if it contains the tag given in parameter.
//		check string :
//			Tag that we try to find in the array of tags.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the tag is present in the array.
//			- false if not.
func ContainsTag(tags []string, check string) int {
	for i, elem := range tags {
		if elem == check {
			return i
		}
	}
	return -1
}

// Description :
//		Checks if the id given in parameter is present in the array.
// Parameters :
//		users []bson.ObjectId :
//			Array of ids checked to see if it contains the id given in parameter.
//		check bson.ObjectId :
//			Id that we try to find in the array of ids.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the id is present in the array.
//			- false if not.
func ContainsId(ids []bson.ObjectId, check bson.ObjectId) int {
	for i, elem := range ids {
		if elem == check {
			return i
		}
	}
	return -1
}
