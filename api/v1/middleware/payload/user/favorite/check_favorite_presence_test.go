package favorite

import (
	"gopkg.in/mgo.v2/bson"
	"testing"
)

func TestContainsTag(t *testing.T) {
	tags := []string{"gaming", "sport", "music"}

	if ContainsTag(tags, "test") != -1 {
		t.Fatalf("Expected -1, because the string isn't present")
	}

	if ContainsTag(tags, "gaming") != 0 {
		t.Fatalf("Expected 0, because this return the first element matching the check parameter")
	}
}

func TestContainsId(t *testing.T) {
	ids := []bson.ObjectId{"5cef9f62c205a965e9693448", "5cef9f62c205a965e9693449", "5cef9f62c205a965e9693450"}

	if ContainsId(ids, "5cef9f62c205a965e9693430") != -1 {
		t.Fatalf("Expected -1, because the string isn't present")
	}

	if ContainsId(ids, "5cef9f62c205a965e9693448") != 0 {
		t.Fatalf("Expected 0, because this return the first element matching the check parameter")
	}
}
