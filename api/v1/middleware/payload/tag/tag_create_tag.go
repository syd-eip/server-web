package tag

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/tag"

	tagconst "gitlab.com/syd-eip/server-web/api/v1/constant/tag"
	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a TagCreateTag model.
//		It also checks if the required fields are filled and valid for the user CreateTag function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsCreateTag(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var userCreateTag model.TagRequestBody

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&userCreateTag); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if status, mess := checkFieldsMissingCreateTag(userCreateTag); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatAndAvailabilityCreateTag(userCreateTag); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the tag CreateTag function.
// Parameters :
//		tagCreateTag model.TagCreateTag :
//			Tag containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingCreateTag(tagCreateTag model.TagRequestBody) (int, string) {
	switch {
	case tagCreateTag.Name == "":
		return http.StatusBadRequest, errormessage.TagNameMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format and available for the tag CreateTag function.
// Parameters :
//		tagCreateTag model.TagCreateTag :
//			Tag containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatAndAvailabilityCreateTag(tagCreateTag model.TagRequestBody) (int, string) {
	tagName := strings.TrimSpace(tagCreateTag.Name)

	switch {
	case !checkformat.CheckTagName(tagName):
		return http.StatusBadRequest, errormessage.TagNameBadFormat
	case len(tagName) < tagconst.MinLengthTagName:
		return http.StatusBadRequest, errormessage.TagNameMinLength
	case DaoTag.FindByName(tagName) == true:
		return http.StatusConflict, errormessage.TagNameAlreadyUsed
	}

	return 0, ""
}
