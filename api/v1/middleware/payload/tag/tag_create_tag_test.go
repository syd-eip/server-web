package tag

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

func TestCheckFieldsCreateTag(t *testing.T) {

	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Good request",
			body: "{" +
				"\"name\": \"test\"" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: " name missing",
			body: "{" +
				"\"name\": \"\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.TagNameMissing,
			err:     "",
		},
		{
			name: "Tag Name bad format",
			body: "{" +
				"\"name\": \"()\"" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.TagNameBadFormat,
			err:     "",
		},
		{
			name: "Tag Name already used",
			body: "{" +
				"\"name\": \"sport\"" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.TagNameAlreadyUsed,
			err:     "",
		},
	}

	for _, tc := range tt {

		middles := sydMiddlewares.ChainMiddlewares(CheckFieldsCreateTag)
		//Lambda defining a fake route for the test.
		handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusNoContent)
		}))

		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
