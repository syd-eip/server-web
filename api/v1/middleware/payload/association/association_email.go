package association

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/association"

	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a AssociationEmail model.
//		It also checks if the required field is filled and valid when a association send his email.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsEmail(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var associationEmail model.AssociationEmail

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&associationEmail); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		associationEmail.Email = strings.TrimSpace(associationEmail.Email)

		if status, mess := checkFieldsMissingEmail(associationEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatEmail(associationEmail); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required field is not missing when a association send his email.
// Parameters :
//		associationEmail model.AssociationEmail :
//			Email containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingEmail(associationEmail model.AssociationEmail) (int, string) {
	if associationEmail.Email == "" {
		return http.StatusBadRequest, errormessage.EmailMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field is set to the good format when a association send his email.
// Parameters :
//		associationEmail model.Email :
//			Email containing the field to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatEmail(associationEmail model.AssociationEmail) (int, string) {
	switch checkformat.CheckEmail(associationEmail.Email) {
	case "bad_format":
		return http.StatusBadRequest, errormessage.EmailBadFormat
	case "bad_mail":
		return http.StatusNotFound, errormessage.EmailNotExist
	}
	if DaoAssociation.FindByEmail(associationEmail.Email) {
		return http.StatusConflict, errormessage.EmailAlreadyUsed
	}
	return 0, ""
}
