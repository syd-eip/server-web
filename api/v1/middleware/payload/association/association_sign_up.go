package association

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	. "gitlab.com/syd-eip/server-web/api/v1/controller/association"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"io/ioutil"
	"net/http"

	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a AssociationSignUp model.
//		It also checks if the required fields are filled and valid for the association SignUp function.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsSignUp(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var associationSignUp model.AssociationSignUp

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&associationSignUp); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		TrimAssociationSignUp(&associationSignUp)

		if status, mess := checkFieldsMissingSignUp(associationSignUp); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatAndAvailabilitySignUp(associationSignUp); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing for the association SignUp function.
// Parameters :
//		associationSignUp model.AssociationSignUp :
//			Association containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingSignUp(associationSignUp model.AssociationSignUp) (int, string) {
	switch {
	case associationSignUp.Name == "":
		return http.StatusBadRequest, errormessage.AssociationNameMissing
	case associationSignUp.Email == "":
		return http.StatusBadRequest, errormessage.EmailMissing
	case associationSignUp.Phone.CountryCode == "":
		return http.StatusBadRequest, errormessage.PhoneCountryCodeMissing
	case associationSignUp.Phone.NationalNumber == "":
		return http.StatusBadRequest, errormessage.PhoneNumberMissing
	case associationSignUp.Location.CountryCode == "":
		return http.StatusBadRequest, errormessage.LocationCountryCodeMissing
	case associationSignUp.Location.City == "":
		return http.StatusBadRequest, errormessage.LocationCityMissing
	case associationSignUp.Location.ZipCode == "":
		return http.StatusBadRequest, errormessage.LocationZipCodeMissing
	case associationSignUp.Location.StreetAddress == "":
		return http.StatusBadRequest, errormessage.LocationStreetAddressMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format and available for the association SignUp function.
// Parameters :
//		associationSignUp model.AssociationSignUp :
//			Association containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatAndAvailabilitySignUp(associationSignUp model.AssociationSignUp) (int, string) {
	email := checkformat.CheckEmail(associationSignUp.Email)

	switch {
	case !checkformat.CheckLocation(associationSignUp.Location):
		return http.StatusBadRequest, errormessage.LocationBadFormat
	case email == "bad_format":
		return http.StatusBadRequest, errormessage.EmailBadFormat
	case email == "bad_mail":
		return http.StatusNotFound, errormessage.EmailNotExist
	case checkformat.CheckPhone(&associationSignUp.Phone) == false:
		return http.StatusBadRequest, errormessage.PhoneBadFormat
	case DaoAssociation.FindByName(associationSignUp.Name) == true:
		return http.StatusConflict, errormessage.AssociationNameAlreadyUsed
	case DaoAssociation.FindByEmail(associationSignUp.Email) == true:
		return http.StatusConflict, errormessage.EmailAlreadyUsed
	case DaoAssociation.FindByPhone(associationSignUp.Phone.InternationalNumber) == true:
		return http.StatusConflict, errormessage.PhoneAlreadyUsed
	}
	return 0, ""
}
