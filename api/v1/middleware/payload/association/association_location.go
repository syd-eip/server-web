package association

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a AssociationLocation model.
//		It also checks if the required fields are filled and valid when a association send his location.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsLocation(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var associationLocation model.Location

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&associationLocation); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		associationLocation.City = strings.TrimSpace(associationLocation.City)
		associationLocation.ZipCode = strings.TrimSpace(associationLocation.ZipCode)
		associationLocation.CountryCode = strings.TrimSpace(associationLocation.CountryCode)
		associationLocation.StreetAddress = strings.TrimSpace(associationLocation.StreetAddress)

		if status, mess := checkFieldsMissingLocation(associationLocation); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatLocation(associationLocation); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a association send his location.
// Parameters :
//		associationLocation model.Location :
//			Location containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingLocation(associationLocation model.Location) (int, string) {
	switch {
	case associationLocation.CountryCode == "":
		return http.StatusBadRequest, errormessage.LocationCountryCodeMissing
	case associationLocation.City == "":
		return http.StatusBadRequest, errormessage.LocationCityMissing
	case associationLocation.StreetAddress == "":
		return http.StatusBadRequest, errormessage.LocationStreetAddressMissing
	case associationLocation.ZipCode == "":
		return http.StatusBadRequest, errormessage.LocationZipCodeMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required fields are set to the good format when a association send his location.
// Parameters :
//		associationLocation model.Location :
//			Location containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatLocation(associationLocation model.Location) (int, string) {
	if !checkformat.CheckLocation(associationLocation) {
		return http.StatusBadRequest, errormessage.LocationBadFormat
	}
	return 0, ""
}
