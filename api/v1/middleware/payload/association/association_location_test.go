package association

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsLocation(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Location OK",
			body: "{\"countryCode\": \"FR\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "Location country code missing",
			body: "{\"countryCode\": \"\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationCountryCodeMissing,
			err:     "",
		},
		{
			name: "Location city missing",
			body: "{\"countryCode\": \"FR\",\"city\":\"\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationCityMissing,
			err:     "",
		},
		{
			name: "Location street address missing",
			body: "{\"countryCode\": \"FR\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"\",\"zipCode\":\"33000\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationStreetAddressMissing,
			err:     "",
		},
		{
			name: "Location zip code missing",
			body: "{\"countryCode\": \"FR\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationZipCodeMissing,
			err:     "",
		},
		{
			name: "Location country code bad",
			body: "{\"countryCode\": \"Test\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationBadFormat,
			err:     "",
		},
		{
			name: "Location zip code bad",
			body: "{\"countryCode\": \"FR\",\"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"badZipCode\"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationBadFormat,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(CheckFieldsLocation)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
