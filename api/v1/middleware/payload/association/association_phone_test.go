package association

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckFieldsPhone(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name:    "Phone number OK",
			body:    "{\"countryCode\": \"FR\", \"nationalNumber\": \"0333333333\"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name:    "Country code missing",
			body:    "{\"countryCode\": \"\", \"nationalNumber\": \"0333333333\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneCountryCodeMissing,
			err:     "",
		},
		{
			name:    "National number missing",
			body:    "{\"countryCode\": \"FR\", \"nationalNumber\": \"\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneNumberMissing,
			err:     "",
		},
		{
			name:    "Phone bad format",
			body:    "{\"countryCode\": \"FR\", \"nationalNumber\": \"Test\"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneBadFormat,
			err:     "",
		},
		{
			name:    "Phone already used",
			body:    "{\"countryCode\": \"FR\", \"nationalNumber\": \"0222222222\"}",
			status:  http.StatusConflict,
			message: errormessage.PhoneAlreadyUsed,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(CheckFieldsPhone)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
