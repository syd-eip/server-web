package association

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/association"

	checkformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a Association Phone model.
//		It also checks if the required field is filled and valid when a association send his phone number.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsPhone(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var associationPhone model.Phone

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&associationPhone); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		associationPhone.CountryCode = strings.TrimSpace(associationPhone.CountryCode)
		associationPhone.NationalNumber = strings.TrimSpace(associationPhone.NationalNumber)

		if status, mess := checkFieldsMissingPhone(associationPhone); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}
		if status, mess := checkFieldsFormatPhone(associationPhone); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a association send his phone.
// Parameters :
//		associationPhone model.Phone :
//			Phone containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingPhone(associationPhone model.Phone) (int, string) {
	if associationPhone.CountryCode == "" {
		return http.StatusBadRequest, errormessage.PhoneCountryCodeMissing
	}
	if associationPhone.NationalNumber == "" {
		return http.StatusBadRequest, errormessage.PhoneNumberMissing
	}
	return 0, ""
}

// Description :
//		Checks if the required field are set to the good format when a association send his phone.
// Parameters :
//		associationPhone model.Phone :
//			Phone containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsFormatPhone(associationPhone model.Phone) (int, string) {
	if checkformat.CheckPhone(&associationPhone) == false {
		return http.StatusBadRequest, errormessage.PhoneBadFormat
	}
	if DaoAssociation.FindByPhone(associationPhone.InternationalNumber) {
		return http.StatusConflict, errormessage.PhoneAlreadyUsed
	}

	return 0, ""
}
