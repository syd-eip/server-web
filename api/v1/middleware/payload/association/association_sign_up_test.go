package association

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/response"

	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckRequiredFieldsUp(t *testing.T) {
	tt := []struct {
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			name: "Sign up OK",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\", \"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			name: "Name missing",
			body: "{" +
				"\"name\": \"\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.AssociationNameMissing,
			err:     "",
		},
		{
			name: "Email missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailMissing,
			err:     "",
		},
		{
			name: "Phone country code missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneCountryCodeMissing,
			err:     "",
		},
		{
			name: "Phone national number missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneNumberMissing,
			err:     "",
		},
		{
			name: "Location country code missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationCountryCodeMissing,
			err:     "",
		},
		{
			name: "Location city missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationCityMissing,
			err:     "",
		},
		{
			name: "Location zip code missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationZipCodeMissing,
			err:     "",
		},
		{
			name: "Location zip code missing",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationStreetAddressMissing,
			err:     "",
		},
		{
			name: "Name already used",
			body: "{" +
				"\"name\": \"Test\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.AssociationNameAlreadyUsed,
			err:     "",
		},
		{
			name: "Email bad format",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.EmailBadFormat,
			err:     "",
		},
		{
			name: "Email already used",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test.test@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.EmailAlreadyUsed,
			err:     "",
		},
		{
			name: "Phone bad format",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"test\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.PhoneBadFormat,
			err:     "",
		},
		{
			name: "Phone already used",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0222222222\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusConflict,
			message: errormessage.PhoneAlreadyUsed,
			err:     "",
		},
		{
			name: "Location country code not exist",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"Test\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"33000\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationBadFormat,
			err:     "",
		},
		{
			name: "Location zip code not exist",
			body: "{" +
				"\"name\": \"Test_mid\", \"description\": \"Test\", \"email\": \"test_mid@test.com\"," +
				"\"phone\": { \"nationalNumber\": \"0333333333\", \"countryCode\": \"FR\" }," +
				"\"location\": { \"countryCode\": \"FR\", \"city\":\"Bordeaux\"," +
				"\"streetAddress\":\"1 Rue Test\",\"zipCode\":\"Test\"}" +
				"}",
			status:  http.StatusBadRequest,
			message: errormessage.LocationBadFormat,
			err:     "",
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(CheckFieldsSignUp)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Body)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
