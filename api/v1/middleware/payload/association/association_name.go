package association

import (
	"bytes"
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the payload in the request is valid and correspond to a AssociationName model.
//		It also checks if the required fields are filled and valid when a association send his name.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckFieldsName(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var associationName model.AssociationName

		defer r.Body.Close()
		bodyBytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		if err := json.NewDecoder(r.Body).Decode(&associationName); err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, err.Error(), nil)
			return
		}
		r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		associationName.Name = strings.TrimSpace(associationName.Name)

		if status, mess := checkFieldsMissingName(associationName); status != 0 {
			response.RespondWithJson(w, status, mess, nil)
			return
		}

		next(w, r)
	}
}

// Description :
//		Checks if the required fields are not missing when a association send his name.
// Parameters :
//		associationName model.AssociationName :
//			Name containing the fields to check.
// Returns :
//		int :
//			Status of the response, or 0 if everything is valid.
//		string :
//			Message of the response or "" if everything is valid.
func checkFieldsMissingName(associationName model.AssociationName) (int, string) {
	switch {
	case associationName.Name == "":
		return http.StatusBadRequest, errormessage.AssociationNameMissing
	}
	return 0, ""
}
