package staticfiles

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/codemodus/chain"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/server"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
)

func TestCheckGet(t *testing.T) {
	tt := []struct {
		name          string
		requestMethod string
		status        int
		headerKey     string
		headerValue   string
	}{
		{
			name:          "GET Request OK",
			requestMethod: "GET",
			status:        http.StatusNoContent,
		},
		{
			name:          "OPTIONS request without header",
			requestMethod: "OPTIONS",
			status:        http.StatusMethodNotAllowed,
		},
		{
			name:          "OPTIONS request with header",
			requestMethod: "OPTIONS",
			status:        http.StatusNoContent,
			headerKey:     "Access-Control-Request-Method",
			headerValue:   "GET",
		},
		{
			name:          "Wrong request",
			requestMethod: "FAIL",
			status:        http.StatusMethodNotAllowed,
		},
	}

	for _, tc := range tt {

		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest(tc.requestMethod, ServerBaseRoute, nil)
			if err != nil {
				t.Fatalf("Could not created request %v", err)
			}

			rec := httptest.NewRecorder()

			if len(tc.headerKey) > 0 {
				req.Header.Add(tc.headerKey, tc.headerValue)
			}

			middles := chain.New(CheckGet)

			handler := http.Handler(middles.End(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
				w.Header().Set(headerConst.ContentType, "application/json")
			})))

			handler.ServeHTTP(rec, req)

			if status := rec.Code; status != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, status)
			}
		})
	}
}
