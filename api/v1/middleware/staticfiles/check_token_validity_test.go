package staticfiles

import (
	"encoding/json"
	cookie2 "gitlab.com/syd-eip/server-web/api/v1/constant/cookie"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/codemodus/chain"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
)

func TestCheckTokenValidity(t *testing.T) {
	tt := []struct {
		place   int
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			place:   0,
			name:    "Token validity OK",
			body:    "",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			place:   1,
			name:    "User not authorized",
			body:    "",
			status:  http.StatusUnauthorized,
			message: errormessage.UserNotAuthorized,
			err:     "",
		},
		{
			place:   2,
			name:    "Failed parsing token",
			body:    "",
			status:  http.StatusUnauthorized,
			message: errormessage.UserNotAuthorized,
			err:     "",
		},
	}

	var testUser model.User
	token, err := security.GenerateJWTUserToken(testUser)
	CookieSecure, err := strconv.ParseBool(os.Getenv(cookie2.CookieTokenUserSecureValue))
	if err != nil {
		t.Fatalf("Could not get env variable CookieTokenUserSecureValue %v", err)
	}
	CookieHttp, err := strconv.ParseBool(os.Getenv(cookie2.CookieTokenUserHttpValue))
	if err != nil {
		t.Fatalf("Could not get env variable CookieTokenUserHttpValue %v", err)
	}
	expire := time.Now().Add(cookie2.CookieTokenUserExpire)
	cookie := http.Cookie{
		Name:     cookie2.CookieTokenUserName,
		Value:    token,
		Path:     cookie2.CookieTokenUserPath,
		Expires:  expire,
		MaxAge:   cookie2.CookieTokenUserMaxAge,
		HttpOnly: CookieHttp,
		Secure:   CookieSecure,
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			if tc.place == 2 {
				cookie.Value = ""
			}

			if tc.place != 1 {
				req.AddCookie(&cookie)
			}

			recorder := httptest.NewRecorder()
			middles := chain.New(CheckTokenValidity)

			//Lambda defining a fake route for the test.
			handler := http.Handler(middles.End(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			})))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
