// This package provides specific middlewares to serve static files.
// Indeed the documentation, or others static files and data need http.Handler to be served.
// So we had to create middlewares different from the others that are using http.HandleFunc.
package staticfiles

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	checkrole "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
)

// Description :
//		Checks if the array of roles contains the role : GUEST.
// Parameters :
//		h http.Handler :
//			Interface to respond to a request.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.Handler :
//			Next http.Handler called and executed after this middleware.
func IsGuest(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, e := security.ExtractJwt(r)
		if e != nil {
			if e.Error() == "MISSING_TOKEN" {
				response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
				return
			}
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}

		if claim, ok := token.Claims.(jwt.MapClaims); ok {
			if checkrole.ContainsRoleInterface(claim["role"], role.GuestRole) != -1 {
				response.RespondWithJson(w, http.StatusForbidden, errormessage.UserIsGuest, nil)
				return
			}
		} else {
			response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
			return
		}
		h.ServeHTTP(w, r)
	})
}
