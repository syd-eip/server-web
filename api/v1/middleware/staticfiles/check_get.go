package staticfiles

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		Checks if the request method it GET.
// Parameters :
//		h http.Handler :
//			Interface to respond to a request.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.Handler :
//			Next http.Handler called and executed after this middleware.
func CheckGet(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			if r.Header.Get("Access-Control-Request-Method") != "GET" {
				response.RespondWithJson(w, http.StatusMethodNotAllowed, errormessage.MethodNotAllowed, nil)
				return
			}
			response.RespondWithJson(w, http.StatusNoContent, successmessage.OK, nil)
			return
		} else if r.Method != "GET" {
			response.RespondWithJson(w, http.StatusMethodNotAllowed, errormessage.MethodNotAllowed, nil)
			return
		}
		h.ServeHTTP(w, r)
	})
}
