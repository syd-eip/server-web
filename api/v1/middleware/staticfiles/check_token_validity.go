package staticfiles

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
)

// Description :
//		Checks if the token contained in the header or in the cookie is valid.
// Parameters :
//		h http.Handler :
//			Interface to respond to a request.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.Handler :
//			Next http.Handler called and executed after this middleware.
func CheckTokenValidity(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, err := security.ExtractJwt(r)
		if err != nil {
			if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
				response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
				return
			}
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}
		if token.Valid {
			h.ServeHTTP(w, r)
		}
	})
}
