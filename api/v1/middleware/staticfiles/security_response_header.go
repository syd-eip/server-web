package staticfiles

import (
	"net/http"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
)

// Description :
//		Fills the header with some properties for the security and the compatibility with Nginx.
// Parameters :
//		h http.Handler :
//			Interface to respond to a request.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.Handler :
//			Next http.Handler called and executed after this middleware.
func SecurityResponseHeader(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set(headerConst.StrictTransportSecurity, "max-age=31536000; includeSubdomains; preload")
		r.Header.Set(headerConst.XContentTypeOptions, "nosniff")
		r.Header.Set(headerConst.XFrameOptions, "SAMEORIGIN")
		r.Header.Set(headerConst.XXSSProtection, "1; mode=block")
		h.ServeHTTP(w, r)
	})
}
