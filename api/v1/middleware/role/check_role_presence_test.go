package role

import "testing"

func TestContainsRole(t *testing.T) {
	roles := []string{"USER", "ADMIN", "SUPER_ADMIN", "INFLUENCER"}

	if ContainsRole(roles, "TEST") != -1 {
		t.Fatalf("Expected -1, because the string isn't present")
	}

	if ContainsRole(roles, "USER") != 0 {
		t.Fatalf("Expected 0, because this return the first element matching the check parameter")
	}
}
