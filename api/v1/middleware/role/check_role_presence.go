package role

// Description :
//		Checks if the role given in parameter is present in the array.
// Parameters :
//		roles []string :
//			Array of roles checked to see if it contains the role given in parameter.
//		check string :
//			Role that we try to find in the array of roles.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the role is present in the array.
//			- false if not.
func ContainsRole(roles []string, check string) int {
	for i, elem := range roles {
		if elem == check {
			return i
		}
	}
	return -1
}

// Description :
//		Checks if the role given in parameter is present in the claim.
// Parameters :
//		claim interface{} :
//			Claim checked to see if it contains the role given in parameter.
//		check string :
//			Role that we try to find in the claim.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the role is present in the claim.
//			- false if not.
func ContainsRoleInterface(claim interface{}, check string) int {
	roleInterface := claim.([]interface{})
	roles := make([]string, len(roleInterface))
	for i, v := range roleInterface {
		roles[i] = v.(string)
	}
	return ContainsRole(roles, check)
}
