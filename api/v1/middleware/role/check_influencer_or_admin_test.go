package role

import (
	"encoding/json"
	cookie2 "gitlab.com/syd-eip/server-web/api/v1/constant/cookie"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"
	"net/http/httptest"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestIsInfluencerOrAdmin(t *testing.T) {
	tt := []struct {
		place   int
		name    string
		body    string
		status  int
		message string
		err     string
	}{
		{
			place:   0,
			name:    "Influencer role OK",
			body:    "",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			place:   1,
			name:    "Admin role OK",
			body:    "",
			status:  http.StatusNoContent,
			message: "",
			err:     "",
		},
		{
			place:   2,
			name:    "User not Influencer or Admin",
			body:    "",
			status:  http.StatusForbidden,
			message: errormessage.UserNotInfluencer,
			err:     "",
		},
	}

	var testUser model.User
	testUser.Role = []string{role.InfluencerRole}
	token, err := security.GenerateJWTUserToken(testUser)
	CookieSecure, err := strconv.ParseBool(os.Getenv(cookie2.CookieTokenUserSecureValue))
	if err != nil {
		t.Fatalf("Could not get env variable CookieTokenUserSecureValue %v", err)
	}
	CookieHttp, err := strconv.ParseBool(os.Getenv(cookie2.CookieTokenUserHttpValue))
	if err != nil {
		t.Fatalf("Could not get env variable CookieTokenUserHttpValue %v", err)
	}
	expire := time.Now().Add(cookie2.CookieTokenUserExpire)
	cookie := http.Cookie{
		Name:     cookie2.CookieTokenUserName,
		Value:    token,
		Path:     cookie2.CookieTokenUserPath,
		Expires:  expire,
		MaxAge:   cookie2.CookieTokenUserMaxAge,
		HttpOnly: CookieHttp,
		Secure:   CookieSecure,
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest("", "", strings.NewReader(tc.body))
			if err != nil {
				t.Fatalf("Could not create request %v", err)
			}
			if tc.place == 1 {
				testUser.Role = []string{role.AdminRole}
				token, err := security.GenerateJWTUserToken(testUser)
				if err != nil {
					t.Fatalf("Could not generate token %v", err)
				}
				cookie.Value = token
			}
			if tc.place == 2 {
				testUser.Role = []string{role.UserRole}
				token, err := security.GenerateJWTUserToken(testUser)
				if err != nil {
					t.Fatalf("Could not generate token %v", err)
				}
				cookie.Value = token
			}
			req.AddCookie(&cookie)

			recorder := httptest.NewRecorder()
			middles := sydMiddlewares.ChainMiddlewares(IsInfluencerOrAdmin)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))

			handler.ServeHTTP(recorder, req)

			if recorder.Code != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, recorder.Code)
			}
			if tc.status/100 != 2 {
				var resp response.ResponseStruct
				if readErr := json.Unmarshal([]byte(recorder.Body.String()), &resp); readErr != nil {
					t.Fatalf(readErr.Error())
				}
				if resp.Message != tc.message {
					t.Fatalf("ERROR: expected object name %v  _  got %v", tc.message, resp.Message)
				}
			}
		})
	}
}
