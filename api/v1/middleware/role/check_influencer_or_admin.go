package role

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
)

// Description :
//		First, checks if the token is valid.
//		Then, checks if the array of roles contains the role : INFLUENCER or ADMIN.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func IsInfluencerOrAdmin(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token, e := security.ExtractJwt(r)
		if e != nil {
			if e.Error() == "MISSING_TOKEN" {
				response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
				return
			}
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}

		if claim, ok := token.Claims.(jwt.MapClaims); ok {
			if ContainsRoleInterface(claim["role"], role.InfluencerRole) == -1 &&
				ContainsRoleInterface(claim["role"], role.AdminRole) == -1 {
				response.RespondWithJson(w, http.StatusForbidden, errormessage.UserNotInfluencer, nil)
				return
			}
		} else {
			response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
			return
		}
		next(w, r)
	}
}
