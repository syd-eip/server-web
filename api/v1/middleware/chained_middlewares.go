// This package provides middlewares for the requests.
package sydMiddlewares

import "net/http"

// Type defining a function that returns a new handlerFunc.
type Mw func(http.HandlerFunc) http.HandlerFunc

// Description :
//		Allows to chained middlewares.
// Parameters :
//		mw ...Mw :
//			List of middlewares that are going to be executed.
// Returns :
//		Mw :
//			Next middleware that'll be executed.
func ChainMiddlewares(mw ...Mw) Mw {
	return func(final http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			last := final
			for i := len(mw) - 1; i >= 0; i-- {
				last = mw[i](last)
			}
			last(w, r)
		}
	}
}
