// This package provides middlewares to check and add some security to the requests and responses.
package security

import (
	"net/http"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
)

// Description :
//		Fills the header with some properties for the security and the compatibility with Nginx.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func SecurityResponseHeader(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		r.Header.Set(headerConst.StrictTransportSecurity, "max-age=31536000; includeSubdomains; preload")
		r.Header.Set(headerConst.XContentTypeOptions, "nosniff")
		r.Header.Set(headerConst.XFrameOptions, "SAMEORIGIN")
		r.Header.Set(headerConst.XXSSProtection, "1; mode=block")
		next(w, r)
	}
}
