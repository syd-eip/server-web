package format

import (
	"github.com/nyaruka/phonenumbers"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Checks if a phone number is valid when he registers.
// Parameters :
//		phone *model.Phone :
//			Phone number that'll be checked and validated or not.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the phone number is valid.
//			- false if not.
func CheckPhone(phone *model.Phone) bool {
	numberProto, err := phonenumbers.Parse(phone.NationalNumber, phone.CountryCode)
	if err != nil {
		return false
	}
	phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
	return true
}
