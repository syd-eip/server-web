package format

import (
	"regexp"
)

// Description :
//		Checks if a name (first or last) of the user is valid.
// Parameters :
//		name string :
//			Name of the user to be validated.
// Returns :
//		bool :
//			This bool indicates the status of the verification :
//			- true if the name is valid.
//			- false if the name is not valid.
func CheckName(name string) bool {
	validName := regexp.MustCompile(`^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$`)

	return validName.MatchString(name)
}

// Description :
//		Checks if the name of the event is valid.
// Parameters :
//		name string :
//			Name of the event to be validated.
// Returns :
//		bool :
//			This bool indicates the status of the verification :
//			- true if the name is valid.
//			- false if the name is not valid.
func CheckEventName(name string) bool {
	validName := regexp.MustCompile(`^[a-zA-Z0-9àáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.:<>!?*$€()_+=@'-]+$`)

	return validName.MatchString(name)
}

// Description :
//		Checks if the name of the tag is valid.
// Parameters :
//		name string :
//			Name of the tag to be validated.
// Returns :
//		bool :
//			This bool indicates the status of the verification :
//			- true if the name is valid.
//			- false if the name is not valid.
func CheckTagName(name string) bool {
	validName := regexp.MustCompile(`^[a-z\-]+$`)

	return validName.MatchString(name)
}
