package format

import (
	"github.com/Boostport/address"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

// Description :
//		Checks if a location is valid.
// Parameters :
//		location model.location :
//			Location that'll be checked and validated or not.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the phone number is valid.
//			- false if not.
func CheckLocation(location model.Location) bool {
	_, err := address.NewValid(
		address.WithCountry(location.CountryCode),
		address.WithStreetAddress([]string{location.StreetAddress}),
		address.WithLocality(location.City),
		address.WithPostCode(location.ZipCode),
	)
	if err != nil {
		return false
	}

	return true
}

// Description :
//		Checks if the user's countryCode is valid when he registers.
// Parameters :
//		countryCode string :
//			CountryCode that'll be checked and validated or not.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the countryCode exists.
//			- false if not.
func CheckCountryCodeSignUp(countryCode string) bool {
	for _, elem := range address.ListCountries("") {
		if elem.Code == countryCode {
			return true
		}
	}
	return false
}

// Description :
//		Returns the country's name corresponding to the country code.
// Parameters :
//		countryCode string :
//			CountryCode of the country we want.
// Returns :
//		string :
//			Name of the country.
//			Or "" if nothing is found.
func GetCountryname(countryCode string) string {
	for _, elem := range address.ListCountries("") {
		if elem.Code == countryCode {
			return elem.Name
		}
	}
	return ""
}
