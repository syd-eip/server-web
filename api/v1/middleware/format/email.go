// This package provides middlewares to check the format of some data.
package format

import (
	"regexp"
)

// Description :
//		Checks if an email is valid.
// Parameters :
//		email string :
//			Email that'll be checked and validated or not.
// Returns :
//		string :
//			This string indicates the status of the verification :
//			- "" if the email is valid.
//			- "bad_format" if the email's format is not valid.
//			- "bad_mail" if the email is not valid.
func CheckEmail(email string) string {
	var rxEmail = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if !rxEmail.MatchString(email) {
		return "bad_format"
	}
	return ""
}
