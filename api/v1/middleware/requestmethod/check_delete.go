// This package provides middlewares to check the request method.
package requestmethod

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"net/http"

	"gitlab.com/syd-eip/server-web/api/v1/response"
)

// Description :
//		First, checks if the request method is OPTIONS:
//			If yes, checks if "Access-Control-Request-Method" is DELETE.
//			If not, checks if the request method is DELETE.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckDelete(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "OPTIONS" {
			if r.Header.Get("Access-Control-Request-Method") != "DELETE" {
				response.RespondWithJson(w, http.StatusMethodNotAllowed, errormessage.MethodNotAllowed, nil)
				return
			}
			response.RespondWithJson(w, http.StatusNoContent, successmessage.OK, nil)
			return
		} else if r.Method != "DELETE" {
			response.RespondWithJson(w, http.StatusMethodNotAllowed, errormessage.MethodNotAllowed, nil)
			return
		}
		next(w, r)
	}
}
