package requestmethod

import (
	"net/http"
	"net/http/httptest"
	"testing"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/server"
	sydMiddlewares "gitlab.com/syd-eip/server-web/api/v1/middleware"
)

func TestCheckDelete(t *testing.T) {
	tt := []struct {
		name				string
		requestMethod		string
		status				int
		headerKey			string
		headerValue			string
	}{
		{
			name: "Good DELETE Request",
			requestMethod: "DELETE",
			status:    http.StatusNoContent,
		},
		{
			name: "OPTIONS request without header",
			requestMethod: "OPTIONS",
			status:    http.StatusMethodNotAllowed,
		},
		{
			name: "OPTIONS request with header",
			requestMethod: "OPTIONS",
			status:    http.StatusNoContent,
			headerKey: "Access-Control-Request-Method",
			headerValue: "DELETE",
		},
		{
			name: "Wrong request",
			requestMethod: "FAIL",
			status:    http.StatusMethodNotAllowed,
		},
	}

	for _, tc := range tt {

		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest(tc.requestMethod, ServerBaseRoute, nil)
			if err != nil { t.Fatalf("could not created request %v", err) }

			rec := httptest.NewRecorder()

			if len(tc.headerKey) > 0 { req.Header.Add(tc.headerKey, tc.headerValue) }

			middles := sydMiddlewares.ChainMiddlewares(CheckDelete)

			//Lambda defining a fake route for the test.
			handler := http.HandlerFunc(middles(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNoContent)
			}))
			handler.ServeHTTP(rec, req)

			if status := rec.Code; status != tc.status {
				t.Fatalf("ERROR: expected status %v  _  got %v", tc.status, status)
			}
		})
	}
}