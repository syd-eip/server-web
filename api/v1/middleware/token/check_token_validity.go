// This package provides middlewares to check tokens.
package token

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	. "gitlab.com/syd-eip/server-web/api/v1/controller/user"
)

// Description :
//		Checks if the token contained in the header or in the cookie is valid.
// Parameters :
//		h http.HandlerFunc :
//			Adapter to allow the use of ordinary functions as HTTP handlers :
//				HandlerFunc(f) is a Handler that calls f.
//			This one is created to handle the actions done in this middleware.
// Returns :
//		http.HandlerFunc :
//			Next http.HandlerFunc called and executed after this middleware.
func CheckTokenValidity(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token, err := security.ExtractJwt(r)
		if err != nil {
			if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
				response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
				return
			}
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}
		if claim, ok := token.Claims.(jwt.MapClaims); ok {
			err := DaoUser.FindById(claim["id"].(string))
			if err == false {
				response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
				return
			}
		}

		next(w, r)
	}
}
