package event

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the events' routes.
const (
	// Value representing the association' route to access all the associations.
	AssociationRoute = ServerBaseRoute + "/association"

	// Value representing the association' route to create a new association.
	CreateAssociationRoute = AssociationRoute + "/new"

	// Value representing the association route to access an association by its Id or name.
	AssociationIdOrNameRoute = AssociationRoute + "/id/{association}"

	// Value representing the association route to delete an association.
	DeleteAssociationRoute = AssociationIdOrNameRoute + "/delete"

	// Value representing the association route to access the settings of an association by its Id or name.
	AssociationSettingRoute = AssociationIdOrNameRoute + "/setting"

	// Value representing the association route to access the name of an association by its Id or name.
	AssociationSettingNameRoute = AssociationSettingRoute + "/name"

	// Value representing the association route to access the description of an association by its Id or name.
	AssociationSettingDescriptionRoute = AssociationSettingRoute + "/description"

	// Value representing the association route to access the email of an association by its Id or name.
	AssociationSettingEmailRoute = AssociationSettingRoute + "/email"

	// Value representing the association route to access the location of an association by its Id or name.
	AssociationSettingLocationRoute = AssociationSettingRoute + "/location"

	// Value representing the association route to access the phone of an association by its Id or name.
	AssociationSettingPhoneRoute = AssociationSettingRoute + "/phone"

	// Value representing the association route to access an image of an association by its Id or name.
	AssociationSettingImageRoute = AssociationSettingRoute + "/image"
)
