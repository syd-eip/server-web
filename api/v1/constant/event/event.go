// Constants set for the event.
package event

import "time"

// Constants set for the event.
const (
	// Value representing the minimum length of an event's name.
	MinLengthEventName = 3

	// Value representing the time between the creation of the event and its unfolding.
	MinDiffEventCreationDate = time.Hour * 24

	// Value representing the time difference between the draw date and the creation of the event.
	MinDiffDrawAndEventDate = time.Hour * 1
)
