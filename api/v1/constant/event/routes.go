package event

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the event routes.
const (
	// Value representing the event route to access all the events.
	EventRoute = ServerBaseRoute + "/event"

	// Value representing the event's "new" route to create new event.
	CreateEventRoute = EventRoute + "/new"

	// Value representing the event's route to access event by its id or name.
	EventByIdOrNameRoute = EventRoute + "/id/{event}"

	// Value representing the event's delete route to delete an event by its id or name.
	DeleteEventRoute = EventByIdOrNameRoute + "/delete"

	// Value representing the event's "user" route to access all the events of an user thanks his userId.
	GetEventsByUserRoute = EventRoute + "/user/{user}"

	// Value representing the event's "user" route to access all the events of an user.
	GetEventsByTagRoute = EventRoute + "/tag/{tag}"

	// Value representing the event's "me" route to access all the events of the authenticated user.
	GetAllMyEventsRoute = EventRoute + "/me"

	// Value representing the event settings routes route to update an event thanks to its id or name.
	EventSettingRoute = EventByIdOrNameRoute + "/setting"

	// Value representing the event setting route to update the event name thanks to its id or name.
	PatchEventNameRoute = EventSettingRoute + "/name"

	// Value representing the event setting route to update the event Description thanks to its id or name.
	PatchEventDescriptionRoute = EventSettingRoute + "/description"

	// Value representing the event setting route to update the event Tags thanks to its id or name.
	PatchEventTagsRoute = EventSettingRoute + "/tags"

	// Value representing the event setting route to update an image from an event thanks to its id or name.
	PatchEventImageRoute = EventSettingRoute + "/image"

	// Value representing the event route to add bet thanks to its id or name.
	AddBetByEventRoute = EventByIdOrNameRoute + "/bet/add"

	// Value representing the event route to get its own bet to a specific event thanks to its id or name.
	GetBetByEventRoute = EventByIdOrNameRoute + "/bet"
)
