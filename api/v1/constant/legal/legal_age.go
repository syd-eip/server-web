// Constants set for the legal values of SYD.
package legal

const (
	// Value representing the legal age necessary to register on SYD.
	LegalAgeConst = 13
)
