// Constants set for the user's cookie authentication.
package cookie

const (
	// Value representing the user's authentication token cookie name.
	CookieAuthenticationName = "CookieAuthentication"
)
