// Constants set for the user's token cookie.
package cookie

import "time"

const (
	// Refers to the environment variable COOKIE_HTTP_VALUE_TOKEN_USER.
	// Value representing if the user's token cookie is Http only (true) or not (false).
	CookieTokenUserHttpValue = "COOKIE_HTTP_VALUE_TOKEN_USER"

	// Refers to the environment variable COOKIE_SECURE_VALUE_TOKEN_USER.
	// Value representing if the user's token cookie is secure => https (true) or not (false).
	CookieTokenUserSecureValue = "COOKIE_SECURE_VALUE_TOKEN_USER"

	// Value representing the user's token cookie name.
	CookieTokenUserName = "CookieTokenUser"

	// Value representing the user's token cookie path.
	CookieTokenUserPath = "/"

	// Value representing the user's token cookie expiration time.
	CookieTokenUserExpire = 24 * time.Hour

	// Value representing the user's token cookie max age.
	CookieTokenUserMaxAge = 86400

	// Value representing the user's token cookie domain.
	CookieTokenUserDomain = ".shareyourdreams.fr"
)
