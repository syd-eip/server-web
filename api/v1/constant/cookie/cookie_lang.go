// Constants set for the cookie lang.
package cookie

const (
	// Value representing the cookie lang's name.
	CookieLangName = "lang"

	// Value representing the cookie lang's value for the French language.
	CookieLangValueFR = "fr"
)
