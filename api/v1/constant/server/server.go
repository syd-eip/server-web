// Constants defining the possible general server values.
package server

const (
	// Refers to the environment variable DEV.
	// Value representing if the API is set for development (true) or production (false).
	Dev = "DEV"

	// Refers to the environment variable SERVER_PORT.
	// Value representing the server port.
	ServerPort = "SERVER_PORT"

	// Value representing the root of the server route.
	ServerBaseRoute = "/v1"

	// Refers to the environment variable SSL_STATUS.
	// Value representing if the API respond to https (true) or http (false).
	SslStatus = "SSL_STATUS"

	// Refers to the environment variable SSL_CERT.
	// Value representing the path of the SSL certificate.
	// Useful if SSL_STATUS is true.
	SslCert = "SSL_CERT"

	// Refers to the environment variable SSL_KEY.
	// Value representing the path of the SSL key.
	// Useful if SSL_STATUS is true.
	SslKey = "SSL_KEY"
)
