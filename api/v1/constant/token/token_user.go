// Constants set for the user's token.
package token

import (
	"os"
	"time"
)

const (
	// Value representing the expiration time of the user's token.
	UserTokenExpire = 24 * time.Hour

	// Refers to the environment variable SIGNING_KEY.
	// Value representing signing key necessary to create the user's token.
	SigningKeyName = "SIGNING_KEY"
)

// Value representing the signing key
var SigningKey = []byte(os.Getenv(SigningKeyName))
