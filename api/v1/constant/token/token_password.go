// Constants set for the password's token.
package token

import (
	"os"
	"time"
)

const (
	// Value representing the expiration time of the password's token.
	PasswordTokenExpire = 30 * time.Minute

	// Refers to the environment variable PASSWORD_KEY.
	// Value representing key necessary to create the password's token.
	PasswordKeyName = "PASSWORD_KEY"
)

// Value representing the password key
var PasswordKey = []byte(os.Getenv(PasswordKeyName))
