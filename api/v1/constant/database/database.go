// Constants set for SYD's MongoDB database.
package database

const (
	// Refers to the environment variable MONGO_ADDR.
	// Value representing the SYD's MongoDB database address.
	MongoAddr = "MONGO_ADDR"

	// Refers to the environment variable SYD_DB.
	// Value representing the SYD's MongoDB database name.
	SydDatabase = "SYD_DB"

	// Refers to the environment variable TIMEOUT_DB.
	// Value representing the SYD's MongoDB database timeout.
	TimeoutDatabase = "TIMEOUT_DB"

	// Refers to the environment variable SUPER_ADMIN_USERNAME_SYD_DB.
	// Value representing the SYD's MongoDB database Super Admin username.
	SuperAdminUsernameSydDatabase = "SUPER_ADMIN_USERNAME_SYD_DB"

	// Refers to the environment variable SUPER_ADMIN_PASSWORD_SYD_DB.
	// Value representing the SYD's MongoDB database Super Admin password.
	SuperAdminPasswordSydDatabase = "SUPER_ADMIN_PASSWORD_SYD_DB"

	// Refers to the environment variable COLLECTION_USERS.
	// Value representing the SYD's users's collection name.
	CollectionUsers = "COLLECTION_USERS"

	// Refers to the environment variable COLLECTION_EVENTS.
	// Value representing the SYD's events's collection name.
	CollectionEvents = "COLLECTION_EVENTS"

	// Refers to the environment variable COLLECTION_TAGS.
	// Value representing the SYD's tags's collection name.
	CollectionTags = "COLLECTION_TAGS"

	// Refers to the environment variable COLLECTION_ASSOCIATIONS.
	// Value representing the SYD's associations's collection name.
	CollectionAssociations = "COLLECTION_ASSOCIATIONS"

	// Refers to the environment variable COLLECTION_IMAGES.
	// Value representing the SYD's images's collection name.
	CollectionImages = "COLLECTION_IMAGES"

	// Refers to the environment variable COLLECTION_JWT_TOKENS.
	// Value representing the SYD's jwtToken's collection name.
	CollectionJwtTokens = "COLLECTION_JWT_TOKENS"
)
