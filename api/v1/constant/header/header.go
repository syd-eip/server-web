// Constants set for the variables related to the header.
package header

// Constants set for the variables related to the header.
const (
	// Value representing the "Accept" header.
	Accept = "Accept"

	// Value representing the "Authorization" header.
	Authorization = "Authorization"

	// Value representing the "Origin" header.
	Origin = "Origin"

	// Value representing the "Content-Type" header.
	ContentType = "Content-Type"

	// Value representing the "Content-Length" header.
	ContentLength = "Content-Length"

	// Value representing the "Content-Range" header.
	ContentRange = "Content-Range"

	// Value representing the "Range" header.
	Range = "Range"

	// Value representing the "Accept-Encoding" header.
	AcceptEncoding = "Accept-Encoding"

	// Value representing the "Cache-Control" header.
	CacheControl = "Cache-Control"

	// Value representing the "User-Agent" header.
	UserAgent = "User-Agent"

	// Value representing the "Keep-Alive" header.
	KeepAlive = "Keep-Alive"

	// Value representing the "If-Modified-Since" header.
	IfModifiedSince = "If-Modified-Since"

	// Value representing the "DNT" header.
	DNT = "DNT"

	// Value representing the "X-Requested-With" header.
	XRequestedWith = "X-Requested-With"

	// Value representing the "X-CustomHeader" header.
	XCustomHeader = "X-CustomHeader"

	// Value representing the "X-CSRF-Token" header.
	XCSRFToken = "X-CSRF-Token"

	// Value representing the "X-Content-Type-Options" header.
	XContentTypeOptions = "X-Content-Type-Options"

	// Value representing the "X-XSS-Protection" header.
	XXSSProtection = "X-XSS-Protection"

	// Value representing the "X-Frame-Options" header.
	XFrameOptions = "X-Frame-Options"

	// Value representing the "Strict-Transport-Security" header.
	StrictTransportSecurity = "Strict-Transport-Security"

	// Value representing the "Origin-Request-Front" header.
	OriginRequestFront = "Origin-Request-Front"

	// Value representing the "Reset-Token" header.
	ResetToken = "Reset-Token"
)
