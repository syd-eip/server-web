package mailing

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the events' routes.
const (
	// Value representing the tag' route to access all the tags.
	MailRoute = ServerBaseRoute + "/mail"
)
