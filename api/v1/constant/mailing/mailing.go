// Constants set for the mailing credentials of SYD.
package mailing

const (
	// Refers to the environment variable SYD_MAIL_ADDRESS.
	// Value representing the mail address of the SYD's support.
	SydMailAddress = "SYD_MAIL_ADDRESS"

	// Refers to the environment variable SYD_MAIL_PASSWORD.
	// Value representing the mail password of the SYD's support.
	SydMailPassword = "SYD_MAIL_PASSWORD"
)
