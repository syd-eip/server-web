// Constants set for the SYD's documentation.
package doc

import (
	"os"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/server"
)

const (
	// Value representing the SYD's documentation root route.
	DocumentationRoute = ServerBaseRoute + "/documentation/"

	// Refers to the environment variable DOCUMENTATION_FOLDER_PATH.
	// Value representing the SYD's documentation folder path.
	DocumentationFolderPath = "DOCUMENTATION_FOLDER_PATH"
)

// Value representing the index path of the documentation folder
var IndexDocumentationPath = []byte(os.Getenv(DocumentationFolderPath))
