package event

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the events' routes.
const (
	// Value representing the image' route to access all the tags.
	ImageRoute = ServerBaseRoute + "/image"

	DownloadImageRoute = ImageRoute + "/id/{imageId}"
)
