package user

// Constants set for the user.
const (
	// Value representing the default role of the user.
	UserDefaultRole = "USER_DEFAULT_ROLE"
)
