// Constants set for the user.
package user

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the user' /me routes.
const (
	// Value representing the user's "me" route to access to the authenticated user.
	Self = ServerBaseRoute + "/user/me"

	// Value representing the user's "setting" route to access the settings of the authenticated user.
	SelfSetting = Self + "/setting"

	// Value representing the user's "availability" to check the availability of some data of the authenticated user.
	SelfAvailability = Self + "/availability"

	// Value representing the user's "setting/name" route to change the authenticated user first and last names.
	SelfSettingName = SelfSetting + "/name"

	// Value representing the user's "setting/location" route to change the authenticated user location.
	SelfSettingLocation = SelfSetting + "/location"

	// Value representing the user's "setting/birthday" route to change the authenticated user birthday date.
	SelfSettingBirthday = SelfSetting + "/birthday"

	// Value representing the user's "setting/username" route to change the authenticated user username.
	SelfSettingUsername = SelfSetting + "/username"

	// Value representing the user's "setting/email" route to change the authenticated user email.
	SelfSettingEmail = SelfSetting + "/email"

	// Value representing the user's "setting/phone" route to change the authenticated user phone number.
	SelfSettingPhone = SelfSetting + "/phone"

	// Value representing the user's "setting/password" route to change the authenticated user password.
	SelfSettingPassword = SelfSetting + "/password"

	// Value representing the user's "setting/image" route to change the authenticated user one image from his images.
	SelfSettingImage = SelfSetting + "/image"

	// Value representing the user's "availability/username" route to check the availability of a username.
	SelfCheckAvailableUsername = SelfAvailability + "/username"

	// Value representing the user's "availability/email" route to check the availability of a email.
	SelfCheckAvailableEmail = SelfAvailability + "/email"

	// Value representing the user's "availability/phone" route to check the availability of a phone number.
	SelfCheckAvailablePhone = SelfAvailability + "/phone"

	// Value representing the user's "me/wallet/add" route to add token to the wallet of the authenticated user.
	SelfAddTokensUser = Self + "/wallet/add"

	// Value representing the user's "me/favorite" route to access the favorites of the authenticated user.
	SelfFavorite = Self + "/favorite"

	// Value representing the user's "me/favorite/tag" route to access the favorites tags of the authenticated user.
	SelfFavoriteTag = SelfFavorite + "/tag"

	// Value representing the user's "me/favorite/tag/add" route to add favorites tags to the authenticated user.
	SelfFavoriteTagAdd = SelfFavoriteTag + "/add"

	// Value representing the user's "me/favorite/tag/remove" route to remove favorites tags to the authenticated user.
	SelfFavoriteTagRemove = SelfFavoriteTag + "/remove"

	// Value representing the user's "me/favorite/user" route to access the favorites users of the authenticated user.
	SelfFavoriteUser = SelfFavorite + "/user"

	// Value representing the user's "me/favorite/user/add" route to add favorites users to the authenticated user.
	SelfFavoriteUserAdd = SelfFavoriteUser + "/add"

	// Value representing the user's "me/favorite/user/remove" route to remove favorites users to the authenticated user.
	SelfFavoriteUserRemove = SelfFavoriteUser + "/remove"

	// Value representing the user's "me/favorite/association" route to access the favorites associations of the authenticated user.
	SelfFavoriteAssociation = SelfFavorite + "/association"

	// Value representing the user's "me/favorite/association/add" route to add favorites associations to the authenticated user
	SelfFavoriteAssociationAdd = SelfFavoriteAssociation + "/add"

	// Value representing the user's "me/favorite/association/remove" route to remove favorites associations to the authenticated user
	SelfFavoriteAssociationRemove = SelfFavoriteAssociation + "/remove"

	// Value representing the user's "me/discover" route to access the events that may please the authenticated user the most
	SelfDiscover = Self + "/discover"
)
