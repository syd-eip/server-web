package user

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the user' routes.
const (
	// Value representing the user's route to access the data of all the users.
	UserRoute = ServerBaseRoute + "/user"

	// Value representing the user's "sign" route to access the authentication functions.
	UserSignRoute = UserRoute + "/sign"

	// Value representing the user's reset password route to access the reset password function.
	UserResetPasswordRoute = UserRoute + "/reset/password"

	// Value representing the user's check token route to check the token for the reset password.
	UserPasswordCheckTokenRoute = UserResetPasswordRoute + "/check/token"

	// Value representing the user's check token route to check the token for the reset password.
	UserUpdatePasswordWithTokenRoute = UserResetPasswordRoute + "/update"

	// Value representing the user's "id" or "username" route to access a user by its id or username.
	UserByIdOrUsernameRoute = UserRoute + "/id/{user}"

	// Value representing the user's "influencers" route to access all influencers users.
	UserInfluencerRoute = UserRoute + "/influencer/all"

	// Value representing the user's "public" route to access a user by its id or "username".
	UserPublicRoute = UserRoute + "/public/id/{user}"

	// Value representing the user's "setting" route to access the settings of a user by its id or username.
	UserSettingRoute = UserByIdOrUsernameRoute + "/setting"

	// Value representing the user's "sign/up" route to access the sign up form.
	UserSignUpRoute = UserSignRoute + "/up"

	// Value representing the user's "sign/in" route to access the sign in form.
	UserSignInUserRoute = UserSignRoute + "/in"

	// Value representing the user's "sign/out" route to access the sign out function.
	UserSignOutUserRoute = UserSignRoute + "/out"

	// Value representing the user's "setting/name" route to change first and last names of a user by its id or username.
	UserSettingNameRoute = UserSettingRoute + "/name"

	// Value representing the user's "setting/location" route to change the location of a user by its id or username.
	UserSettingLocationRoute = UserSettingRoute + "/location"

	// Value representing the user's "setting/birthday" route to change the birthday of a user by its id or username.
	UserSettingBirthdayRoute = UserSettingRoute + "/birthday"

	// Value representing the user's "setting/username" route to change the username of a user by its id or username.
	UserSettingUsernameRoute = UserSettingRoute + "/username"

	// Value representing the user's "setting/email" route to change the email of a user by its id or username.
	UserSettingEmailRoute = UserSettingRoute + "/email"

	// Value representing the user's "setting/phone" route to change the phone of a user by its id or username.
	UserSettingPhoneRoute = UserSettingRoute + "/phone"

	// Value representing the user's "setting/password" route to change the password of a user by its id or username.
	UserSettingPasswordRoute = UserSettingRoute + "/password"

	// Value representing the user's "setting/image" route to change an image from the ismages of a user by its id or username.
	UserSettingImageRoute = UserSettingRoute + "/image"

	// Value representing the user's "setting/role/add" route to add a user's role by its id or username.
	UserSettingRoleAddRoute = UserSettingRoute + "/role/add"

	// Value representing the user's "setting/role/remove" route to delete a user's role by its id or username.
	UserSettingRoleRemoveRoute = UserSettingRoute + "/role/remove"

	// Value representing the user's "wallet/add" route to add tokens to a user by its id or username.
	UserAddTokensRoute = UserByIdOrUsernameRoute + "/wallet/add"

	// Value representing the user's "wallet/remove" route to remove tokens from a user by its id or username.
	UserRemoveTokensRoute = UserByIdOrUsernameRoute + "/wallet/remove"
)
