// Constants defining the possible roles on the SYD platform.
package role

const (
	// Value representing the USER role.
	UserRole = "USER"

	// Value representing the ADMIN role.
	AdminRole = "ADMIN"

	// Value representing the SUPER_ADMIN role.
	SuperAdminRole = "SUPER_ADMIN"

	// Value representing the INFLUENCER role.
	InfluencerRole = "INFLUENCER"

	// Value representing the GUEST role.
	GuestRole = "GUEST"
)

var RolesTab = []string{UserRole, AdminRole, SuperAdminRole, InfluencerRole, GuestRole}
