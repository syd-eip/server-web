// Constants set for the lotteries's draw.
package draw

import "time"

const (
	// Value representing the minimum time, before the draw date, for the creation of the event.
	MinDiffEventCreationDate = time.Hour * 24
)
