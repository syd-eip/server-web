package tag

// Constants set for the events' routes.
const (
	// Value representing the minimum length of an tag's name.
	MinLengthTagName = 3
)
