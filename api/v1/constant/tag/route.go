package tag

import . "gitlab.com/syd-eip/server-web/api/v1/constant/server"

// Constants set for the events' routes.
const (
	// Value representing the tag' route to access all the tags.
	TagRoute = ServerBaseRoute + "/tag"

	// Value representing the tag' route to create a new tag.
	CreateTagRoute = TagRoute + "/new"

	// Value representing the tag "id" route to access tag by its id or its name.
	TagByIdOrNameRoute = TagRoute + "/id/{tag}"

	// Value representing the tag "id" route to delete a tag by its id or its name.
	DeleteTagRoute = TagByIdOrNameRoute + "/delete"

	// Value representing the tag "id" route to get all tag names.
	GetTagsName = TagRoute + "/name"

	// Value representing the tag settings routes route to update a tag thanks to its id or name.
	EventSettingRoute = TagByIdOrNameRoute + "/setting"

	// Value representing the image settings routes route to update an image from tag thanks to its id or name.
	UpdateImageRoute = EventSettingRoute + "/image"
)
