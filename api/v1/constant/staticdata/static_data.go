// Constants set for the SYD's static data : html and css files, images...
package staticdata

import (
	"os"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/server"
)

const (
	// Value representing the SYD's static data route.
	StaticDataRoute = ServerBaseRoute + "/static/"

	// Refers to the environment variable STATIC_FOLDER_PATH.
	// Value representing the SYD's static folder path.
	StaticFolderPath = "STATIC_FOLDER_PATH"
)

// Value representing the index path of the static folder
var IndexStaticPath = []byte(os.Getenv(StaticFolderPath))
