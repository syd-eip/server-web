package successmessage

// These constants represent the possible successes messages for the Phone.
const (

	// Value representing the success message when the phone number has been correctly updated.
	PhoneUpdated = "SUCCESS/PHONE_NUMBER_UPDATED"

	// Value representing the success message when the phone number is not used by an other user.
	PhoneFree = "SUCCESS/PHONE_NUMBER_FREE"
)
