package successmessage

// These constants represent the possible success messages for the Tags.
const (

	// Value representing the success message when the tag has been correctly updated.
	ImageUpdated = "SUCCESS/IMAGE_UPDATED"
)
