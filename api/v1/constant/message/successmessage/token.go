package successmessage

// These constants represent the possible successes messages for the Tokens.
const (

	// Value representing the success message when tokens have been correctly added.
	TokensAdded = "SUCCESS/TOKENS_ADDED"

	// Value representing the success message when tokens have been correctly removed.
	TokensRemoved = "SUCCESS/TOKENS_REMOVED"
)
