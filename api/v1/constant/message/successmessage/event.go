package successmessage

// These constants represent the possible success messages for the Events.
const (

	// Value representing the success message when the user has been correctly created.
	EventCreated = "SUCCESS/EVENT_CREATED"

	// Value representing the success message when the event has been correctly deleted.
	EventDeleted = "SUCCESS/EVENT_DELETED"

	// Value representing the success message when the event name has been correctly updated.
	EventNameUpdated = "SUCCESS/EVENT_NAME_UPDATED"

	// Value representing the success message when the event description has been correctly updated.
	EventDescriptionUpdated = "SUCCESS/EVENT_DESCRIPTION_UPDATED"

	// Value representing the success message when the event tags have been correctly updated.
	EventTagsUpdated = "SUCCESS/EVENT_TAGS_UPDATED"

	// Value representing the success message when the bet has been correctly added to an event.
	EventBetAdded = "SUCCESS/BET_ADDED"
)
