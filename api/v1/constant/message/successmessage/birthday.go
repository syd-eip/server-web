package successmessage

// These constants represent the possible successes messages for the birthday.
const (

	// Value representing the success message when the birthday has been correctly updated.
	BirthdayUpdated = "SUCCESS/BIRTHDAY_UPDATED"
)
