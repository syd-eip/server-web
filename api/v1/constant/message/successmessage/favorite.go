package successmessage

// These constants represent the possible success messages for the Favorites.
const (

	// Value representing the success message when the favorite tags has been correctly updated.
	FavoriteTagsUpdated = "SUCCESS/FAVORITE_TAGS_UPDATED"

	// Value representing the success message when the favorite users has been correctly updated.
	FavoriteUsersUpdated = "SUCCESS/FAVORITE_USERS_UPDATED"

	// Value representing the success message when the favorite associations has been correctly updated.
	FavoriteAssociationsUpdated = "SUCCESS/FAVORITE_ASSOCIATIONS_UPDATED"
)
