package successmessage

// These constants represent the possible successes messages for the User names.
const (

	// Value representing the success message when the username has been correctly updated.
	UsernameUpdated = "SUCCESS/USERNAME_UPDATED"

	// Value representing the success message when the first and last names have been correctly updated.
	UserNameUpdated = "SUCCESS/NAME_UPDATED"

	// Value representing the success message when the username is not used by an other user.
	UsernameFree = "SUCCESS/USERNAME_FREE"
)
