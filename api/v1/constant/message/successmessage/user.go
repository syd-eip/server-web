package successmessage

// These constants represent the possible successes messages for the User.
const (

	// Value representing the success message when the user is still authenticated.
	UserAuthenticated = "SUCCESS/USER_AUTHENTICATED"

	// Value representing the success message when the user is still successfully disconnected.
	UserDisconnected = "SUCCESS/USER_DISCONNECTED"

	// Value representing the success message when the user has been correctly created.
	UserCreated = "SUCCESS/USER_CREATED"

	// Value representing the success message when the user has been correctly deleted.
	UserDeleted = "SUCCESS/USER_DELETED"
)
