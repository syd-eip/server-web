package successmessage

// These constants represent the possible successes messages for the password.
const (

	// Value representing the success message when the password has been correctly updated.
	PasswordUpdated = "SUCCESS/PASSWORD_UPDATED"

	// Value representing the success message when the password has been correctly reset.
	PasswordReset = "SUCCESS/PASSWORD_RESET"
)
