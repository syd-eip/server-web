package successmessage

// These constants represent the possible successes messages for the Email.
const (
	// Value representing the success message when the email has been correctly updated.
	EmailUpdated = "SUCCESS/EMAIL_UPDATED"

	// Value representing the success message when the email is not used by an other user.
	EmailFree = "SUCCESS/EMAIL_FREE"
)
