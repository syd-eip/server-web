package successmessage

// These constants represent the possible success messages for the Tags.
const (

	// Value representing the success message when the tag has been correctly created.
	TagCreated = "SUCCESS/TAG_CREATED"

	// Value representing the success message when the tag has been correctly deleted.
	TagDeleted = "SUCCESS/TAG_DELETED"
	)
