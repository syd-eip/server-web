package successmessage

// These constants represent the possible success messages for the Associations.
const (

	// Value representing the success message when the association has been correctly created.
	AssociationCreated = "SUCCESS/ASSOCIATION_CREATED"

	// Value representing the success message when the association has been correctly deleted.
	AssociationDeleted = "SUCCESS/ASSOCIATION_DELETED"

	// Value representing the success message when the association's name has been correctly updated.
	AssociationNameUpdated = "SUCCESS/ASSOCIATION_NAME_UPDATED"

	// Value representing the success message when the association's description has been correctly updated.
	AssociationDescriptionUpdated = "SUCCESS/ASSOCIATION_DESCRIPTION_UPDATED"
)
