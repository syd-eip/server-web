package successmessage

// These constants represent the possible successes messages for the location.
const (

	// Value representing the success message when the location has been correctly updated.
	LocationUpdated = "SUCCESS/LOCATION_UPDATED"
)
