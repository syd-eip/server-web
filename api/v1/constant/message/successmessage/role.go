package successmessage

// These constants represent the possible success messages for the Roles.
const (

	// Value representing the success message when the role has been correctly added.
	RoleAdded = "SUCCESS/ROLE_ADDED"

	// Value representing the success message when the role has been correctly removed.
	RoleRemoved = "SUCCESS/ROLE_REMOVED"
)
