package successmessage

// These constants represent the possible successes messages.
const (

	// Value representing the success message when the request works.
	OK = "SUCCESS/OK"
)
