package errormessage

// These constants represent the possible error messages for the Tags.
const (

	// Value representing the error message when the tag name is bad.
	TagNameBadFormat = "ERROR/TAG_NAME_BAD_FORMAT"

	// Value representing the error message when the tag name is too short.
	TagNameMinLength = "ERROR/TAG_NAME_MIN_LENGTH"

	// Value representing the error message when the tag name is already used.
	TagNameAlreadyUsed = "ERROR/TAG_NAME_ALREADY_USED"

	// Value representing the error message when the tag name field is not filled.
	TagNameMissing = "ERROR/TAG_NAME_MISSING"

	// Value representing the error message when the tag can not be found.
	TagNotFound = "ERROR/TAG_NOT_FOUND"
)
