// Constants defining the messages sent by SYD's API in its responses.
package errormessage

// These constants represent the possible error messages for the Lottery.
const (

	// Value representing the error message when the Lottery can not be found.
	LotteryNotFound = "ERROR/LOTTERY_NOT_FOUND"

	// Value representing the error message when the Lottery can not be found.
	BetNotFound = "ERROR/BET_NOT_FOUND"

	// Value representing the error message when the value added is less or equal to zero
	BetNegativeOrNull = "ERROR/BET_NEGATIVE_OR_NULL"
)
