package errormessage

// These constants represent the possible error messages for the Password token.
const (

	// Value representing the error message when the token is missing in the header.
	ResetPasswordTokenMissing = "ERROR/RESET_PASSWORD_TOKEN_MISSING"

	// Value representing the error message when the token is invalid.
	TokenInvalid = "ERROR/TOKEN_INVALID"

	// Value representing the error message when the server failed to get information from claims.
	CantGetFromClaims = "Can't get from claims"

	// Value representing the error message when the server failed to parse the token.
	ParsingToken = "Error parsing token"
)
