package errormessage

// These constants represent the possible error messages for the Mailing.
const (

	// Value representing the error message when there is no subject to the mail sent.
	SubjectMissing = "ERROR/SUBJECT_MISSING"

	// Value representing the error message when there is no politeness formula to the mail sent.
	BodyMissing = "ERROR/BODY_MISSING"

	// Value representing the error message when there is no politeness formula to the mail sent.
	PolitenessFormulaMissing = "ERROR/POLITENESS_FORMULA_MISSING"

	// Value representing the error message when there is no signature to the mail sent.
	SignatureMissing = "ERROR/SIGNATURE_MISSING"

	// Value representing the error message when there are no receivers to the mail sent.
	ReceiversMissing = "ERROR/RECEIVERS_MISSING"
)
