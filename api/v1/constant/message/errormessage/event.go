package errormessage

// These constants represent the possible error messages for the Events.
const (

	// Value representing the error message when the event name is bad.
	EventNameBadFormat = "ERROR/EVENT_NAME_BAD_FORMAT"

	// Value representing the error message when the event name is already used.
	EventNameAlreadyUsed = "ERROR/EVENT_NAME_ALREADY_USED"

	// Value representing the error message when the event name field is not filled.
	EventNameMissing = "ERROR/EVENT_NAME_MISSING"

	// Value representing the error message when the event id field is not filled.
	EventIdMissing = "ERROR/EVENT_ID_MISSING"

	// Value representing the error message when the event name field is too short.
	EventNameMinLength = "ERROR/EVENT_NAME_MIN_LENGTH"

	// Value representing the error message when the event eventDate field is not filled.
	EventEventDateMissing = "ERROR/EVENT_EVENT_DATE_MISSING"

	// Value representing the error message when the event eventDate field is not filled.
	EventDrawDateMissing = "ERROR/EVENT_DRAW_DATE_MISSING"

	// Value representing the error message when the event can not be found.
	EventNotFound = "ERROR/EVENT_NOT_FOUND"

	// Value representing the error message when the date is badly formatted.
	EventEventDateBadFormat = "ERROR/EVENT_EVENT_DATE_BAD_FORMAT"

	// Value representing the error message when the date is badly formatted.
	EventDrawDateBadFormat = "ERROR/EVENT_DRAW_DATE_BAD_FORMAT"

	// Value representing the error message when the draw date of the event is to close to the creation of the event.
	EventDrawDateTooEarly = "ERROR/DRAW_DATE_TOO_EARLY"

	// Value representing the error message when the event is created less then 24 hours before it takes place.
	EventEventDateTooEarly = "ERROR/EVENT_DATE_TOO_EARLY"

	// Value representing the error message when the number of draw it less than 1.
	EventLotteriesSizeMinLength = "ERROR/EVENT_LOTTERIES_SIZE_MIN_LENGTH"
)
