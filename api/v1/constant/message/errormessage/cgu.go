package errormessage

// These constants represent the possible error messages for the CGU.
const (

	// Value representing the error message when the CGU are not validated.
	CguMustBeValidated = "ERROR/CGU_NOT_VALIDATED"
)
