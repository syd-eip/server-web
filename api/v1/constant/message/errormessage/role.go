package errormessage

// These constants represent the possible error messages for the Roles.
const (

	// Value representing the error message when the role field is not fill.
	RoleMissing = "ERROR/ROLE_MISSING"

	// Value representing the error message when the role sent in the body does not exist.
	RoleNotExist = "ERROR/ROLE_NOT_EXIST"

	// Value representing the error message when the user is a guest and do not have access to SYD.
	UserIsGuest = "ERROR/USER_IS_GUEST"

	// Value representing the error message when the user has not the role USER.
	UserNotUser = "ERROR/USER_NOT_USER"

	// Value representing the error message when the user has not the role INFLUENCER.
	UserNotInfluencer = "ERROR/USER_NOT_INFLUENCER"

	// Value representing the error message when the user has not the role ADMIN.
	UserNotAdmin = "ERROR/USER_NOT_ADMIN"

	// Value representing the error message when the user has not the role SUPER_ADMIN.
	UserNotSuperAdmin = "ERROR/USER_NOT_SUPER_ADMIN"
)
