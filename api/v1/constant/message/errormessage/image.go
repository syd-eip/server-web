package errormessage

// These constants represent the possible error messages for the Tags.
const (

	// Value representing the error message when the image name field is not filled.
	ImageNameMissing = "ERROR/IMAGE_NAME_MISSING"

	// Value representing the error message when the image can not be found.
	ImageNotFound = "ERROR/IMAGE_NOT_FOUND"

	// Value representing the error message when the image content contains error.
	ImageCorrupted = "ERROR/IMAGE_CORRUPTED"
)
