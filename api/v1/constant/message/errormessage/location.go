package errormessage

// These constants represent the possible error messages for the Location.
const (

	// Value representing the error message when the location's countryCode field is not filled.
	LocationCountryCodeMissing = "ERROR/LOCATION_COUNTRY_CODE_MISSING"

	// Value representing the error message when the location's city field is not filled.
	LocationCityMissing = "ERROR/LOCATION_CITY_MISSING"

	// Value representing the error message when the location's street address field is not filled.
	LocationStreetAddressMissing = "ERROR/LOCATION_STREET_ADDRESS_MISSING"

	// Value representing the error message when the location's zip code field is not filled.
	LocationZipCodeMissing = "ERROR/LOCATION_ZIP_CODE_MISSING"

	// Value representing the error message when the location is badly formatted.
	LocationBadFormat = "ERROR/LOCATION_BAD_FORMAT"

	// Value representing the error message when the countryCode does not exist.
	LocationCountryCodeNotExist = "ERROR/LOCATION_COUNTRY_CODE_NOT_EXIST"
)
