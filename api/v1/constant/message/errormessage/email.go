package errormessage

// These constants represent the possible error messages for the Email.
const (

	// Value representing the error message when the email is badly formatted.
	EmailBadFormat = "ERROR/EMAIL_BAD_FORMAT"

	// Value representing the error message when the email does not exist.
	EmailNotExist = "ERROR/EMAIL_DOES_NOT_EXIST"

	// Value representing the error message when the email field is not fill.
	EmailMissing = "ERROR/EMAIL_MISSING"

	// Value representing the error message when the email is already used by an other user.
	EmailAlreadyUsed = "ERROR/EMAIL_ALREADY_USED"
)
