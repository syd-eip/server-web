package errormessage

// These constants represent the possible error messages for the User names.
const (

	// Value representing the error message when the username is already used by an other user.
	UsernameAlreadyUsed = "ERROR/USERNAME_ALREADY_USED"

	// Value representing the error message when the username field is not fill.
	UsernameMissing = "ERROR/USERNAME_MISSING"

	// Value representing the error message when the first name field is not fill.
	FirstNameMissing = "ERROR/FIRST_NAME_MISSING"

	// Value representing the error message when the first name is badly formatted.
	FirstNameBadFormat = "ERROR/FIRST_NAME_BAD_FORMAT"

	// Value representing the error message when the last name field is not fill.
	LastNameMissing = "ERROR/LAST_NAME_MISSING"

	// Value representing the error message when the last name is badly formatted.
	LastNameBadFormat = "ERROR/LAST_NAME_BAD_FORMAT"
)
