package errormessage

// These constants represent the possible error messages for the Favorites.
const (

	// Value representing the error message when the favoriteTag is empty.
	FavoriteTagMissing = "ERROR/FAVORITE_TAG_MISSING"

	// Value representing the error message when the favoriteAssociation is empty.
	FavoriteAssociationMissing = "ERROR/FAVORITE_ASSOCIATION_MISSING"

	// Value representing the error message when the favoriteUser is empty.
	FavoriteUserMissing = "ERROR/FAVORITE_USER_MISSING"
)
