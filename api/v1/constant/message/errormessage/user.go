package errormessage

// These constants represent the possible error messages for the User.
const (

	// Value representing the error message when the user can not be found.
	UserNotFound = "ERROR/USER_NOT_FOUND"

	// Value representing the error message when the user is not authorized to perform a request on SYD.
	UserNotAuthorized = "ERROR/USER_NOT_AUTHORIZED"
)
