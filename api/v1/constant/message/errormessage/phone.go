package errormessage

// These constants represent the possible error messages for the Phone.
const (

	// Value representing the error message when the phone is badly formatted.
	PhoneBadFormat = "ERROR/PHONE_BAD_FORMAT"

	// Value representing the error message when the phone is already used by an other user.
	PhoneAlreadyUsed = "ERROR/PHONE_ALREADY_USED"

	// Value representing the error message when the phone country code field is not filled.
	PhoneCountryCodeMissing = "ERROR/PHONE_COUNTRY_CODE_MISSING"

	// Value representing the error message when the phone number field is not filled.
	PhoneNumberMissing = "ERROR/PHONE_NUMBER_MISSING"
)
