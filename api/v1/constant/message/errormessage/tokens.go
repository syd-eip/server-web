// Constants defining the messages sent by SYD's API in its responses.
package errormessage

// These constants represent the possible error messages for the Tokens.
const (

	// Value representing the error message when the value requested is less or equal to zero
	ValueNegativeOrNull = "ERROR/VALUE_NEGATIVE_OR_NULL"

	// Value representing the error message when the role sent in the body does not exist.
	UserNotEnoughTokens = "ERROR/USER_NOT_ENOUGH_TOKENS"
)
