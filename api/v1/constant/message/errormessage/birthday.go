package errormessage

// These constants represent the possible error messages for the Birthday.
const (

	// Value representing the error message when the birthday field is not filled.
	BirthdayMissing = "ERROR/BIRTHDAY_MISSING"

	// Value representing the error message when the birthday date is badly formatted.
	BirthdayBadFormat = "ERROR/BIRTHDAY_BAD_FORMAT"

	// Value representing the error message when the user is not old enough to register on SYD.
	BirthdayNotLegalAge = "ERROR/BIRTHDAY_NOT_LEGAL_AGE"
)
