package errormessage

// These constants represent the possible error messages for the Associations.
const (

	// Value representing the error message when the association name is already used.
	AssociationNameAlreadyUsed = "ERROR/ASSOCIATION_NAME_ALREADY_USED"

	// Value representing the error message when the association name field is not filled.
	AssociationNameMissing = "ERROR/ASSOCIATION_NAME_MISSING"

	// Value representing the error message when the association name field is not filled.
	AssociationMissing = "ERROR/ASSOCIATION_MISSING"

	// Value representing the error message when the association can not be found.
	AssociationNotFound = "ERROR/ASSOCIATION_NOT_FOUND"
)
