package errormessage

// These constants represent the possible error messages for the Password.
const (

	// Value representing the error message when the password is not corresponding to the login.
	PasswordBad = "ERROR/PASSWORD_BAD"

	// Value representing the error message when the password field is not fill.
	PasswordMissing = "ERROR/PASSWORD_MISSING"

	// Value representing the error message when the confirm password field is not fill.
	ConfirmPasswordMissing = "ERROR/CONFIRM_PASSWORD_MISSING"

	// Value representing the error message when the password and confirm password are not identical.
	ConfirmPasswordMatch = "ERROR/CONFIRM_PASSWORD_MATCH"

	// Value representing the error message when the new password and old password are identical.
	NewPasswordMatchOld = "ERROR/NEW_PASSWORD_MATCH_OLD"

	// Value representing the error message when the length of the password is to small.
	PasswordMinLength = "ERROR/PASSWORD_MIN_LENGTH"

	// Value representing the error message when the length of the password is to big.
	PasswordMaxLength = "ERROR/PASSWORD_MAX_LENGTH"

	// Value representing the error message when the composition of the password is not good.
	PasswordComposition = "ERROR/PASSWORD_COMPOSITION"
)
