package errormessage

// These constants represent the possible error messages for the Login.
const (

	// Value representing the error message when the login is not corresponding to a known login.
	LoginBad = "ERROR/LOGIN_BAD"

	// Value representing the error message when the login field is not fill.
	LoginMissing = "ERROR/LOGIN_MISSING"
)
