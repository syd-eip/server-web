package errormessage

// These constants represent the possible error messages for the request Method.
const (

	// Value representing the error message when the request method is not allowed.
	MethodNotAllowed = "ERROR/METHOD_NOT_ALLOWED"
)
