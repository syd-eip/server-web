// This package connects to "syd" database.
// It also contains all queries regarding "association" routes.
package association

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/nyaruka/phonenumbers"
	"gitlab.com/syd-eip/server-web/api/v1/controller/image"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	midformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		Trim all trailing spaces in a AssociationSignUp model.
// Parameters :
//		associationSignUp *model.AssociationSignUp :
//			AssociationSignUp where the strings have to be cleaned.
func TrimAssociationSignUp(associationSignUp *model.AssociationSignUp) {
	associationSignUp.Name = strings.TrimSpace(associationSignUp.Name)
	associationSignUp.Email = strings.TrimSpace(associationSignUp.Email)
	associationSignUp.Phone.CountryCode = strings.TrimSpace(associationSignUp.Phone.CountryCode)
	associationSignUp.Phone.NationalNumber = strings.TrimSpace(associationSignUp.Phone.NationalNumber)
	associationSignUp.Location.CountryCode = strings.TrimSpace(associationSignUp.Location.CountryCode)
	associationSignUp.Location.City = strings.TrimSpace(associationSignUp.Location.City)
	associationSignUp.Location.ZipCode = strings.TrimSpace(associationSignUp.Location.ZipCode)
	associationSignUp.Location.StreetAddress = strings.TrimSpace(associationSignUp.Location.StreetAddress)
}

// Description
//		Method POST.
//		This function registers a new association on SYD and saves it in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func CreateAssociation(w http.ResponseWriter, r *http.Request) {
	var associationSignUp model.AssociationSignUp
	var association model.Association

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationSignUp)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	TrimAssociationSignUp(&associationSignUp)

	numberProto, _ := phonenumbers.Parse(associationSignUp.Phone.NationalNumber, associationSignUp.Phone.CountryCode)
	associationSignUp.Phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
	associationSignUp.Phone.Number = associationSignUp.Phone.InternationalNumber

	association = response.ConvertAssociationSignUpToAssociation(associationSignUp)

	association.Id = bson.NewObjectId()
	association.Location.Country = midformat.GetCountryname(association.Location.CountryCode)
	association.Images = make(map[string]string)

	if err := DaoAssociation.Insert(association); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusCreated, successmessage.AssociationCreated, nil)
}

// Description
//		Method DELETE.
//		This function delete a association on SYD database thanks to his "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func DeleteAssociation(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}

	err = DaoAssociation.Delete(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.AssociationDeleted, nil)
}

// Description
//		Method GET.
//		This function get all associations on SYD in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetAllAssociations(w http.ResponseWriter, r *http.Request) {
	associations, err := DaoAssociation.FindAll()
	if err != nil {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, associations)
}

// Description
//		Method GET.
//		This function recovers an association in "syd" database thanks to his "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetAssociation(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, association)
}

// Description :
//		Method PATCH.
//		This function updates the name of a association thanks to his "id" or "name".
//		All the checks about the Name have be done in the middleware "CheckFieldsName".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateAssociationName(w http.ResponseWriter, r *http.Request) {
	var associationName model.AssociationName

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationName)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}
	associationName.Name = strings.TrimSpace(associationName.Name)

	association.Name = associationName.Name
	err = DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.AssociationNameUpdated, association.Name)
}

// Description :
//		Method PATCH.
//		This function updates the first and last description of a association thanks to his "id" or "name".
//		All the checks about the Description have be done in the middleware "CheckFieldsDescription".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateAssociationDescription(w http.ResponseWriter, r *http.Request) {
	var associationDescription model.AssociationDescription

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationDescription)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}

	association.Description = associationDescription.Description
	err = DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.AssociationDescriptionUpdated, association.Description)
}

// Description :
//		Method PATCH.
//		This function updates the email of a association thanks to his "id" or "name".
//		All the checks about the Email have be done in the middleware "CheckFieldsEmail".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateAssociationEmail(w http.ResponseWriter, r *http.Request) {
	var associationEmail model.AssociationEmail

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationEmail)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}
	associationEmail.Email = strings.TrimSpace(associationEmail.Email)

	association.Email = associationEmail.Email
	err = DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.EmailUpdated, association.Email)
}

// Description :
//		Method PATCH.
//		This function updates the location of a association thanks to his "id" or "name".
//		All the checks about the Location have be done in the middleware "CheckFieldsLocation".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateAssociationLocation(w http.ResponseWriter, r *http.Request) {
	var associationLocation model.Location

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationLocation)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}
	associationLocation.City = strings.TrimSpace(associationLocation.City)
	associationLocation.ZipCode = strings.TrimSpace(associationLocation.ZipCode)
	associationLocation.CountryCode = strings.TrimSpace(associationLocation.CountryCode)
	associationLocation.StreetAddress = strings.TrimSpace(associationLocation.StreetAddress)

	association.Location = associationLocation
	association.Location.Country = midformat.GetCountryname(associationLocation.CountryCode)
	err = DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.LocationUpdated, association.Location)
}

// Description :
//		Method PATCH.
//		This function updates the phone number of a association thanks to his "id" or "name".
//		All the checks about the Phone have be done in the middleware "CheckFieldsPhone".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateAssociationPhone(w http.ResponseWriter, r *http.Request) {
	var associationPhone model.Phone

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&associationPhone)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}
	associationPhone.CountryCode = strings.TrimSpace(associationPhone.CountryCode)
	associationPhone.NationalNumber = strings.TrimSpace(associationPhone.NationalNumber)

	association.Phone = associationPhone
	numberProto, _ := phonenumbers.Parse(associationPhone.NationalNumber, associationPhone.CountryCode)
	association.Phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
	association.Phone.Number = association.Phone.InternationalNumber
	err = DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.PhoneUpdated, association.Phone)
}

// Description
//		Method PATCH.
//		This function updates an image of an association thanks to its "id" or its "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func UpdateAssociationImage(w http.ResponseWriter, r *http.Request) {
	// Get User from Id
	params := mux.Vars(r)
	association, err := DaoAssociation.FindByIdSendBack(params["association"])
	if err != nil {
		association, err = DaoAssociation.FindByNameSendBack(params["association"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.AssociationNotFound, nil)
			return
		}
	}

	// Get Images From Body
	var imageRequest model.UploadRequestImage
	defer r.Body.Close()

	decodeErr := json.NewDecoder(r.Body).Decode(&imageRequest)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}

	imageRequest.Name = strings.TrimSpace(imageRequest.Name)
	if len(imageRequest.Content) == 0 {
		// Delete Image
		var deleteErr error
		if association, deleteErr = deleteImageFromAssociation(imageRequest, association); deleteErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, deleteErr.Error(), nil)
			return
		}
	} else {
		// Update Image
		var updateErr error
		if association, updateErr = updateImageFromAssociation(imageRequest, association); updateErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
			return
		}
	}

	// Update Association
	updateErr := DaoAssociation.Update(association)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.ImageUpdated, association.Images)
}

// Description
//		This function updates an image of an association thanks to its "id" or its "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func updateImageFromAssociation(imageRequest model.UploadRequestImage, association model.Association) (model.Association, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = association.Id.Hex() + "_" + imageRequest.Name

	if uploadErr := image.DaoImage.Update(imageRequest); uploadErr != nil {
		return association, uploadErr
	}

	if association.Images == nil {
		association.Images = make(map[string]string)
	}

	association.Images[imageKey] = imageRequest.Name

	return association, nil
}

// Description
//		This function deletes an image of an association thanks to its "id" or its "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func deleteImageFromAssociation(imageRequest model.UploadRequestImage, association model.Association) (model.Association, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = association.Id.Hex() + "_" + imageRequest.Name

	if updateErr := image.DaoImage.Delete(imageRequest.Name); updateErr != nil {
		return association, updateErr
	}

	if association.Images == nil {
		return association, errors.New(errormessage.ImageNotFound)
	}

	if _, ok := association.Images[imageKey]; !ok {
		return association, errors.New(errormessage.ImageNotFound)
	}

	delete(association.Images, imageKey)

	return association, nil
}
