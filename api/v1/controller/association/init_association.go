package association

import (
	"gitlab.com/syd-eip/server-web/api/v1/repository/association"
)

var DaoAssociation = association.DbAssociationsStruct{}

//Connection to SYD database and Tag collection
func init() {
	DaoAssociation.Connect()
}
