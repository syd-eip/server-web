// This package connects to "syd" database.
package mailing

import (
	"gitlab.com/syd-eip/server-web/api/v1/repository/jwtToken"
	"gitlab.com/syd-eip/server-web/api/v1/repository/user"
)

// Connection to the "syd" database and users collection.
var DaoUser = user.DbUsersStruct{}

// Connection to the "syd" database and jwtToken collection.
var DaoJwt = jwtToken.DbJwtTokensStruct{}

// Description :
//		Connection to "syd" database and to the "users" and "jwtTokens" collection.
func init() {
	DaoUser.Connect()
	DaoJwt.Connect()
}
