package mailing

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/smtp"
	"os"
	"strings"

	"mime/quotedprintable"

	"gitlab.com/syd-eip/server-web/api/v1/constant/mailing"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
)

const (
	// Gmail SMTP Server
	SMTPServer = "smtp.gmail.com"
)

type Sender struct {
	User     string
	Password string
}

func NewSender(UserMail, Password string) Sender {

	return Sender{UserMail, Password}
}

func (sender Sender) SendMail(Dest []string, Subject, bodyMessage string) (bool, string) {

	msg := "From: " + sender.User + "\n" +
		"To: " + strings.Join(Dest, ",") + "\n" +
		"Subject: " + Subject + "\n" + bodyMessage

	err := smtp.SendMail(SMTPServer+":587",
		smtp.PlainAuth("", sender.User, sender.Password, SMTPServer),
		sender.User, Dest, []byte(msg))

	if err != nil {
		return false, "ERROR: " + err.Error()
	}

	return true, "SUCCESS"
}

func (sender Sender) WriteEmail(dest []string, contentType, subject, bodyMessage string) string {

	header := make(map[string]string)
	header["From"] = sender.User

	recipient := ""

	for _, user := range dest {
		recipient = recipient + user
	}

	header["To"] = recipient
	header["Subject"] = subject
	header["MIME-Version"] = "1.0"
	header[headerConst.ContentType] = fmt.Sprintf("%s; charset=\"utf-8\"", contentType)
	header["Content-Transfer-Encoding"] = "quoted-printable"
	header["Content-Disposition"] = "inline"

	message := ""

	for key, value := range header {
		message += fmt.Sprintf("%s: %s\r\n", key, value)
	}

	var encodedMessage bytes.Buffer

	finalMessage := quotedprintable.NewWriter(&encodedMessage)
	_, _ = finalMessage.Write([]byte(bodyMessage))
	_ = finalMessage.Close()

	message += "\r\n" + encodedMessage.String()

	return message
}

func (sender *Sender) WriteHTMLEmail(dest []string, subject, bodyMessage string) string {

	return sender.WriteEmail(dest, "text/html", subject, bodyMessage)
}

func (sender *Sender) WritePlainEmail(dest []string, subject, bodyMessage string) string {

	return sender.WriteEmail(dest, "text/plain", subject, bodyMessage)
}

// Description :
//		Method POST.
//		This function sends a mail to one or more user(s) from SYD's mail.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SendMailFromScratch(w http.ResponseWriter, r *http.Request) {
	var mail model.MailingStructure

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&mail)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	sender := NewSender(os.Getenv(mailing.SydMailAddress), os.Getenv(mailing.SydMailPassword))

	message := `
	<!doctype html>
	<html lang="en">
	<head>
	<meta charset="utf-8"/>
	<meta http-equiv="content-type" content="text/html" ; charset=ISO-8859-1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	</head>
	<body>
	<div>
	<p>` + mail.Body + `</p>
	<div class="moz-signature">
	<i><br><br>` + mail.PolitenessFormula + `<br>` + mail.Signature + `<br></i>
	</div>
	</div>
	</body>
	</html>
	`
	bodyMessage := sender.WriteHTMLEmail(mail.Receivers, mail.Subject, message)

	var responseData []model.MailingResponse

	for _, receiver := range mail.Receivers {
		var elem model.MailingResponse
		elem.Mail = receiver

		isSent, _ := sender.SendMail([]string{receiver}, mail.Subject, bodyMessage)
		if isSent {
			elem.Message = "SUCCESS"
		} else {
			elem.Message = "ERROR"
		}

		responseData = append(responseData, elem)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, responseData)
}
