package user

import (
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/nyaruka/phonenumbers"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/syd-eip/server-web/api/v1/constant/cookie"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/controller/image"
	"gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
	roleNames "gitlab.com/syd-eip/server-web/api/v1/constant/role"
	userConst "gitlab.com/syd-eip/server-web/api/v1/constant/user"
	middleFormat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		This function selects the appropriate function, depending on the request method.
//		It calls "getUser" if the method is GET.
//		It calls "deleteUser" if the method is DELETE.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetOrDeleteUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		getUser(w, r)
	case "DELETE":
		deleteUser(w, r)
	}
}

// Description :
//		Method GET.
//		This function recovers a detailed user from "syd" database thanks to his "id" or "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func getUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	details := response.ConvertUserToUserDetailed(user)
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, details)
}

// Description :
//		Method GET.
//		This function recovers a public user from "syd" database thanks to his "id" or "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserPublic(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	public := response.ConvertUserToUserPublic(user)
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, public)
}

// Description :
//		Method DELETE.
//		This function deletes a user from "syd" database thanks to his "id" or "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func deleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	err = DaoUser.Delete(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.UserDeleted, nil)
}

// Description :
//		Method GET.
//		This function gets a list of all detailed users of SYD from "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, err := DaoUser.FindAll()
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	var details []model.UserDetailed
	for _, elem := range users {
		details = append(details, response.ConvertUserToUserDetailed(elem))
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, details)
}

// Description :
//		Method GET.
//		This function gets a list of all the influencers users of SYD from "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetAllInfluencers(w http.ResponseWriter, r *http.Request) {
	users, err := DaoUser.FindAll()
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	var influencers []model.UserPublic
	for _, elem := range users {
		if role.ContainsRole(elem.Role, roleNames.InfluencerRole) != -1 {
			influencers = append(influencers, response.ConvertUserToUserPublic(elem))
		}
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, influencers)
}

// Description :
//		Trim all trailing spaces in a UserSignUp model.
// Parameters :
//		userSignUp *model.UserSignUp :
//			UserSignUp where the strings have to be cleaned.
func TrimUserSignUp(userSignUp *model.UserSignUp) {
	userSignUp.FirstName = strings.TrimSpace(userSignUp.FirstName)
	userSignUp.LastName = strings.TrimSpace(userSignUp.LastName)
	userSignUp.Username = strings.TrimSpace(userSignUp.Username)
	userSignUp.Birthday = strings.TrimSpace(userSignUp.Birthday)
	userSignUp.Email = strings.TrimSpace(userSignUp.Email)
	userSignUp.Phone.CountryCode = strings.TrimSpace(userSignUp.Phone.CountryCode)
	userSignUp.Phone.NationalNumber = strings.TrimSpace(userSignUp.Phone.NationalNumber)
	userSignUp.Location.CountryCode = strings.TrimSpace(userSignUp.Location.CountryCode)
}

// Description :
//		Method POST.
//		This function registers a user on SYD and saves him in "syd" database.
//		All the checks about the User sign up model have be done in the middleware "CheckFieldsSignUp".
//		So, we can here directly use a User model.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SignUpUser(w http.ResponseWriter, r *http.Request) {
	var userSignUp model.UserSignUp
	var user model.User

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userSignUp)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	TrimUserSignUp(&userSignUp)

	if userSignUp.Password, err = security.HashPassword(userSignUp.Password); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	numberProto, _ := phonenumbers.Parse(userSignUp.Phone.NationalNumber, userSignUp.Phone.CountryCode)
	userSignUp.Phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
	userSignUp.Phone.Number = userSignUp.Phone.InternationalNumber

	userSignUp.Location.City = ""
	userSignUp.Location.ZipCode = ""
	userSignUp.Location.StreetAddress = ""
	userSignUp.Location.Country = middleFormat.GetCountryname(userSignUp.Location.CountryCode)

	user = response.ConvertUserSignUpToUser(userSignUp)

	user.Id = bson.NewObjectId()
	user.Role = append(user.Role, os.Getenv(userConst.UserDefaultRole))
	user.Images = make(map[string]string)

	if err := DaoUser.Insert(user); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusCreated, successmessage.UserCreated, nil)
}

// Description :
//		Method POST.
//		This function authenticates a user on SYD.
//		It creates a Cookie with a JWToken inside to keep him logged in.
//		All the checks about the User sign in model have be done in the middleware "CheckFieldsSignIn".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SignInUser(w http.ResponseWriter, r *http.Request) {
	var user model.User
	var token string
	var userSignIn model.UserSignIn

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userSignIn)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	userSignIn.Login = strings.TrimSpace(userSignIn.Login)

	if userUsername, errUsername := DaoUser.FindByUsernameSendBackUser(userSignIn.Login); errUsername != nil {
		user, _ = DaoUser.FindByEmailSendBackUser(userSignIn.Login)
	} else {
		user = userUsername
	}

	token, err = security.GenerateJWTUserToken(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	CookieSecure, err := strconv.ParseBool(os.Getenv(cookie.CookieTokenUserSecureValue))
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	CookieHttp, err := strconv.ParseBool(os.Getenv(cookie.CookieTokenUserHttpValue))
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	expire := time.Now().Add(cookie.CookieTokenUserExpire)

	domain := cookie.CookieTokenUserDomain

	r.Header.Add(headerConst.OriginRequestFront, "localhost")

	if r.Header.Get(headerConst.OriginRequestFront) == "local.shareyourdreams.fr" {
		CookieSecure = false
	}

	if r.Header.Get(headerConst.OriginRequestFront) == "localhost" {
		domain = "localhost"
		CookieSecure = false
	}

	cookieTokenUser := http.Cookie{
		Name:     cookie.CookieTokenUserName,
		Value:    token,
		Path:     cookie.CookieTokenUserPath,
		Expires:  expire,
		MaxAge:   cookie.CookieTokenUserMaxAge,
		Domain:   domain,
		SameSite: http.SameSiteLaxMode,
		HttpOnly: CookieHttp,
		Secure:   CookieSecure}
	http.SetCookie(w, &cookieTokenUser)

	// This cookie is a copy of the cookieTokenUser cookie
	// But it doesn't contained the value of the token
	// And it's not in HTTP only, so that the header can check if the user is still authenticated.
	cookieAuthenticationExpiration := http.Cookie{
		Name:     cookie.CookieAuthenticationName,
		Value:    "true",
		Path:     cookie.CookieTokenUserPath,
		Expires:  expire,
		MaxAge:   cookie.CookieTokenUserMaxAge,
		Domain:   domain,
		SameSite: http.SameSiteLaxMode,
		HttpOnly: false,
		Secure:   CookieSecure}
	http.SetCookie(w, &cookieAuthenticationExpiration)

	details := response.ConvertUserToUserDetailed(user)
	response.RespondWithJson(w, http.StatusOK, successmessage.UserAuthenticated, details)
}

// Description :
//		Method POST.
//		This function disconnects a user from SYD.
//		It invalidates his Cookie to prevent him from reconnecting to SYD thanks to his web browser.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SignOutUser(w http.ResponseWriter, r *http.Request) {
	expire := time.Now().Add(-1)
	cookieToSet := http.Cookie{Name: cookie.CookieTokenUserName, Value: "", Domain: cookie.CookieTokenUserDomain, Path: "/", Expires: expire, MaxAge: -1}
	http.SetCookie(w, &cookieToSet)

	cookieToSet = http.Cookie{Name: cookie.CookieAuthenticationName, Value: "", Domain: cookie.CookieTokenUserDomain, Path: "/", Expires: expire, MaxAge: -1}
	http.SetCookie(w, &cookieToSet)

	response.RespondWithJson(w, http.StatusOK, successmessage.UserDisconnected, nil)
}

// Description :
//		Method PATCH.
//		This function updates the first and last name of a user thanks to his "id" or his "username".
//		All the checks about the Name have be done in the middleware "CheckFieldsName".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserName(w http.ResponseWriter, r *http.Request) {
	var userName model.UserName

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userName)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userName.FirstName = strings.TrimSpace(userName.FirstName)
	userName.LastName = strings.TrimSpace(userName.LastName)

	user.FirstName = userName.FirstName
	user.LastName = userName.LastName
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.UserNameUpdated, userName)
}

// Description :
//		Method PATCH.
//		This function updates the location of a user thanks to his "id" or his "username".
//		All the checks about the Location have be done in the middleware "CheckFieldsLocation".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserLocation(w http.ResponseWriter, r *http.Request) {
	var userLocation model.Location

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userLocation)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userLocation.City = strings.TrimSpace(userLocation.City)
	userLocation.ZipCode = strings.TrimSpace(userLocation.ZipCode)
	userLocation.CountryCode = strings.TrimSpace(userLocation.CountryCode)
	userLocation.StreetAddress = strings.TrimSpace(userLocation.StreetAddress)

	user.Location = userLocation
	user.Location.Country = middleFormat.GetCountryname(userLocation.CountryCode)
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.LocationUpdated, user.Location)
}

// Description :
//		Method PATCH.
//		This function updates the birthday of a user thanks to his "id" or his "username".
//		All the checks about the Birthday have be done in the middleware "CheckFieldsBirthday".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserBirthday(w http.ResponseWriter, r *http.Request) {
	var userBirthday model.UserBirthday

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userBirthday)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userBirthday.Birthday = strings.TrimSpace(userBirthday.Birthday)

	user.Birthday = userBirthday.Birthday
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.BirthdayUpdated, user.Birthday)
}

// Description :
//		Method PATCH.
//		This function updates the username of a user thanks to his "id" or his "username".
//		All the checks about the Username have be done in the middleware "CheckFieldsUsername".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserUsername(w http.ResponseWriter, r *http.Request) {
	var userUsername model.UserUsername

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userUsername)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userUsername.Username = strings.TrimSpace(userUsername.Username)

	user.Username = userUsername.Username
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.UsernameUpdated, user.Username)
}

// Description :
//		Method PATCH.
//		This function updates the email of a user thanks to his "id" or his "username".
//		All the checks about the Email have be done in the middleware "CheckFieldsEmailUnique".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserEmail(w http.ResponseWriter, r *http.Request) {
	var userEmail model.UserEmail

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userEmail)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userEmail.Email = strings.TrimSpace(userEmail.Email)

	user.Email = userEmail.Email
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.EmailUpdated, user.Email)
}

// Description :
//		Method PATCH.
//		This function updates the phone number of a user thanks to his "id" or his "username".
//		All the checks about the Phone have be done in the middleware "CheckFieldsPhone".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserPhone(w http.ResponseWriter, r *http.Request) {
	var userPhone model.Phone

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userPhone)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}
	userPhone.CountryCode = strings.TrimSpace(userPhone.CountryCode)
	userPhone.NationalNumber = strings.TrimSpace(userPhone.NationalNumber)

	user.Phone = userPhone
	numberProto, _ := phonenumbers.Parse(userPhone.NationalNumber, userPhone.CountryCode)
	user.Phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
	user.Phone.Number = user.Phone.InternationalNumber
	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.PhoneUpdated, user.Phone)
}

// Description :
//		Method PATCH.
//		This function updates the password of a user thanks to his "id" or his "username".
//		All the checks about the Password have be done in the middleware "CheckFieldsPassword".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateUserPassword(w http.ResponseWriter, r *http.Request) {
	var userPassword model.UserPassword

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userPassword)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	if ok := security.CheckPasswordHash(userPassword.Password, user.Password); ok == true {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.NewPasswordMatchOld, nil)
		return
	}
	if user.Password, err = security.HashPassword(userPassword.Password); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.PasswordUpdated, nil)
}

// Description :
//		Method PATCH.
//		This function add a role to a user thanks to his "id" or his "username".
//		All the checks about the Role have be done in the middleware "CheckFieldsRole".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddRoleToUser(w http.ResponseWriter, r *http.Request) {
	var userRole model.UserRole

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userRole)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	userRole.Role = strings.TrimSpace(userRole.Role)

	if role.ContainsRole(user.Role, userRole.Role) == -1 {
		user.Role = append(user.Role, userRole.Role)
	}

	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.RoleAdded, user.Role)
}

// Description :
//		Method PATCH.
//		This function remove a role from a user thanks to his "id" or his "username".
//		All the checks about the Role have be done in the middleware "CheckFieldsRole".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func RemoveRoleFromUser(w http.ResponseWriter, r *http.Request) {
	var userRole model.UserRole

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userRole)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	userRole.Role = strings.TrimSpace(userRole.Role)

	if index := role.ContainsRole(user.Role, userRole.Role); index != -1 {
		user.Role[index] = user.Role[len(user.Role)-1]
		user.Role = user.Role[:len(user.Role)-1]
	}

	err = DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.RoleRemoved, user.Role)
}

// Description
//		Method PATCH.
//		This function updates an image of a user thanks to his "id" or his "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func UpdateUserImage(w http.ResponseWriter, r *http.Request) {
	// Get User from Id
	params := mux.Vars(r)
	user, err := DaoUser.FindByIdSendBack(params["user"])
	if err != nil {
		user, err = DaoUser.FindByUsernameSendBackUser(params["user"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
	}

	// Get Images From Body
	var imageRequest model.UploadRequestImage
	defer r.Body.Close()

	decodeErr := json.NewDecoder(r.Body).Decode(&imageRequest)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}

	imageRequest.Name = strings.TrimSpace(imageRequest.Name)
	if len(imageRequest.Content) == 0 {
		// Delete Image
		var deleteErr error
		if user, deleteErr = deleteImageFromUser(imageRequest, user); deleteErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, deleteErr.Error(), nil)
			return
		}
	} else {
		// Update Image
		var updateErr error
		if user, updateErr = updateImageFromUser(imageRequest, user); updateErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
			return
		}
	}

	// Update User
	updateErr := DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.ImageUpdated, user.Images)
}

// Description
//		This function update an image of a user in "syd" database thanks to his "id" or "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func updateImageFromUser(imageRequest model.UploadRequestImage, user model.User) (model.User, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = user.Id.Hex() + "_" + imageRequest.Name

	if uploadErr := image.DaoImage.Update(imageRequest); uploadErr != nil {
		return user, uploadErr
	}

	if user.Images == nil {
		user.Images = make(map[string]string)
	}

	user.Images[imageKey] = imageRequest.Name

	return user, nil
}

// Description
//		This function delete an image of a user in "syd" database thanks to his "id" or "username".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func deleteImageFromUser(imageRequest model.UploadRequestImage, user model.User) (model.User, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = user.Id.Hex() + "_" + imageRequest.Name

	if updateErr := image.DaoImage.Delete(imageRequest.Name); updateErr != nil {
		return user, updateErr
	}

	if user.Images == nil {
		return user, errors.New(errormessage.ImageNotFound)
	}

	if _, ok := user.Images[imageKey]; !ok {
		return user, errors.New(errormessage.ImageNotFound)
	}

	delete(user.Images, imageKey)

	return user, nil
}

// Description :
//		Method PATCH.
//		This function add Token to a user thanks to his "id" or "username" to his wallet.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddTokensToUser(w http.ResponseWriter, r *http.Request) {
	var userWallet model.UserUpdateToken

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userWallet)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)

	wallet, err := DaoUser.AddTokensToWallet(params["user"], userWallet.Value)

	if err != nil {
		response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
		return
	}

	var walletUpdate model.UserWallet
	walletUpdate.Wallet = wallet

	response.RespondWithJson(w, http.StatusOK, successmessage.TokensAdded, walletUpdate)
}

// Description :
//		Method PATCH.
//		This function remove tokens from a user thanks to his "id" or "username" to his wallet.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func RemoveTokensFromUser(w http.ResponseWriter, r *http.Request) {
	var userWallet model.UserUpdateToken

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userWallet)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	params := mux.Vars(r)

	wallet, err := DaoUser.RemoveTokensFromWallet(params["user"], userWallet.Value)

	if err != nil {
		if err.Error() == errormessage.UserNotEnoughTokens {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.UserNotEnoughTokens, nil)
			return
		}
		response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
		return
	}

	var walletUpdate model.UserWallet
	walletUpdate.Wallet = wallet

	response.RespondWithJson(w, http.StatusOK, successmessage.TokensRemoved, walletUpdate)
}
