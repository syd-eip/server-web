package user

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/nyaruka/phonenumbers"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	midformat "gitlab.com/syd-eip/server-web/api/v1/middleware/format"
)

// Description :
//		This function selects the appropriate function, depending on the request method.
//		It calls "getMeUser" if the method is GET.
//		It calls "deleteMeUser" if the method is DELETE.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SelectMethodMeUser(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		getMeUser(w, r)
	case "DELETE":
		deleteMeUser(w, r)
	}
}

// Description :
//		Method GET.
//		This function recovers the detailed user of the authenticated user from "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func getMeUser(w http.ResponseWriter, r *http.Request) {
	token, e := security.ExtractJwt(r)
	if e != nil {
		if e.Error() == "MISSING_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, _ := DaoUser.FindByIdSendBack(claim["id"].(string))

		details := response.ConvertUserToUserDetailed(user)
		response.RespondWithJson(w, http.StatusOK, successmessage.OK, details)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method DELETE.
//		This function deletes the authenticated user from "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func deleteMeUser(w http.ResponseWriter, r *http.Request) {
	token, e := security.ExtractJwt(r)
	if e != nil {
		if e.Error() == "MISSING_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))

		err = DaoUser.Delete(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}
		response.RespondWithJson(w, http.StatusOK, successmessage.UserDeleted, nil)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the first and last name of the authenticated user.
//		All the checks about the Name have be done in the middleware "CheckFieldsName".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserName(w http.ResponseWriter, r *http.Request) {
	var userName model.UserName

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userName)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userName.FirstName = strings.TrimSpace(userName.FirstName)
		userName.LastName = strings.TrimSpace(userName.LastName)

		user.FirstName = userName.FirstName
		user.LastName = userName.LastName
		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.UserNameUpdated, userName)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the location of the authenticated user.
//		All the checks about the Location have be done in the middleware "CheckFieldsLocation".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserLocation(w http.ResponseWriter, r *http.Request) {
	var userLocation model.Location

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userLocation)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userLocation.City = strings.TrimSpace(userLocation.City)
		userLocation.ZipCode = strings.TrimSpace(userLocation.ZipCode)
		userLocation.CountryCode = strings.TrimSpace(userLocation.CountryCode)
		userLocation.StreetAddress = strings.TrimSpace(userLocation.StreetAddress)

		user.Location = userLocation
		user.Location.Country = midformat.GetCountryname(userLocation.CountryCode)

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.LocationUpdated, user.Location)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the birthday of the authenticated user.
//		All the checks about the Birthday have be done in the middleware "CheckFieldsBirthday".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserBirthday(w http.ResponseWriter, r *http.Request) {
	var userBirthday model.UserBirthday

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userBirthday)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userBirthday.Birthday = strings.TrimSpace(userBirthday.Birthday)

		user.Birthday = userBirthday.Birthday
		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.BirthdayUpdated, user.Birthday)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the username of the authenticated user.
//		All the checks about the Username have be done in the middleware "CheckFieldsUsername".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserUsername(w http.ResponseWriter, r *http.Request) {
	var userUsername model.UserUsername

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userUsername)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userUsername.Username = strings.TrimSpace(userUsername.Username)

		user.Username = userUsername.Username
		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.UsernameUpdated, user.Username)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the email of the authenticated user.
//		All the checks about the Email have be done in the middleware "CheckFieldsEmail".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserEmail(w http.ResponseWriter, r *http.Request) {
	var userEmail model.UserEmail

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userEmail)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userEmail.Email = strings.TrimSpace(userEmail.Email)

		user.Email = userEmail.Email
		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.EmailUpdated, user.Email)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the phone number of the authenticated user.
//		All the checks about the Phone have be done in the middleware "CheckFieldsPhone".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserPhone(w http.ResponseWriter, r *http.Request) {
	var userPhone model.Phone

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userPhone)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		userPhone.CountryCode = strings.TrimSpace(userPhone.CountryCode)
		userPhone.NationalNumber = strings.TrimSpace(userPhone.NationalNumber)

		user.Phone = userPhone
		numberProto, _ := phonenumbers.Parse(userPhone.NationalNumber, userPhone.CountryCode)
		user.Phone.InternationalNumber = phonenumbers.Format(numberProto, phonenumbers.E164)
		user.Phone.Number = user.Phone.InternationalNumber
		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.PhoneUpdated, user.Phone)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function updates the password of the authenticated user.
//		All the checks about the Password have be done in the middleware "CheckFieldsPassword".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdateMeUserPassword(w http.ResponseWriter, r *http.Request) {
	var userPassword model.UserPassword

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userPassword)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		if ok := security.CheckPasswordHash(userPassword.Password, user.Password); ok == true {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.NewPasswordMatchOld, nil)
			return
		}
		if user.Password, err = security.HashPassword(userPassword.Password); err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.PasswordUpdated, nil)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description
//		Method PATCH.
//		This function updates an image of the authenticated user.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func UpdateMeUserImage(w http.ResponseWriter, r *http.Request) {
	// Get User Id
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claim, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}

	user, err := DaoUser.FindByIdSendBack(claim["id"].(string))

	// Get Images From Body
	var imageRequest model.UploadRequestImage
	defer r.Body.Close()

	decodeErr := json.NewDecoder(r.Body).Decode(&imageRequest)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}

	imageRequest.Name = strings.TrimSpace(imageRequest.Name)
	if len(imageRequest.Content) == 0 {
		// Delete Image
		var deleteErr error
		if user, deleteErr = deleteImageFromUser(imageRequest, user); deleteErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, deleteErr.Error(), nil)
			return
		}
	} else {
		// Update Image
		var updateErr error
		if user, updateErr = updateImageFromUser(imageRequest, user); updateErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
			return
		}
	}

	// Update User
	updateErr := DaoUser.Update(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.ImageUpdated, user.Images)
}

// Description :
//		Method POST.
//		This function checks the availability of a username asked by the authenticated user.
//		All the checks about the Username have be done in the middleware "CheckFieldsUsername".
//		So, here, if there are no errors in the claims and the user exists, it always sends back "USERNAME_FREE".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func CheckUserUsernameAvailability(w http.ResponseWriter, r *http.Request) {
	response.RespondWithJson(w, http.StatusOK, successmessage.UsernameFree, nil)
}

// Description :
//		Method POST.
//		This function checks the availability of an email asked by the authenticated user.
//		All the checks about the Email have be done in the middleware "CheckFieldsEMail".
//		So, here, if there are no errors in the claims and the user exists, it always sends back "EMAIL_FREE".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func CheckUserEmailAvailability(w http.ResponseWriter, r *http.Request) {
	response.RespondWithJson(w, http.StatusOK, successmessage.EmailFree, nil)
}

// Description :
//		Method POST
//		This function checks the availability of a phone number asked by the authenticated user.
//		All the checks about the Phone have be done in the middleware "CheckFieldsPhone".
//		So, here, if there are no errors in the claims and the user exists, it always sends back "PHONE_NUMBER_FREE".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func CheckUserPhoneAvailability(w http.ResponseWriter, r *http.Request) {
	response.RespondWithJson(w, http.StatusOK, successmessage.PhoneFree, nil)
}

// Description :
//		Method PATCH.
//		This function add Token to the authenticated user into his "wallet".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddTokensToUserMe(w http.ResponseWriter, r *http.Request) {
	var userWallet model.UserUpdateToken

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userWallet)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claim, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}

	wallet, err := DaoUser.AddTokensToWallet(claim["id"].(string), userWallet.Value)

	if err != nil {
		if err.Error() == "not found" {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	var walletUpdate model.UserWallet
	walletUpdate.Wallet = wallet

	response.RespondWithJson(w, http.StatusOK, successmessage.TokensAdded, walletUpdate)
}
