package user

import (
	"encoding/json"
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/controller/association"
	"gitlab.com/syd-eip/server-web/api/v1/controller/tag"
	"gitlab.com/syd-eip/server-web/api/v1/middleware/payload/user/favorite"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
)

// Description :
//		Method GET.
//		This function retrieve all the favorites of the authenticated user.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserFavorites(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.OK, user.Favorites)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method GET.
//		This function retrieve all the favorites tags of the authenticated user.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserFavoritesTags(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.OK, user.Favorites.Tags)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function add favorite tags to the authenticated user.
//		All the checks about the Favorite tags have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddFavoriteTag(w http.ResponseWriter, r *http.Request) {
	var favoriteTags []string

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteTags)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteTags); i++ {
			if tag.DaoTag.FindByName(favoriteTags[i]) && favorite.ContainsTag(user.Favorites.Tags, favoriteTags[i]) == -1 {
				user.Favorites.Tags = append(user.Favorites.Tags, favoriteTags[i])
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteTagsUpdated, user.Favorites.Tags)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function remove favorite tags to the authenticated user.
//		All the checks about the Favorite tags have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func RemoveFavoriteTag(w http.ResponseWriter, r *http.Request) {
	var favoriteTags []string

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteTags)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteTags); i++ {
			if index := favorite.ContainsTag(user.Favorites.Tags, favoriteTags[i]); index != -1 {
				user.Favorites.Tags[index] = user.Favorites.Tags[len(user.Favorites.Tags)-1]
				user.Favorites.Tags = user.Favorites.Tags[:len(user.Favorites.Tags)-1]
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteTagsUpdated, user.Favorites.Tags)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method GET.
//		This function retrieve all the favorites users of the authenticated user.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserFavoritesUsers(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.OK, user.Favorites.Users)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function add favorite users to the authenticated user.
//		All the checks about the Favorite users have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddFavoriteUser(w http.ResponseWriter, r *http.Request) {
	var favoriteUsers []model.UserPublic

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteUsers)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteUsers); i++ {
			if DaoUser.FindById(favoriteUsers[i].Id.Hex()) && favorite.ContainsId(user.Favorites.Users, favoriteUsers[i].Id) == -1 {
				user.Favorites.Users = append(user.Favorites.Users, favoriteUsers[i].Id)
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteUsersUpdated, user.Favorites.Users)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function remove favorite users to the authenticated user.
//		All the checks about the Favorite users have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func RemoveFavoriteUser(w http.ResponseWriter, r *http.Request) {
	var favoriteUsers []model.UserPublic

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteUsers)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteUsers); i++ {
			if index := favorite.ContainsId(user.Favorites.Users, favoriteUsers[i].Id); index != -1 {
				user.Favorites.Users[index] = user.Favorites.Users[len(user.Favorites.Users)-1]
				user.Favorites.Users = user.Favorites.Users[:len(user.Favorites.Users)-1]
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteUsersUpdated, user.Favorites.Users)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method GET.
//		This function retrieve all the favorites associations of the authenticated user.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserFavoritesAssociations(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.OK, user.Favorites.Associations)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function add favorite associations to the authenticated user.
//		All the checks about the Favorite associations have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddFavoriteAssociation(w http.ResponseWriter, r *http.Request) {
	var favoriteAssociations []model.Association

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteAssociations)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteAssociations); i++ {
			if association.DaoAssociation.FindById(favoriteAssociations[i].Id.Hex()) && favorite.ContainsId(user.Favorites.Associations, favoriteAssociations[i].Id) == -1 {
				user.Favorites.Associations = append(user.Favorites.Associations, favoriteAssociations[i].Id)
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteAssociationsUpdated, user.Favorites.Associations)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}

// Description :
//		Method PATCH.
//		This function remove favorite associations to the authenticated user.
//		All the checks about the Favorite associations have be done in the middleware "CheckFieldsFavorite".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func RemoveFavoriteAssociation(w http.ResponseWriter, r *http.Request) {
	var favoriteAssociations []model.Association

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&favoriteAssociations)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		for i := 0; i < len(favoriteAssociations); i++ {
			if index := favorite.ContainsId(user.Favorites.Associations, favoriteAssociations[i].Id); index != -1 {
				user.Favorites.Associations[index] = user.Favorites.Associations[len(user.Favorites.Associations)-1]
				user.Favorites.Associations = user.Favorites.Associations[:len(user.Favorites.Associations)-1]
			}
		}

		err = DaoUser.Update(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.FavoriteAssociationsUpdated, user.Favorites.Associations)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}
