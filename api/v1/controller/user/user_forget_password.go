package user

import (
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"os"

	"gitlab.com/syd-eip/server-web/api/v1/constant/mailing"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	headerConst "gitlab.com/syd-eip/server-web/api/v1/constant/header"
	tokenConst "gitlab.com/syd-eip/server-web/api/v1/constant/token"
	mailingController "gitlab.com/syd-eip/server-web/api/v1/controller/mailing"
)

// Description :
//		Method POST.
//		This function sends a mail to a user to reset his password if his mail exist in SYD's database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func SendMailToUserToResetPassword(w http.ResponseWriter, r *http.Request) {
	var userMail model.UserEmail

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userMail)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	sender := mailingController.NewSender(os.Getenv(mailing.SydMailAddress), os.Getenv(mailing.SydMailPassword))

	user, _ := DaoUser.FindByEmailSendBackUser(userMail.Email)

	token, err := security.GenerateJWTPasswordToken(user)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	url := "/user/reset-password"

	if r.Header.Get(headerConst.OriginRequestFront) == "local.shareyourdreams.fr" {
		url = "http://" + r.Header.Get(headerConst.OriginRequestFront) + ":8082" + url
	} else {
		url = "https://" + r.Header.Get(headerConst.OriginRequestFront) + url
	}

	url = url + "?reset-token=" + token

	message := `
	<!doctype html>
	<html lang="en">
	<head>
	<meta charset="utf-8"/>
  	<meta http-equiv="content-type" content="text/html" ; charset=ISO-8859-1">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	</head>
	<body>
	<div>
  	<p>Here is the URL to reset your password:</p>
	<br>
  	<a href=` + url + `>Click to reset your password</a>
  	<br>
  	<div class="moz-signature">
    <i><br><br>Best Regards,<br>SYD support<br></i>
  	</div>
	</div>
	</body>
	</html>
	`
	bodyMessage := sender.WriteHTMLEmail([]string{userMail.Email}, "Reset password", message)

	var responseData []model.MailingResponse

	var data model.MailingResponse
	data.Mail = userMail.Email

	isSent, _ := sender.SendMail([]string{userMail.Email}, "Reset password", bodyMessage)
	if isSent {
		data.Message = "SUCCESS"
	} else {
		data.Message = "ERROR"
	}

	responseData = append(responseData, data)

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, responseData)

	//CLEAR OLD JWT PASSWORD TOKEN
	//This action is done in the background, when a new token is asked, it clears all the old ones.
	tokens, _ := DaoJwt.FindAll()
	for _, elem := range tokens {
		if elem.Token != "Test" && elem.Token != "Test2" {
			_, err = jwt.Parse(elem.Token, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					_ = DaoJwt.Delete(elem)
					return nil, nil
				}
				return tokenConst.PasswordKey, nil
			})
			if err != nil {
				_ = DaoJwt.Delete(elem)
			}
		}
	}
}

// Description :
//		Method GET.
//		This function check if the token received is a valid token for the reset password.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetResetPasswordCheckToken(w http.ResponseWriter, r *http.Request) {

	if r.Header[headerConst.ResetToken] != nil {
		head := r.Header[headerConst.ResetToken][0]

		token, err := jwt.Parse(head, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("PARSING_FAILED")
			}
			return tokenConst.PasswordKey, nil
		})
		if err != nil || DaoJwt.FindByToken(token.Raw) {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}

		if claim, ok := token.Claims.(jwt.MapClaims); ok {
			user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
			if err != nil {
				response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
				return
			}
			if user.Email != claim["email"].(string) {
				response.RespondWithJson(w, http.StatusNotFound, errormessage.EmailNotExist, nil)
				return
			}
		}
	} else {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.ResetPasswordTokenMissing, nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, nil)
}

// Description :
//		Method PATCH.
//		This function check if the token received is a valid token for the reset password.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func UpdatePasswordWithToken(w http.ResponseWriter, r *http.Request) {
	var userPassword model.UserPassword

	defer r.Body.Close()
	err := json.NewDecoder(r.Body).Decode(&userPassword)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	if r.Header[headerConst.ResetToken] != nil {
		head := r.Header[headerConst.ResetToken][0]

		token, err := jwt.Parse(head, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("PARSING_FAILED")
			}
			return tokenConst.PasswordKey, nil
		})
		if err != nil || DaoJwt.FindByToken(token.Raw) {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
			return
		}

		if claim, ok := token.Claims.(jwt.MapClaims); ok {
			user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
			if err != nil {
				response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
				return
			}
			if user.Email != claim["email"].(string) {
				response.RespondWithJson(w, http.StatusNotFound, errormessage.EmailNotExist, nil)
				return
			}
			if ok := security.CheckPasswordHash(userPassword.Password, user.Password); ok == true {
				response.RespondWithJson(w, http.StatusBadRequest, errormessage.NewPasswordMatchOld, nil)
				return
			}
			if user.Password, err = security.HashPassword(userPassword.Password); err != nil {
				response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
				return
			}
			err = DaoUser.Update(user)
			if err != nil {
				response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
				return
			}

			var blackListedToken model.JwtToken
			blackListedToken.Id = bson.NewObjectId()
			blackListedToken.Token = token.Raw
			if err := DaoJwt.Insert(blackListedToken); err != nil {
				response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
				return
			}

			sender := mailingController.NewSender(os.Getenv(mailing.SydMailAddress), os.Getenv(mailing.SydMailPassword))

			message := `
			<!doctype html>
			<html lang="en">
			<head>
			<meta charset="utf-8"/>
  			<meta http-equiv="content-type" content="text/html" ; charset=ISO-8859-1">
  			<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
			</head>
			<body>
			<div>
  			<p>Hello ` + user.FirstName + ` ` + user.LastName + ` your password has been updated.</p>
  			<div class="moz-signature">
    		<i><br><br>Best Regards,<br>SYD support<br></i>
  			</div>
			</div>
			</body>
			</html>
			`
			bodyMessage := sender.WriteHTMLEmail([]string{user.Email}, "Password Updated", message)

			var responseData []model.MailingResponse

			var data model.MailingResponse
			data.Mail = user.Email

			isSent, _ := sender.SendMail([]string{user.Email}, "Password Updated", bodyMessage)
			if isSent {
				data.Message = "SUCCESS"
			} else {
				data.Message = "ERROR"
			}

			responseData = append(responseData, data)

			response.RespondWithJson(w, http.StatusOK, successmessage.PasswordReset, responseData)
		}

	} else {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.ResetPasswordTokenMissing, nil)
		return
	}
}
