package user

import (
	"net/http"

	"github.com/dgrijalva/jwt-go"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	eventcontroll "gitlab.com/syd-eip/server-web/api/v1/controller/event"
)

// Description :
//		Method GET.
//		This function uses the favorites to match the events that may please the authenticated user the most.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetUserDiscover(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		if err.Error() == "MISSING_TOKEN" || err.Error() == "BAD_TOKEN" {
			response.RespondWithJson(w, http.StatusUnauthorized, errormessage.UserNotAuthorized, nil)
			return
		}
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claim, ok := token.Claims.(jwt.MapClaims); ok {
		user, err := DaoUser.FindByIdSendBack(claim["id"].(string))
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.UserNotFound, nil)
			return
		}

		events, err := eventcontroll.DaoEvent.FindDiscoverEvents(user)
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.EventNotFound, nil)
			return
		}

		response.RespondWithJson(w, http.StatusOK, successmessage.OK, events)
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}
}
