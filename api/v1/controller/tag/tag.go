// This package connects to "syd" database.
// It also contains all queries regarding "tag" routes.
package tag

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/controller/image"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
)

func decodeTagRequestBody(r *http.Request) (model.TagRequestBody, error) {
	var tag model.TagRequestBody
	defer r.Body.Close()

	decodeError := json.NewDecoder(r.Body).Decode(&tag)

	return tag, decodeError
}

// Description
//		Method POST.
//		This function registers a new tag on SYD and saves it in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func CreateTag(w http.ResponseWriter, r *http.Request) {

	tagRequest, decodeErr := decodeTagRequestBody(r)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}

	var tag model.Tag
	tag.Id = bson.NewObjectId()
	tag.Name = strings.TrimSpace(tagRequest.Name)
	tag.ActualEvent = 0
	tag.TotalEvent = 0
	tag.Images = make(map[string]string)

	if err := DaoTag.Insert(tag); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusCreated, successmessage.TagCreated, tag)
}

// Description
//		Method DELETE.
//		This function delete a tag on SYD in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func DeleteTag(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	tag, err := DaoTag.FindByIdSendBack(params["tag"])
	if err != nil {
		tag, err = DaoTag.FindByNameSendBack(params["tag"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.TagNotFound, nil)
			return
		}
	}

	err = DaoTag.Delete(tag)
	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.TagDeleted, nil)
}

// Description
//		Method GET.
//		This function get all tags on SYD in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetAllTags(w http.ResponseWriter, r *http.Request) {
	tags, err := DaoTag.FindAll()
	if err != nil {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.TagNotFound, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, tags)
}

// Description
//		Method GET.
//		This function get all tags name on SYD in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetAllTagsName(w http.ResponseWriter, r *http.Request) {
	tags, err := DaoTag.FindAllNames()
	if err != nil {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.TagNotFound, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, tags)
}

// Description
//		Method GET.
//		This function recover a tag in "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetTag(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	tag, err := DaoTag.FindByIdSendBack(params["tag"])
	if err != nil {
		tag, err = DaoTag.FindByNameSendBack(params["tag"])
		if err != nil {
			response.RespondWithJson(w, http.StatusBadRequest, errormessage.TagNotFound, nil)
			return
		}
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, tag)
}

// Description
//		Method PATCH.
//		This function recover a tag in "syd" database thanks to its "id" or "name" and update its image field.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func UpdateTagImage(w http.ResponseWriter, r *http.Request) {
	// Get Tag from Id
	params := mux.Vars(r)
	tag, err := DaoTag.FindByIdSendBack(params["tag"])
	if err != nil {
		tag, err = DaoTag.FindByNameSendBack(params["tag"])
		if err != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, errormessage.TagNotFound, nil)
			return
		}
	}

	// Get Images From Body
	var imageRequest model.UploadRequestImage

	decodeErr := json.NewDecoder(r.Body).Decode(&imageRequest)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}
	defer r.Body.Close()

	imageRequest.Name = strings.TrimSpace(imageRequest.Name)
	if len(imageRequest.Content) == 0 {
		// Delete Image
		var deleteErr error
		tag, deleteErr = deleteImageFromTag(imageRequest, tag)
		if deleteErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, deleteErr.Error(), nil)
			return
		}
	} else {
		// Update Image
		var updateErr error
		if tag, updateErr = updateImageFromTag(imageRequest, tag); updateErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
			return
		}
	}

	// Update Tag
	updateErr := DaoTag.Update(tag)
	if updateErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.ImageUpdated, tag.Images)
}

// Description
//		This function update an image from a tag in "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.

func updateImageFromTag(imageRequest model.UploadRequestImage, tag model.Tag) (model.Tag, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = tag.Id.Hex() + "_" + imageRequest.Name

	if uploadErr := image.DaoImage.Update(imageRequest); uploadErr != nil {
		return tag, uploadErr
	}

	if tag.Images == nil {
		tag.Images = make(map[string]string)
	}

	tag.Images[imageKey] = imageRequest.Name

	return tag, nil
}

// Description
//		This function delete an image from a tag in "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func deleteImageFromTag(imageRequest model.UploadRequestImage, tag model.Tag) (model.Tag, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = tag.Id.Hex() + "_" + imageRequest.Name

	if updateErr := image.DaoImage.Delete(imageRequest.Name); updateErr != nil {
		return tag, updateErr
	}

	if tag.Images == nil {
		return tag, errors.New(errormessage.ImageNotFound)
	}

	if _, ok := tag.Images[imageKey]; !ok {
		return tag, errors.New(errormessage.ImageNotFound)
	}

	delete(tag.Images, imageKey)

	return tag, nil
}
