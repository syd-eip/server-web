package tag

import (
	"gitlab.com/syd-eip/server-web/api/v1/repository/tag"
)

// Connection to the "syd" database and tags collection.
var DaoTag = tag.DbTagsStruct{}

//Connection to SYD database and Tag collection
func init() {
	DaoTag.Connect()
}
