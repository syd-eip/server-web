package event

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

// Description
//		Method POST.
//		This function add a bet to a specific lottery inside an event thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func AddBet(w http.ResponseWriter, r *http.Request) {
	var betRequest model.AddBetRequestModel
	defer r.Body.Close()

	if decodeError := json.NewDecoder(r.Body).Decode(&betRequest); decodeError != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeError.Error(), nil)
		return
	}

	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if betRequest.Value <= 0 {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.BetNegativeOrNull, nil)
		return
	}
	event.Lotteries[betRequest.LotteryId].TotalValue += betRequest.Value

	// ADD REQUEST TO USER WALLET

	found := false
	for idx, participation := range event.Lotteries[betRequest.LotteryId].Participations {
		if participation.UserId.Hex() == claims["id"].(string) {
			event.Lotteries[betRequest.LotteryId].Participations[idx].Value += betRequest.Value
			found = true
			break
		}
	}
	if !found {
		var newParticipation model.Participation
		newParticipation.UserId = bson.ObjectIdHex(claims["id"].(string))
		newParticipation.Value = betRequest.Value
		newParticipation.SplitRatio = 0.5
		event.Lotteries[betRequest.LotteryId].Participations = append(event.Lotteries[betRequest.LotteryId].Participations, newParticipation)
	}

	DaoEvent.Update(event)

	response.RespondWithJson(w, http.StatusOK, successmessage.EventBetAdded, event.Lotteries)
}

// Description
//		Method GET.
//		This function get a bet to a specific lottery inside an event thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetBet(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	found := false
	totalParticipation := make([]model.Lottery, event.LotteriesSize, event.LotteriesSize)

	for lotteryId := 0; lotteryId < event.LotteriesSize; lotteryId++ {
		for _, participation := range event.Lotteries[lotteryId].Participations {
			if participation.UserId.Hex() == claims["id"].(string) {
				totalParticipation[lotteryId].Participations = append(totalParticipation[lotteryId].Participations, participation)
				totalParticipation[lotteryId].TotalValue += participation.Value
				found = true
			}
		}
	}
	if found {
		response.RespondWithJson(w, http.StatusOK, successmessage.OK, totalParticipation)
		return
	}
	response.RespondWithJson(w, http.StatusNotFound, errormessage.BetNotFound, nil)
}
