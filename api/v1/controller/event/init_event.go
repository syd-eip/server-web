package event

import (
	"gitlab.com/syd-eip/server-web/api/v1/repository/event"
)

var DaoEvent = event.DbEventsStruct{}

//Connection to SYD database and Event collection
func init() {
	DaoEvent.Connect()
}