// This package connects to "syd" database.
// It also contains all queries regarding "event" routes.
package event

import (
	"encoding/json"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/role"
	"gitlab.com/syd-eip/server-web/api/v1/controller/image"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"strings"

	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"gitlab.com/syd-eip/server-web/api/v1/security"

	associationController "gitlab.com/syd-eip/server-web/api/v1/controller/association"
	tagController "gitlab.com/syd-eip/server-web/api/v1/controller/tag"
	checkingRole "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
)

// Description
//	Decodes the request body and extracts the model.Event
// Parameters :
//	event :
//		event that contains the desired lottery number
// Return :
//	model.Event :
//		event decoded from the body request
//	error :
//		error generated by the decoder
func decodeEventRequestBody(r *http.Request) (model.EventCreateEvent, error) {
	var event model.EventCreateEvent
	defer r.Body.Close()

	decodeError := json.NewDecoder(r.Body).Decode(&event)

	return event, decodeError
}

// Description
//	Creates a lottery list depending on the LotteriesSize inside the model.Event
// Parameters :
//	event :
//		event that contains the desired lottery number
// Return :
//	No return but the parameter event *model.Event is updated
func createLotteryList(event *model.Event) {
	for i := 0; i < event.LotteriesSize; i++ {
		var lottery model.Lottery
		lotteries := append(event.Lotteries, lottery)
		event.Lotteries = lotteries
	}
}

// Description
//		Method POST.
//		This function registers an event on SYD and saves it in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func CreateEvent(w http.ResponseWriter, r *http.Request) {

	eventRequest, decodeErr := decodeEventRequestBody(r)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}

	var event model.Event
	event.Id = bson.NewObjectId()

	event.Information.Name = strings.TrimSpace(eventRequest.Name)
	event.Information.Description = eventRequest.Description
	event.Information.DrawDate = eventRequest.DrawDate
	event.Information.EventDate = eventRequest.EventDate
	event.Images = make(map[string]string)

	event.LotteriesSize = eventRequest.LotteriesSize
	if event.LotteriesSize < 1 {
		event.LotteriesSize = 1
	}

	tagNames, _ := tagController.DaoTag.FindAllNames()
	for _, repositoryTag := range tagNames {
		for _, requestTag := range eventRequest.Tags {
			if repositoryTag == requestTag {
				event.Information.Tags = append(event.Information.Tags, requestTag)
				tagController.DaoTag.AddEvent(repositoryTag)
				break
			}
		}
	}

	association, associationError := associationController.DaoAssociation.FindByIdSendBack(eventRequest.Association)
	if associationError != nil {
		association, associationError = associationController.DaoAssociation.FindByNameSendBack(eventRequest.Association)
		if associationError != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, associationError.Error(), nil)
			return
		}
	}

	event.AssociationId = association.Id

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		event.UserId = bson.ObjectIdHex(claims["id"].(string))
	} else {
		response.RespondWithJson(w, http.StatusInternalServerError, errormessage.CantGetFromClaims, nil)
		return
	}

	createLotteryList(&event)

	if err := DaoEvent.Insert(event); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}
	response.RespondWithJson(w, http.StatusCreated, successmessage.EventCreated, event)
}

// Description
//		Method GET.
//		This function recovers an event from "syd" database thanks to his "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
func GetEvent(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.OK, event)
}

// Description
//		Method GET.
//		This function recovers all the user's events from "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetAllMyEvents(w http.ResponseWriter, r *http.Request) {
	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
	}

	event, err := DaoEvent.FindAllByUserId(claims["id"].(string))
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, event)
}

// Description
//		Method GET.
//		This function recovers detailed events from "syd" database thanks to the "UserId".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetEventsByUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	event, err := DaoEvent.FindAllByUserId(params["user"])
	if err != nil {
		response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, event)
}

// Description
//		Method GET.
//		This function recovers all the detailed events from "syd" database containing the tags.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func GetEventsByTags(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	tagNames := strings.Split(params["tag"], "&")
	event, err := DaoEvent.FindAllByTags(tagNames)

	if err != nil {
		response.RespondWithJson(w, http.StatusBadRequest, errormessage.EventNotFound, nil)
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, event)
}

// Description
//		Method PATCH.
//		This function update an event name thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func PatchEventName(w http.ResponseWriter, r *http.Request) {
	var eventRequest model.EventName
	defer r.Body.Close()

	decodeError := json.NewDecoder(r.Body).Decode(&eventRequest)
	if decodeError != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeError.Error(), nil)
		return
	}

	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
	}

	if checkingRole.ContainsRoleInterface(claims["role"], role.AdminRole) == -1 &&
		event.UserId.Hex() != claims["id"].(string) {
		response.RespondWithJson(w, http.StatusForbidden, errormessage.UserNotAdmin, nil)
		return
	}

	event.Information.Name = eventRequest.Name

	if err := DaoEvent.Update(event); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.EventNameUpdated, event.Information.Name)
}

// Description
//		Method PATCH.
//		This function update an event Description thanks to its id or name.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func PatchEventDescription(w http.ResponseWriter, r *http.Request) {
	var eventRequest model.EventDescription
	defer r.Body.Close()

	decodeError := json.NewDecoder(r.Body).Decode(&eventRequest)
	if decodeError != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeError.Error(), nil)
		return
	}

	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	if checkingRole.ContainsRoleInterface(claims["role"], role.AdminRole) == -1 &&
		event.UserId.Hex() != claims["id"].(string) {
		response.RespondWithJson(w, http.StatusForbidden, errormessage.UserNotAdmin, nil)
		return
	}

	event.Information.Description = eventRequest.Description

	if err := DaoEvent.Update(event); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.EventDescriptionUpdated, event.Information.Description)
}

// Description
//		Method PATCH.
//		This function update tags associated with an event thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func PatchEventTags(w http.ResponseWriter, r *http.Request) {
	var eventRequest model.EventTags
	defer r.Body.Close()

	decodeError := json.NewDecoder(r.Body).Decode(&eventRequest)
	if decodeError != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeError.Error(), nil)
		return
	}

	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
	}

	if checkingRole.ContainsRoleInterface(claims["role"], role.AdminRole) == -1 &&
		event.UserId.Hex() != claims["id"].(string) {
		response.RespondWithJson(w, http.StatusForbidden, errormessage.UserNotAdmin, nil)
		return
	}

	for _, tagName := range event.Information.Tags {
		tagController.DaoTag.DeleteEvent(tagName)
	}
	event.Information.Tags = nil

	tagNames, _ := tagController.DaoTag.FindAllNames()
	for _, repositoryTag := range tagNames {
		for _, requestTag := range eventRequest.Tags {
			if repositoryTag == requestTag {
				event.Information.Tags = append(event.Information.Tags, requestTag)
				tagController.DaoTag.AddEvent(repositoryTag)
				break
			}
		}
	}

	if err := DaoEvent.Update(event); err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.EventTagsUpdated, event.Information.Tags)
}

// Description
//		Method PATCH.
//		This function updates the image of an event thanks to its "id" or its "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func PatchEventImage(w http.ResponseWriter, r *http.Request) {
	// Get Event from Id
	params := mux.Vars(r)
	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	// Get Images From Body
	var imageRequest model.UploadRequestImage

	decodeErr := json.NewDecoder(r.Body).Decode(&imageRequest)
	if decodeErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, decodeErr.Error(), nil)
		return
	}
	defer r.Body.Close()

	imageRequest.Name = strings.TrimSpace(imageRequest.Name)
	if len(imageRequest.Content) == 0 {
		// Delete Image
		var deleteErr error
		if event, deleteErr = deleteImageFromEvent(imageRequest, event); deleteErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, deleteErr.Error(), nil)
			return
		}
	} else {
		// Update Image
		var updateErr error
		if event, updateErr = patchImageFromEvent(imageRequest, event); updateErr != nil {
			response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
			return
		}
	}

	// Update Event
	updateErr := DaoEvent.Update(event)
	if updateErr != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, updateErr.Error(), nil)
		return
	}

	event, _ = DaoEvent.FindByIdSendBack(params["event"])

	response.RespondWithJson(w, http.StatusOK, successmessage.ImageUpdated, event.Images)
}

// Description
//		This function update an image from an event in "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func patchImageFromEvent(imageRequest model.UploadRequestImage, event model.Event) (model.Event, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = event.Id.Hex() + "_" + imageRequest.Name

	if uploadErr := image.DaoImage.Update(imageRequest); uploadErr != nil {
		return event, uploadErr
	}

	if event.Images == nil {
		event.Images = make(map[string]string)
	}

	event.Images[imageKey] = imageRequest.Name

	return event, nil
}

// Description
//		This function delete an image from an event in "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//		model.Tag :
//			Tag updated
//		error :
//			Error throws in case of a fail from the research or upload in the database.
func deleteImageFromEvent(imageRequest model.UploadRequestImage, event model.Event) (model.Event, error) {
	imageKey := imageRequest.Name
	imageRequest.Name = event.Id.Hex() + "_" + imageRequest.Name

	if event.Images == nil {
		return event, errors.New(errormessage.ImageNotFound)
	}

	if _, ok := event.Images[imageKey]; !ok {
		return event, errors.New(errormessage.ImageNotFound)
	}

	if updateErr := image.DaoImage.Delete(imageRequest.Name); updateErr != nil {
		return event, updateErr
	}

	delete(event.Images, imageKey)

	return event, nil
}

// Description
//		Method DELETE.
//		This function deletes an event from "syd" database thanks to its "id" or "name".
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func DeleteEvent(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)

	event, err := DaoEvent.FindByIdSendBack(params["event"])
	if err != nil {
		event, err = DaoEvent.FindByNameSendBack(params["event"])
		if err != nil {
			response.RespondWithJson(w, http.StatusNotFound, errormessage.EventNotFound, nil)
			return
		}
	}

	token, err := security.ExtractJwt(r)
	if err != nil {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
		return
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		response.RespondWithJson(w, http.StatusUnauthorized, errormessage.TokenInvalid, nil)
	}

	if checkingRole.ContainsRoleInterface(claims["role"], role.AdminRole) == -1 &&
		event.UserId.Hex() != claims["id"].(string) {
		response.RespondWithJson(w, http.StatusForbidden, errormessage.UserNotAdmin, nil)
		return
	}

	tags := event.Information.Tags

	err = DaoEvent.Delete(event)

	if err != nil {
		response.RespondWithJson(w, http.StatusInternalServerError, err.Error(), nil)
		return
	}

	for _, tag := range tags {
		tagController.DaoTag.DeleteEvent(tag)
	}

	response.RespondWithJson(w, http.StatusOK, successmessage.EventDeleted, nil)
}
