// This package connects to "syd" database.
// It also contains all queries regarding "image" routes.
package image

import (
	"github.com/gorilla/mux"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/successmessage"
	"gitlab.com/syd-eip/server-web/api/v1/response"
	"net/http"
)

// Description
//		Method GET.
//		This function get an Image on SYD in "syd" database.
// Parameters :
//		w http.ResponseWriter :
//			Interface used by the HTTP handler to construct the HTTP response.
//		r *http.Request :
//			HTTP request received by the API from the client.
// Return :
//	No return but the parameter w http.ResponseWriter is updated
func Download(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	image, ok := DaoImage.FindByIdSendBack(params["imageId"])
	if !ok {
		if image == errormessage.ImageNotFound {
			response.RespondWithJson(w, http.StatusNotFound, image, nil)
		} else {
			response.RespondWithJson(w, http.StatusInternalServerError, image, nil)
		}
		return
	}
	response.RespondWithJson(w, http.StatusOK, successmessage.OK, image)
}

