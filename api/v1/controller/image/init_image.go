package image

import (
	"gitlab.com/syd-eip/server-web/api/v1/repository/image"
)

// Connection to the "syd" database and tags collection.
var DaoImage = image.DbImagesStruct{}

//Connection to SYD database and Tag collection
func init() {
	DaoImage.Connect()
}
