package router

import (
	"github.com/gorilla/mux"

	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/event"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/image"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	eventroute "gitlab.com/syd-eip/server-web/api/v1/constant/event"
	eventcontroll "gitlab.com/syd-eip/server-web/api/v1/controller/event"
)

// Description :
//		Sets the routes for the event : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetEventRoute(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsInfluencer, CheckFieldsCreateEvent)
	router.HandleFunc(eventroute.CreateEventRoute, middles(eventcontroll.CreateEvent))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsInfluencer)
	router.HandleFunc(eventroute.GetAllMyEventsRoute, middles(eventcontroll.GetAllMyEvents))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(eventroute.EventByIdOrNameRoute, middles(eventcontroll.GetEvent))
	router.HandleFunc(eventroute.GetEventsByUserRoute, middles(eventcontroll.GetEventsByUser))
	router.HandleFunc(eventroute.GetEventsByTagRoute, middles(eventcontroll.GetEventsByTags))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckDelete, CheckTokenValidity, IsGuest, IsInfluencerOrAdmin)
	router.HandleFunc(eventroute.DeleteEventRoute, middles(eventcontroll.DeleteEvent))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsInfluencerOrAdmin, CheckFieldNameEvent)
	router.HandleFunc(eventroute.PatchEventNameRoute, middles(eventcontroll.PatchEventName))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsInfluencerOrAdmin, CheckFieldDescriptionEvent)
	router.HandleFunc(eventroute.PatchEventDescriptionRoute, middles(eventcontroll.PatchEventDescription))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsInfluencerOrAdmin, CheckFieldTagsEvent)
	router.HandleFunc(eventroute.PatchEventTagsRoute, middles(eventcontroll.PatchEventTags))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsInfluencerOrAdmin, CheckFieldsUpdateImage)
	router.HandleFunc(eventroute.PatchEventImageRoute, middles(eventcontroll.PatchEventImage))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsUser, CheckFieldsLottery)
	router.HandleFunc(eventroute.AddBetByEventRoute, middles(eventcontroll.AddBet))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(eventroute.GetBetByEventRoute, middles(eventcontroll.GetBet))
}
