package router

import (
	"github.com/gorilla/mux"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/image"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/tag"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	tagroute "gitlab.com/syd-eip/server-web/api/v1/constant/tag"
	tagcontroll "gitlab.com/syd-eip/server-web/api/v1/controller/tag"
)

// Description :
//		Sets the routes for the tags : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetTagRoute(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsAdmin, CheckFieldsCreateTag)
	router.HandleFunc(tagroute.CreateTagRoute, middles(tagcontroll.CreateTag))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckDelete, CheckTokenValidity, IsGuest, IsAdmin)
	router.HandleFunc(tagroute.DeleteTagRoute, middles(tagcontroll.DeleteTag))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsAdmin, CheckFieldsUpdateImage)
	router.HandleFunc(tagroute.UpdateImageRoute, middles(tagcontroll.UpdateTagImage))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(tagroute.GetTagsName, middles(tagcontroll.GetAllTagsName))
	router.HandleFunc(tagroute.TagRoute, middles(tagcontroll.GetAllTags))
	router.HandleFunc(tagroute.TagByIdOrNameRoute, middles(tagcontroll.GetTag))
}