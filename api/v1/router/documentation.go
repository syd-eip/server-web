package router

import (
	"net/http"

	"github.com/codemodus/chain"
	"github.com/gorilla/mux"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/doc"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/staticfiles"
)

// Description :
//		Sets the routes for the documentation : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetDocumentationRoute(router *mux.Router) {
	middles := chain.New(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsSuperAdmin)

	router.PathPrefix(DocumentationRoute).Handler(middles.End(http.StripPrefix(DocumentationRoute,
		http.FileServer(http.Dir(IndexDocumentationPath)))))
}
