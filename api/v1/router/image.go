package router

import (
	"github.com/gorilla/mux"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"

	imageroute "gitlab.com/syd-eip/server-web/api/v1/constant/image"
	imagecontroll "gitlab.com/syd-eip/server-web/api/v1/controller/image"
)

// Description :
//		Sets the routes for the images : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetImageRoute(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(imageroute.DownloadImageRoute, middles(imagecontroll.Download))
}
