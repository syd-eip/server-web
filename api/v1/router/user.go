package router

import (
	"github.com/gorilla/mux"

	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/image"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/user"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	userRoute "gitlab.com/syd-eip/server-web/api/v1/constant/user"
	userController "gitlab.com/syd-eip/server-web/api/v1/controller/user"
)

// Description :
//		Sets the routes for the user : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetUserRoute(router *mux.Router) {

	setUserRouteAuthentication(router)
	setUserRouteGetUsers(router)
	setUserRouteUpdateUsersData(router)
	setUserRouteRoles(router)
	setUserRouteTokens(router)
	setUserRouteForgetPassword(router)
}

// Description :
//		Sets the routes for the user -> user authentication.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteAuthentication(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckFieldsSignUp)
	router.HandleFunc(userRoute.UserSignUpRoute, middles(userController.SignUpUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckFieldsSignIn)
	router.HandleFunc(userRoute.UserSignInUserRoute, middles(userController.SignInUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest)
	router.HandleFunc(userRoute.UserSignOutUserRoute, middles(userController.SignOutUser))
}

// Description :
//		Sets the routes for the user -> get user's profiles.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteGetUsers(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsSuperAdmin)
	router.HandleFunc(userRoute.UserRoute, middles(userController.GetAllUsers))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGetOrDelete, CheckTokenValidity, IsGuest, IsSuperAdmin)
	router.HandleFunc(userRoute.UserByIdOrUsernameRoute, middles(userController.GetOrDeleteUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(userRoute.UserPublicRoute, middles(userController.GetUserPublic))
	router.HandleFunc(userRoute.UserInfluencerRoute, middles(userController.GetAllInfluencers))
}

// Description :
//		Sets the routes for the user -> update user's data.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteUpdateUsersData(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsName)
	router.HandleFunc(userRoute.UserSettingNameRoute, middles(userController.UpdateUserName))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsLocation)
	router.HandleFunc(userRoute.UserSettingLocationRoute, middles(userController.UpdateUserLocation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsBirthday)
	router.HandleFunc(userRoute.UserSettingBirthdayRoute, middles(userController.UpdateUserBirthday))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsUsername)
	router.HandleFunc(userRoute.UserSettingUsernameRoute, middles(userController.UpdateUserUsername))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsEmailUnique)
	router.HandleFunc(userRoute.UserSettingEmailRoute, middles(userController.UpdateUserEmail))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsPhone)
	router.HandleFunc(userRoute.UserSettingPhoneRoute, middles(userController.UpdateUserPhone))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsPassword)
	router.HandleFunc(userRoute.UserSettingPasswordRoute, middles(userController.UpdateUserPassword))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsUpdateImage)
	router.HandleFunc(userRoute.UserSettingImageRoute, middles(userController.UpdateUserImage))
}

// Description :
//		Sets the routes for the user -> roles management.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteRoles(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsRole)
	router.HandleFunc(userRoute.UserSettingRoleAddRoute, middles(userController.AddRoleToUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsRole)
	router.HandleFunc(userRoute.UserSettingRoleRemoveRoute, middles(userController.RemoveRoleFromUser))
}

// Description :
//		Sets the routes for the user -> tokens management.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteTokens(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldUpdateToken)
	router.HandleFunc(userRoute.UserAddTokensRoute, middles(userController.AddTokensToUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldUpdateToken)
	router.HandleFunc(userRoute.UserRemoveTokensRoute, middles(userController.RemoveTokensFromUser))
}

// Description :
//		Sets the routes for the user -> forget password.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserRouteForgetPassword(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckFieldsEmailExists)
	router.HandleFunc(userRoute.UserResetPasswordRoute, middles(userController.SendMailToUserToResetPassword))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(userRoute.UserPasswordCheckTokenRoute, middles(userController.GetResetPasswordCheckToken))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckFieldsPassword)
	router.HandleFunc(userRoute.UserUpdatePasswordWithTokenRoute, middles(userController.UpdatePasswordWithToken))
}
