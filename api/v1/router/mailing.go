package router

import (
	"github.com/gorilla/mux"

	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/mailing"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	mailRoute "gitlab.com/syd-eip/server-web/api/v1/constant/mailing"
	mailController "gitlab.com/syd-eip/server-web/api/v1/controller/mailing"
)

// Description :
//		Sets the routes for the mailing : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetMailingRoute(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsSendMail)
	router.HandleFunc(mailRoute.MailRoute, middles(mailController.SendMailFromScratch))
}
