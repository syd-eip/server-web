package router

import (
	"net/http"

	"github.com/codemodus/chain"
	"github.com/gorilla/mux"

	. "gitlab.com/syd-eip/server-web/api/v1/constant/staticdata"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/staticfiles"
)

// Description :
//		Sets the routes for the static data the API is going to serve.
//		Matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetStaticDataRoute(router *mux.Router) {
	middles := chain.New(SecurityResponseHeader, CheckGet)

	router.PathPrefix(StaticDataRoute).Handler(middles.End(http.StripPrefix(StaticDataRoute,
		http.FileServer(http.Dir(IndexStaticPath)))))
}
