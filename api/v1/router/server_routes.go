// This package provides all the functions to set and handle the server's routes.
package router

import (
	"log"
	"os"

	"github.com/gorilla/mux"

	servconst "gitlab.com/syd-eip/server-web/api/v1/constant/server"
)

// Description :
//		- Definition of the server's routes :
//			- users routes
//			- events routes
//			- tags routes
//			- static routes
//			- documentation routes if the environment is set to development.
func SetServerRoutes() *mux.Router {
	serverMux := mux.NewRouter()

	SetUserRoute(serverMux)
	SetUserSelfRoute(serverMux)

	SetEventRoute(serverMux)

	SetTagRoute(serverMux)

	SetAssociationRoute(serverMux)

	SetImageRoute(serverMux)

	SetStaticDataRoute(serverMux)

	SetMailingRoute(serverMux)

	if os.Getenv(servconst.Dev) == "true" {
		log.Println("Environment of development is DEV => Setting up Documentation's routes")
		SetDocumentationRoute(serverMux)
	}

	log.Println("Server route's are set up")
	return serverMux
}
