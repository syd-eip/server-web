package router

import (
	"github.com/gorilla/mux"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/image"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/user"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/user/favorite"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	userRoute "gitlab.com/syd-eip/server-web/api/v1/constant/user"
	userController "gitlab.com/syd-eip/server-web/api/v1/controller/user"
)

// Description :
//		Sets the routes for the user/me : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetUserSelfRoute(router *mux.Router) {

	setUserSelfRouteGetOrDeleteMe(router)
	setUserSelfRouteDataUpdate(router)
	setUserSelfRouteCheckAvailability(router)
	setUserSelfRouteFavorites(router)
	setUserSelfRouteDiscover(router)
	setUserSelfRouteTokens(router)
}

// Description :
//		Sets the routes for the user/me -> get my profile or delete me: matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteGetOrDeleteMe(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckGetOrDelete, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.Self, middles(userController.SelectMethodMeUser))
}

// Description :
//		Sets the routes for the user/me -> update my data: matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteDataUpdate(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsName)
	router.HandleFunc(userRoute.SelfSettingName, middles(userController.UpdateMeUserName))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsLocation)
	router.HandleFunc(userRoute.SelfSettingLocation, middles(userController.UpdateMeUserLocation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsBirthday)
	router.HandleFunc(userRoute.SelfSettingBirthday, middles(userController.UpdateMeUserBirthday))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsUsername)
	router.HandleFunc(userRoute.SelfSettingUsername, middles(userController.UpdateMeUserUsername))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsEmailUnique)
	router.HandleFunc(userRoute.SelfSettingEmail, middles(userController.UpdateMeUserEmail))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsPhone)
	router.HandleFunc(userRoute.SelfSettingPhone, middles(userController.UpdateMeUserPhone))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsPassword)
	router.HandleFunc(userRoute.SelfSettingPassword, middles(userController.UpdateMeUserPassword))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsUpdateImage)
	router.HandleFunc(userRoute.SelfSettingImage, middles(userController.UpdateMeUserImage))
}

// Description :
//		Sets the routes for the user/me -> check data availability.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteCheckAvailability(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsUser, CheckFieldsUsername)
	router.HandleFunc(userRoute.SelfCheckAvailableUsername, middles(userController.CheckUserUsernameAvailability))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsUser, CheckFieldsEmailUnique)
	router.HandleFunc(userRoute.SelfCheckAvailableEmail, middles(userController.CheckUserEmailAvailability))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsUser, CheckFieldsPhone)
	router.HandleFunc(userRoute.SelfCheckAvailablePhone, middles(userController.CheckUserPhoneAvailability))
}

// Description :
//		Sets the routes for the user/me -> favorites: matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteFavorites(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.SelfFavorite, middles(userController.GetUserFavorites))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.SelfFavoriteTag, middles(userController.GetUserFavoritesTags))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteTag)
	router.HandleFunc(userRoute.SelfFavoriteTagAdd, middles(userController.AddFavoriteTag))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteTag)
	router.HandleFunc(userRoute.SelfFavoriteTagRemove, middles(userController.RemoveFavoriteTag))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.SelfFavoriteUser, middles(userController.GetUserFavoritesUsers))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteUser)
	router.HandleFunc(userRoute.SelfFavoriteUserAdd, middles(userController.AddFavoriteUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteUser)
	router.HandleFunc(userRoute.SelfFavoriteUserRemove, middles(userController.RemoveFavoriteUser))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.SelfFavoriteAssociation, middles(userController.GetUserFavoritesAssociations))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteAssociation)
	router.HandleFunc(userRoute.SelfFavoriteAssociationAdd, middles(userController.AddFavoriteAssociation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldsFavoriteAssociation)
	router.HandleFunc(userRoute.SelfFavoriteAssociationRemove, middles(userController.RemoveFavoriteAssociation))
}

// Description :
//		Sets the routes for the user/me -> discover: matches the routes and the handlers.
//		The discover routes use the favorites to match the events that may please the authenticated user the most.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteDiscover(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckGet, CheckTokenValidity, IsGuest, IsUser)
	router.HandleFunc(userRoute.SelfDiscover, middles(userController.GetUserDiscover))
}

// Description :
//		Sets the routes for the user/me -> tokens management: matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func setUserSelfRouteTokens(router *mux.Router) {

	middles := ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsUser, CheckFieldUpdateToken)
	router.HandleFunc(userRoute.SelfAddTokensUser, middles(userController.AddTokensToUserMe))
}
