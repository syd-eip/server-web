package router

import (
	"github.com/gorilla/mux"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/association"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/payload/image"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/requestmethod"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/role"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/security"
	. "gitlab.com/syd-eip/server-web/api/v1/middleware/token"

	associationRoute "gitlab.com/syd-eip/server-web/api/v1/constant/association"
	associationController "gitlab.com/syd-eip/server-web/api/v1/controller/association"
)

// Description :
//		Sets the routes for the associations : matches the routes and the handlers.
// Parameters :
//		router *mux.Router :
//			Router registers routes to be matched and dispatches a handler.
func SetAssociationRoute(router *mux.Router) {
	middles := ChainMiddlewares(SecurityResponseHeader, CheckPost, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsSignUp)
	router.HandleFunc(associationRoute.CreateAssociationRoute, middles(associationController.CreateAssociation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckDelete, CheckTokenValidity, IsGuest, IsSuperAdmin)
	router.HandleFunc(associationRoute.DeleteAssociationRoute, middles(associationController.DeleteAssociation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckGet)
	router.HandleFunc(associationRoute.AssociationRoute, middles(associationController.GetAllAssociations))
	router.HandleFunc(associationRoute.AssociationIdOrNameRoute, middles(associationController.GetAssociation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsName)
	router.HandleFunc(associationRoute.AssociationSettingNameRoute, middles(associationController.UpdateAssociationName))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsDescription)
	router.HandleFunc(associationRoute.AssociationSettingDescriptionRoute, middles(associationController.UpdateAssociationDescription))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsEmail)
	router.HandleFunc(associationRoute.AssociationSettingEmailRoute, middles(associationController.UpdateAssociationEmail))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsLocation)
	router.HandleFunc(associationRoute.AssociationSettingLocationRoute, middles(associationController.UpdateAssociationLocation))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsPhone)
	router.HandleFunc(associationRoute.AssociationSettingPhoneRoute, middles(associationController.UpdateAssociationPhone))

	middles = ChainMiddlewares(SecurityResponseHeader, CheckPatch, CheckTokenValidity, IsGuest, IsSuperAdmin, CheckFieldsUpdateImage)
	router.HandleFunc(associationRoute.AssociationSettingImageRoute, middles(associationController.UpdateAssociationImage))
}
