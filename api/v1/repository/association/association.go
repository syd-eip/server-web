// This package represents all the functions to handle the database, the collection associations and the connection to them.
package association

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/model"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and associations collection.
type DbAssociationsStruct struct{}

// Represents the connection session to the associations collection.
var dbAssociations *mgo.Database

// Represents the collection associations' name.
var collectionAssociations = os.Getenv(constdb.CollectionAssociations)

// Description :
//		Connects to the "syd" database.
func (m *DbAssociationsStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(mongoTimeout) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbAssociations = session.DB(mongocred.SydDb)
}

// Description :
//		Find all associations in the associations collection.
// Returns :
//		[]model.Association :
//			Array of associations returned if one or more association(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) FindAll() ([]model.Association, error) {
	var associations []model.Association
	err := dbAssociations.C(collectionAssociations).Find(bson.M{}).All(&associations)
	return associations, err
}

// Description :
//		Find a association thanks to its "id" in the associations collection.
// Parameters :
//		id string :
//			Id of the association to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the association is found.
//			- false if not.
func (m *DbAssociationsStruct) FindById(id string) bool {
	var association model.Association

	if !bson.IsObjectIdHex(id) {
		return false
	}
	err := dbAssociations.C(collectionAssociations).FindId(bson.ObjectIdHex(id)).One(&association)
	if err == nil || association.Name != "" {
		return true
	}
	return false
}

// Description :
//		Find a association thanks to its "id" in the associations collection and return it.
// Parameters :
//		id string :
//			Id of the association to find.
// Returns :
//		model.Association :
//			Association returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) FindByIdSendBack(id string) (model.Association, error) {
	var associationFound model.Association

	if !bson.IsObjectIdHex(id) {
		err := errors.New("invalid input to ObjectIdHex: " + id)
		return associationFound, err
	}
	err := dbAssociations.C(collectionAssociations).FindId(bson.ObjectIdHex(id)).One(&associationFound)
	return associationFound, err
}

// Description :
//		Find a association thanks to his "name" in the associations collection.
// Parameters :
//		name string :
//			name of the association to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the association is found.
//			- false if not.
func (m *DbAssociationsStruct) FindByName(associationName string) bool {
	var associationFound model.Association
	err := dbAssociations.C(collectionAssociations).Find(bson.M{"name": associationName}).One(&associationFound)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a association thanks to his "phone" in the associations collection.
// Parameters :
//		phone string :
//			phone of the association to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the association is found.
//			- false if not.
func (m *DbAssociationsStruct) FindByPhone(phone string) bool {
	var association model.Association
	err := dbAssociations.C(collectionAssociations).Find(bson.M{"phone.internationalnumber": phone}).One(&association)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a association thanks to his "email" in the associations collection.
// Parameters :
//		Email string :
//			Email of the association to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the association is found.
//			- false if not.
func (m *DbAssociationsStruct) FindByEmail(email string) bool {
	var association model.Association
	err := dbAssociations.C(collectionAssociations).Find(bson.M{"email": email}).One(&association)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a association thanks to his "name" in the associations collection and return it.
// Parameters :
//		Name string :
//			Name of the association to find.
// Returns :
//		model.Association :
//			Association returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) FindByNameSendBack(associationName string) (model.Association, error) {
	var association model.Association
	err := dbAssociations.C(collectionAssociations).Find(bson.M{"name": associationName}).One(&association)
	return association, err
}

// Description :
//		Insert an element in the associations collection in the "syd" database.
// Parameters :
//		association model.Association :
//			Association to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) Insert(association model.Association) error {
	association.CreationDate = time.Now().Format(time.RFC3339)
	association.UpdateDate = association.CreationDate

	err := dbAssociations.C(collectionAssociations).Insert(&association)
	return err
}

// Description :
//		Delete an element from the associations collection in the "syd" database.
// Parameters :
//		association model.Association :
//			Association to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) Delete(association model.Association) error {
	err := dbAssociations.C(collectionAssociations).Remove(bson.M{"_id": association.Id})
	return err
}

// Description :
//		Update an element in the associations collection in the "syd" database.
// Parameters :
//		association model.Association :
//			Association to update in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbAssociationsStruct) Update(association model.Association) error {
	association.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbAssociations.C(collectionAssociations).UpdateId(association.Id, &association)
	return err
}
