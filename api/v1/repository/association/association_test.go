package association

import (
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

var DaoAssociation = DbAssociationsStruct{}

func TestDbAssociationsStruct_FindAll(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name           string
		nbAssociations int
	}{
		{
			name:           "Find associations by default in database",
			nbAssociations: 2,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			associationsFound, findErr := DaoAssociation.FindAll()
			if findErr != nil {
				t.Fatalf(findErr.Error())
			}

			if len(associationsFound) != tc.nbAssociations {
				t.Fatalf("ERROR: expected %v associations in associations database  _  got %v", tc.nbAssociations, len(associationsFound))
			}
		})
	}
}

func TestDbAssociationsStruct_FindById(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name           string
		associationId  string
		status         bool
		nbAssociations int
	}{
		{
			name:          "association_exist",
			associationId: "5dde55efc205a927ef4c1fff",
			status:        true,
		},
		{
			name:          "association_not_exist",
			associationId: "5dde55efc205a927ef4c1ffe",
			status:        false,
		},
		{
			name:          "association_id_not_id",
			associationId: "fail",
			status:        false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isAssociationFound := DaoAssociation.FindById(tc.associationId)
			if isAssociationFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isAssociationFound)
			}
		})
	}
}

func TestDbAssociationsStruct_FindByIdSendBack(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name          string
		associationId string
		status        bool
	}{
		{
			name:          "association_exist",
			associationId: "5dde55efc205a927ef4c1fff",
			status:        true,
		},
		{
			name:          "association_not_exist",
			associationId: "5dde55efc205a927ef4c1ffe",
			status:        false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			association, err := DaoAssociation.FindByIdSendBack(tc.associationId)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", association.Id.Hex())
			}
			if tc.status == true && association.Id.Hex() != tc.associationId {
				t.Fatalf("ERROR: expected associationId %v  _  got %v", tc.associationId, association.Id.Hex())
			}
		})
	}
}

func TestDbAssociationsStruct_FindByName(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name            string
		associationName string
		status          bool
		nbAssociations  int
	}{
		{
			name:            "association_exist",
			associationName: "Test",
			status:          true,
		},
		{
			name:            "association_not_exist",
			associationName: "FAIL",
			status:          false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isAssociationFound := DaoAssociation.FindByName(tc.associationName)
			if isAssociationFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isAssociationFound)
			}
		})
	}
}

func TestDbAssociationsStruct_FindByNameSendBackAssociation(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name            string
		associationName string
		status          bool
	}{
		{
			name:            "association_exist",
			associationName: "Test",
			status:          true,
		},
		{
			name:            "association_not_exist",
			associationName: "FAIL",
			status:          false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			association, err := DaoAssociation.FindByNameSendBack(tc.associationName)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", association.Name)
			}
			if tc.status == true && association.Name != tc.associationName {
				t.Fatalf("ERROR: expected associationId %v  _  got %v", tc.associationName, association.Name)
			}
		})
	}
}

func TestDbAssociationsStruct_FindByEmail(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name   string
		email  string
		status bool
	}{
		{
			name:   "association_exist",
			email:  "test.test@test.com",
			status: true,
		},
		{
			name:   "association_not_exist",
			email:  "fail.fail@fail.com",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isAssociationFound := DaoAssociation.FindByEmail(tc.email)
			if isAssociationFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isAssociationFound)
			}
		})
	}
}

func TestDbAssociationsStruct_FindByPhone(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name        string
		phoneNumber string
		status      bool
	}{
		{
			name:        "association_exist",
			phoneNumber: "+33222222222",
			status:      true,
		},
		{
			name:        "association_not_exist",
			phoneNumber: "+33333333333",
			status:      false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isAssociationFound := DaoAssociation.FindByPhone(tc.phoneNumber)
			if isAssociationFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isAssociationFound)
			}
		})
	}
}

func TestDbAssociationsStruct_Insert(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name              string
		associationInsert string
		associationName   string
	}{
		{
			name:              "Insert Association in Database",
			associationInsert: "{\"name\":\"insertTest\"}",
			associationName:   "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var association model.Association
			parseErr := json.Unmarshal([]byte(tc.associationInsert), &association)
			association.Id = bson.NewObjectId()
			insertErr := DaoAssociation.Insert(association)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoAssociation.Delete(association)

			if !DaoAssociation.FindByName(tc.associationName) {
				t.Fatalf("ERROR: association %v not found", tc.associationName)
			}
		})
	}
}

func TestDbAssociationsStruct_Delete(t *testing.T) {
	DaoAssociation.Connect()
	tt := []struct {
		name            string
		association     string
		associationName string
	}{
		{
			name:            "Delete Association in Database",
			association:     "{\"name\":\"insertTest\"}",
			associationName: "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var association model.Association
			parseErr := json.Unmarshal([]byte(tc.association), &association)
			association.Id = bson.NewObjectId()
			insertErr := DaoAssociation.Insert(association)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoAssociation.Delete(association)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoAssociation.FindByName(tc.associationName) {
				t.Fatalf("ERROR: association %v was found and not deleted", tc.associationName)
			}
		})
	}
}

func TestDbAssociationsStruct_Update(t *testing.T) {
	DaoAssociation.Connect()

	tt := []struct {
		name                   string
		association            string
		associationName        string
		updatedAssociationName string
	}{
		{
			name:                   "Update Association in DataBase",
			association:            "{\"name\":\"insertTest\"}",
			associationName:        "insertTest",
			updatedAssociationName: "updatedTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var association model.Association
			parseErr := json.Unmarshal([]byte(tc.association), &association)
			association.Id = bson.NewObjectId()
			insertErr := DaoAssociation.Insert(association)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			parseUpdatedErr := json.Unmarshal([]byte(tc.association), &association)
			if parseUpdatedErr != nil {
				t.Fatalf(parseUpdatedErr.Error())
			}

			association.Name = tc.updatedAssociationName
			err := DaoAssociation.Update(association)
			if err != nil {
				t.Fatalf(err.Error())
			}
			defer DaoAssociation.Delete(association)

			if DaoAssociation.FindByName(tc.associationName) {
				t.Fatalf("ERROR: association %v was no updated", tc.associationName)
			}
			if !DaoAssociation.FindByName(tc.updatedAssociationName) {
				t.Fatalf("ERROR: association %v not found", tc.updatedAssociationName)
			}
		})
	}
}
