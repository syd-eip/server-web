// This package represents all the functions to handle the database, the collection images and the connection to them.
package image

import (
	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gopkg.in/mgo.v2"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/model"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and images collection.
type DbImagesStruct struct{}

// Represents the connection session to the images collection.
var dbImages *mgo.Database

// Represents the collection images' name.
var collectionImages = os.Getenv(constdb.CollectionImages)

// Description :
//		Connects to the "syd" database.
func (m *DbImagesStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(time.Duration(mongoTimeout)) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbImages = session.DB(mongocred.SydDb)
}

// Description :
//		Find a image thanks to its "id" in the images collection.
// Parameters :
//		id string :
//			Id of the image to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the image is found.
//			- false if not.
func (m *DbImagesStruct) FindById(imageId string) bool {
	file, err := dbImages.GridFS(collectionImages).Open(imageId)
	if err != nil {
		return false
	}
	defer file.Close()
	return true
}

// Description :
//		Find a image thanks to its "id" in the images collection and return it.
// Parameters :
//		id string :
//			Id of the image to find.
// Returns :
//		model.Image :
//			Image returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbImagesStruct) FindByIdSendBack(imageId string) (string, bool) {
	file, err := dbImages.GridFS(collectionImages).Open(imageId)
	if err != nil {
		return errormessage.ImageNotFound, false
	}
	defer file.Close()

	buffer := make([]byte, file.Size())
	_, err = file.Read(buffer)
	if err != nil {
		return errormessage.ImageCorrupted, false
	}

	return string(buffer), true
}

// Description :
//		Insert an element in the images collection in the "syd" database.
// Parameters :
//		image model.Image :
//			Image to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbImagesStruct) Insert(image model.UploadRequestImage) error {
	image.CreationDate = time.Now().Format(time.RFC3339)
	image.UpdateDate = image.CreationDate

	file, err := dbImages.GridFS(collectionImages).Create(image.Name)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write([]byte(image.Content))
	if err != nil {
		return err
	}

	return nil
}

// Description :
//		Delete an element from the images collection in the "syd" database.
// Parameters :
//		image model.Image :
//			Image to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbImagesStruct) Delete(imageId string) error {
	err := dbImages.GridFS(collectionImages).Remove(imageId)
	return err
}

// Description :
//		Update an element in the images collection in the "syd" database.
// Parameters :
//		image model.Image :
//			Image to update in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbImagesStruct) Update(image model.UploadRequestImage) error {
	image.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbImages.GridFS(collectionImages).Remove(image.Name)
	if err != nil {
		return err
	}

	file, err := dbImages.GridFS(collectionImages).Create(image.Name)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write([]byte(image.Content))
	if err != nil {
		return err
	}

	return nil
}
