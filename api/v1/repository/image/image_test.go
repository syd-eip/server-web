package image

import (
	"encoding/json"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

var DaoImage = DbImagesStruct{}

func TestDbImagesStruct_Insert(t *testing.T) {
	DaoImage.Connect()

	tt := []struct {
		name      string
		imageInsert string
		image   string
	}{
		{
			name:      "Insert Image in Database",
			imageInsert: "{" +
				"\"name\":\"insertTest\"," +
				"\"content\":\"image1\"" +
				"}",
			image:   "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var image model.UploadRequestImage
			parseErr := json.Unmarshal([]byte(tc.imageInsert), &image)
			insertErr := DaoImage.Insert(image)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoImage.Delete(image.Name)

			if !DaoImage.FindById(tc.image) {
				t.Fatalf("ERROR: image %v not found", tc.image)
			}
		})
	}
}

func TestDbImagesStruct_Delete(t *testing.T) {
	DaoImage.Connect()
	tt := []struct {
		name    string
		image     string
		imageName string
	}{
		{
			name:    "Delete Image in Database",
			image:     "{\"name\":\"insertTest\"}",
			imageName: "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var image model.UploadRequestImage
			parseErr := json.Unmarshal([]byte(tc.image), &image)
			insertErr := DaoImage.Insert(image)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoImage.Delete(image.Name)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoImage.FindById(tc.imageName) {
				t.Fatalf("ERROR: image %v was found and not deleted", tc.imageName)
			}
		})
	}
}

func TestDbImagesStruct_Update(t *testing.T) {
	DaoImage.Connect()

	tt := []struct {
		name           string
		user           string
		imageName        string
		updatedImageName string
	}{
		{
			name:           "Update Image in DataBase",
			user:           "{\"name\":\"updatedTest\"}",
			imageName:        "insertTest",
			updatedImageName: "updatedTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var image model.UploadRequestImage
			parseErr := json.Unmarshal([]byte(tc.user), &image)
			insertErr := DaoImage.Insert(image)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			parseUpdatedErr := json.Unmarshal([]byte(tc.user), &image)
			if parseUpdatedErr != nil {
				t.Fatalf(parseUpdatedErr.Error())
			}

			image.Name = tc.updatedImageName
			err := DaoImage.Update(image)
			if err != nil {
				t.Fatalf(err.Error())
			}
			defer DaoImage.Delete(image.Name)

			if DaoImage.FindById(tc.imageName) {
				t.Fatalf("ERROR: image %v was not updated", tc.imageName)
			}
			if !DaoImage.FindById(tc.updatedImageName) {
				t.Fatalf("ERROR: image %v not found", tc.updatedImageName)
			}
		})
	}
}
