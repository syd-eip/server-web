// This package represents all the functions to handle the database, the collection users and the connection to them.
package user

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/constant/message/errormessage"
	"gitlab.com/syd-eip/server-web/api/v1/model"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and users collection.
type DbUsersStruct struct{}

// Represents the connection session to the users collection.
var dbUsers *mgo.Database

// Represents the collection users' name.
var collectionUsers = os.Getenv(constdb.CollectionUsers)

// Description :
//		Connects to the "syd" database.
func (m *DbUsersStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(mongoTimeout) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbUsers = session.DB(mongocred.SydDb)
}

// Description :
//		Find all users in the users collection.
// Returns :
//		[]model.User :
//			Array of users returned if one or more user(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) FindAll() ([]model.User, error) {
	var users []model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{}).All(&users)
	return users, err
}

// Description :
//		Find a user thanks to his "id" in the users collection.
// Parameters :
//		id string :
//			Id of the user to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the user is found.
//			- false if not.
func (m *DbUsersStruct) FindById(id string) bool {
	var user model.User

	if !bson.IsObjectIdHex(id) {
		return false
	}

	err := dbUsers.C(collectionUsers).FindId(bson.ObjectIdHex(id)).One(&user)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a user thanks to his "username" in the users collection.
// Parameters :
//		username string :
//			Username of the user to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the user is found.
//			- false if not.
func (m *DbUsersStruct) FindByUsername(username string) bool {
	var user model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{"username": username}).One(&user)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a user thanks to his "phone" in the users collection.
// Parameters :
//		phone string :
//			phone of the user to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the user is found.
//			- false if not.
func (m *DbUsersStruct) FindByPhone(phone string) bool {
	var user model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{"phone.internationalnumber": phone}).One(&user)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a user thanks to his "email" in the users collection.
// Parameters :
//		Email string :
//			Email of the user to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the user is found.
//			- false if not.
func (m *DbUsersStruct) FindByEmail(email string) bool {
	var user model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{"email": email}).One(&user)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a user thanks to his "id" in the users collection.
// Parameters :
//		id string :
//			Id of the user to find.
// Returns :
//		model.User :
//			User returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) FindByIdSendBack(id string) (model.User, error) {
	var user model.User

	if !bson.IsObjectIdHex(id) {
		err := errors.New("invalid input to ObjectIdHex: " + id)
		return user, err
	}

	err := dbUsers.C(collectionUsers).FindId(bson.ObjectIdHex(id)).One(&user)
	return user, err
}

// Description :
//		Find a user thanks to his "username" in the users collection.
// Parameters :
//		username string :
//			Username of the user to find.
// Returns :
//		model.User :
//			User returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) FindByUsernameSendBackUser(username string) (model.User, error) {
	var user model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{"username": username}).One(&user)
	return user, err
}

// Description :
//		Find a user thanks to his "email" in the users collection.
// Parameters :
//		Email string :
//			Email of the user to find.
// Returns :
//		model.User :
//			User returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) FindByEmailSendBackUser(email string) (model.User, error) {
	var user model.User
	err := dbUsers.C(collectionUsers).Find(bson.M{"email": email}).One(&user)
	return user, err
}

// Description :
//		Add tokens to the user wallet thanks to his "id"
// Parameters :
//		id string :
//			Id of the user to find.
// Returns :
//		model.User :
//			User returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) AddTokensToWallet(id string, value int64) (int64, error) {
	var user model.User

	getErr := dbUsers.C(collectionUsers).Find(bson.M{"username": id}).One(&user)
	if getErr != nil {
		if !bson.IsObjectIdHex(id) {
			return 0, getErr
		}
		getErr = dbUsers.C(collectionUsers).FindId(bson.ObjectIdHex(id)).One(&user)
		if getErr != nil {
			return 0, getErr
		}
	}

	user.Wallet += value

	updateErr := dbUsers.C(collectionUsers).UpdateId(user.Id, &user)
	if updateErr != nil {
		return 0, updateErr
	}

	return user.Wallet, nil
}

// Description :
//		Remove tokens from the user wallet thanks to his "id"
// Parameters :
//		id string :
//			Id of the user to find.
// Returns :
//		model.User :
//			User returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) RemoveTokensFromWallet(id string, value int64) (int64, error) {
	var user model.User

	getErr := dbUsers.C(collectionUsers).Find(bson.M{"username": id}).One(&user)
	if getErr != nil {
		if !bson.IsObjectIdHex(id) {
			return 0, getErr
		}
		getErr = dbUsers.C(collectionUsers).FindId(bson.ObjectIdHex(id)).One(&user)
		if getErr != nil {
			return 0, getErr
		}
	}

	if value > user.Wallet {
		return user.Wallet, errors.New(errormessage.UserNotEnoughTokens)
	}
	user.Wallet -= value

	updateErr := dbUsers.C(collectionUsers).UpdateId(user.Id, &user)
	if updateErr != nil {
		return 0, updateErr
	}

	return user.Wallet, nil
}

// Description :
//		Insert an element in the users collection in the "syd" database.
// Parameters :
//		user model.User :
//			User to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) Insert(user model.User) error {
	user.CreationDate = time.Now().Format(time.RFC3339)
	user.UpdateDate = user.CreationDate

	err := dbUsers.C(collectionUsers).Insert(&user)
	return err
}

// Description :
//		Delete an element from the users collection in the "syd" database.
// Parameters :
//		user model.User :
//			User to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) Delete(user model.User) error {
	err := dbUsers.C(collectionUsers).Remove(bson.M{"_id": user.Id})
	return err
}

// Description :
//		Update an element in the users collection in the "syd" database.
// Parameters :
//		user model.User :
//			User to update in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbUsersStruct) Update(user model.User) error {
	user.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbUsers.C(collectionUsers).UpdateId(user.Id, &user)
	return err
}
