package user

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gopkg.in/mgo.v2/bson"
	"testing"
)

var DaoUser = DbUsersStruct{}

func TestDbUsersStruct_FindAll(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name	string
		nbUsers	int
	}{
		{
			name : "Find users by default in database",
			nbUsers : 2,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			usersFound, findErr := DaoUser.FindAll()
			if findErr != nil {
				t.Fatalf(findErr.Error())
			}
			if len(usersFound) != tc.nbUsers {
				t.Fatalf("ERROR: expected %v users in user database  _  got %v", tc.nbUsers, len(usersFound))
			}

		})
	}
}

func TestDbUsersStruct_FindById(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name	string
		userId	string
		status	bool
	}{
		{
			name : "user_exist",
			userId : "5cef9f62c205a965e9693448",
			status: true,
		},
		{
			name : "user_not_exist",
			userId : "5cf27b0cd918e33dc31902eC",
			status: false,
		},
		{
			name : "not an ObjectIdhex",
			userId : "FAIL",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isUserFound := DaoUser.FindById(tc.userId)
			if isUserFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isUserFound)
			}
		})
	}
}

func TestDbUsersStruct_FindByUsername(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		username	string
		status		bool
	}{
		{
			name : "user_exist",
			username : "Test",
			status: true,
		},
		{
			name : "user_not_exist",
			username : "NOT_FOUND",
			status: false,
		},

	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isUserFound := DaoUser.FindByUsername(tc.username)
			if isUserFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isUserFound)
			}
		})
	}
}

func TestDbUsersStruct_FindByPhone(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		phoneNumber	string
		status		bool
	}{
		{
			name : "user_exist",
			phoneNumber : "+33222222222",
			status: true,
		},
		{
			name : "user_not_exist",
			phoneNumber : "+33333333333",
			status: false,
		},

	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isUserFound := DaoUser.FindByPhone(tc.phoneNumber)
			if isUserFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isUserFound)
			}
		})
	}
}

func TestDbUsersStruct_FindByEmail(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		email		string
		status		bool
	}{
		{
			name : "user_exist",
			email : "test.test@test.com",
			status: true,
		},
		{
			name : "user_not_exist",
			email : "fail.fail@fail.com",
			status: false,
		},

	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isUserFound := DaoUser.FindByEmail(tc.email)
			if isUserFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isUserFound)
			}
		})
	}
}

func TestDbUsersStruct_FindByIdSendBack(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		userId		string
		status		bool
	}{
		{
			name : "user_exist",
			userId : "5cef9f62c205a965e9693448",
			status: true,
		},
		{
			name : "user_not_exist",
			userId : "5cf27b0cd918e33dc31902eC",
			status: false,
		},
		{
			name : "not an ObjectIdhex",
			userId : "FAIL",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			user, err := DaoUser.FindByIdSendBack(tc.userId)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found username : %v", user.Id.Hex())
			}
			if tc.status == true && user.Id.Hex() != tc.userId {
				t.Fatalf("ERROR: expected userId %v  _  got %v", tc.userId,  user.Id.Hex())
			}
		})
	}
}

func TestDbUsersStruct_FindByUsernameSendBackUser(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		username	string
		status		bool
	}{
		{
			name : "user_exist",
			username : "Test",
			status: true,
		},
		{
			name : "user_not_exist",
			username : "NOT_FOUND",
			status: false,
		},

	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			user, err := DaoUser.FindByUsernameSendBackUser(tc.username)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found username : %v", user.Username)
			}
			if tc.status == true && user.Username != tc.username {
				t.Fatalf("ERROR: expected username %v  _  got %v", tc.username,  user.Username)
			}
		})
	}
}

func TestDbUsersStruct_FindByEmailSendBackUser(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		email		string
		status		bool
	}{
		{
			name : "user_exist",
			email : "test.test@test.com",
			status: true,
		},
		{
			name : "user_not_exist",
			email : "fail.fail@fail.com",
			status: false,
		},

	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			user, err := DaoUser.FindByEmailSendBackUser(tc.email)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found username : %v", user.Email)
			}
			if tc.status == true && user.Email != tc.email {
				t.Fatalf("ERROR: expected username %v  _  got %v", tc.email,  user.Email)
			}
		})
	}
}

func TestDbUsersStruct_AddTokensToWallet(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		userId		string
		value		int64
		status		bool
	}{
		{
			name : "user_exist + value_ok",
			userId : "5cef9f62c205a965e9693448",
			value : 10,
			status: true,
		},
		{
			name : "user_exist + value_bad",
			userId : "5cef9f62c205a965e9693448",
			value : -10,
			status: false,
		},
		{
			name : "user_not_exist + value_ok",
			userId : "5cf27b0cd918e33dc31902eC",
			value : 10,
			status: false,
		},
		{
			name : "user_not_exist + value_bad",
			userId : "5cf27b0cd918e33dc31902eC",
			value : -10,
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			_, err := DaoUser.AddTokensToWallet(tc.userId, tc.value)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
		})
	}
}

func TestDbUsersStruct_RemoveTokensToWallet(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name		string
		userId		string
		value		int64
		status		bool
	}{
		{
			name : "user_exist + value_bad",
			userId : "5cef9f62c205a965e9693448",
			value : -10,
			status: false,
		},
		{
			name : "user_exist + value_too_high",
			userId : "5cef9f62c205a965e9693448",
			value : 100,
			status: false,
		},
		{
			name : "user_exist + value_ok",
			userId : "5cef9f62c205a965e9693448",
			value : 10,
			status: true,
		},
		{
			name : "user_not_exist + value_ok",
			userId : "5cf27b0cd918e33dc31902eC",
			value : 10,
			status: false,
		},
		{
			name : "user_not_exist + value_bad",
			userId : "5cf27b0cd918e33dc31902eC",
			value : -10,
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			_, err := DaoUser.AddTokensToWallet(tc.userId, tc.value)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
		})
	}
}

func TestDbUsersStruct_Insert(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name	string
		user	string
		username	string

	}{
		{
			name : "Insert User in Database",
			user : "{\"username\":\"insertTest\"}",
			username : "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var user model.User
			parseErr := json.Unmarshal([]byte(tc.user), &user)
			user.Id = bson.NewObjectId()
			insertErr := DaoUser.Insert(user)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoUser.Delete(user)

			if !DaoUser.FindByUsername(tc.username) {
				t.Fatalf("ERROR: user %v not found", tc.username)
			}
		})
	}
}

func TestDbUsersStruct_Delete(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name	string
		user	string
		username	string

	}{
		{
			name : "Delete User in Database",
			user : "{\"username\":\"insertTest\"}",
			username : "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var user model.User
			parseErr := json.Unmarshal([]byte(tc.user), &user)
			user.Id = bson.NewObjectId()
			insertErr := DaoUser.Insert(user)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoUser.Delete(user)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoUser.FindByUsername(tc.username) {
				t.Fatalf("ERROR: user %v was found and not deleted", tc.username)
			}
		})
	}
}

func TestDbUsersStruct_Update(t *testing.T) {
	DaoUser.Connect()

	tt := []struct {
		name			string
		user			string
		username		string
		updatedUsername	string

	}{
		{
			name : "update User in DataBase",
			user : "{\"username\":\"insertTest\"}",
			username : "insertTest",
			updatedUsername : "updatedTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var user model.User
			parseErr := json.Unmarshal([]byte(tc.user), &user)
			user.Id = bson.NewObjectId()
			insertErr := DaoUser.Insert(user)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			parseUpdatedErr := json.Unmarshal([]byte(tc.user), &user)
			if parseUpdatedErr != nil {
				t.Fatalf(parseUpdatedErr.Error())
			}

			user.Username = tc.updatedUsername
			err := DaoUser.Update(user)
			if err != nil {
				t.Fatalf(err.Error())
			}
			defer DaoUser.Delete(user)

			if DaoUser.FindByUsername(tc.username) {
				t.Fatalf("ERROR: user %v was no updated", tc.username)
			}
			if !DaoUser.FindByUsername(tc.updatedUsername) {
				t.Fatalf("ERROR: user %v not found", tc.updatedUsername)
			}
		})
	}
}
