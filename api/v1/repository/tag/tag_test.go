package tag

import (
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"testing"

	"gitlab.com/syd-eip/server-web/api/v1/model"
)

var DaoTag = DbTagsStruct{}

func TestDbTagsStruct_FindAll(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name   string
		nbTags int
	}{
		{
			name:   "Find tags by default in database",
			nbTags: 8,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tagsFound, findErr := DaoTag.FindAll()
			if findErr != nil {
				t.Fatalf(findErr.Error())
			}

			if len(tagsFound) != tc.nbTags {
				t.Fatalf("ERROR: expected %v tags in tags database  _  got %v", tc.nbTags, len(tagsFound))
			}
		})
	}
}

func TestDbTagsStruct_FindById(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name   string
		tagId  string
		status bool
		nbTags int
	}{
		{
			name:   "tag_exist",
			tagId:  "5d89fe0cd918e31bfa32498e",
			status: true,
		},
		{
			name:   "tag_not_exist",
			tagId:  "5cf27b0cd918e33dc31902eA",
			status: false,
		},
		{
			name:   "tag_id_not_id",
			tagId:  "fail",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isTagFound := DaoTag.FindById(tc.tagId)
			if isTagFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isTagFound)
			}
		})
	}
}

func TestDbTagsStruct_FindByIdSendBack(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name   string
		tagId  string
		status bool
	}{
		{
			name:   "tag_exist",
			tagId:  "5d89fe0cd918e31bfa32498e",
			status: true,
		},
		{
			name:   "tag_not_exist",
			tagId:  "5cf27b0cd918e33dc31902eC",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tag, err := DaoTag.FindByIdSendBack(tc.tagId)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", tag.Id.Hex())
			}
			if tc.status == true && tag.Id.Hex() != tc.tagId {
				t.Fatalf("ERROR: expected tagId %v  _  got %v", tc.tagId, tag.Id.Hex())
			}
		})
	}
}

func TestDbTagsStruct_FindByName(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name    string
		tagName string
		status  bool
		nbTags  int
	}{
		{
			name:    "tag_exist",
			tagName: "sport",
			status:  true,
		},
		{
			name:    "tag_not_exist",
			tagName: "FAIL",
			status:  false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isTagFound := DaoTag.FindByName(tc.tagName)
			if isTagFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isTagFound)
			}
		})
	}
}

func TestDbTagsStruct_FindByTagnameSendBackTag(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name    string
		tagName string
		status  bool
	}{
		{
			name:    "tag_exist",
			tagName: "sport",
			status:  true,
		},
		{
			name:    "tag_not_exist",
			tagName: "FAIL",
			status:  false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			tag, err := DaoTag.FindByNameSendBack(tc.tagName)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", tag.Name)
			}
			if tc.status == true && tag.Name != tc.tagName {
				t.Fatalf("ERROR: expected userId %v  _  got %v", tc.tagName, tag.Name)
			}
		})
	}
}

func TestDbTagsStruct_Insert(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name      string
		tagInsert string
		tagName   string
	}{
		{
			name:      "Insert Tag in Database",
			tagInsert: "{\"name\":\"insertTest\"}",
			tagName:   "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var tag model.Tag
			parseErr := json.Unmarshal([]byte(tc.tagInsert), &tag)
			tag.Id = bson.NewObjectId()
			insertErr := DaoTag.Insert(tag)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoTag.Delete(tag)

			if !DaoTag.FindByName(tc.tagName) {
				t.Fatalf("ERROR: tag %v not found", tc.tagName)
			}
		})
	}
}

func TestDbTagsStruct_Delete(t *testing.T) {
	DaoTag.Connect()
	tt := []struct {
		name    string
		tag     string
		tagName string
	}{
		{
			name:    "Delete Tag in Database",
			tag:     "{\"name\":\"insertTest\"}",
			tagName: "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var tag model.Tag
			parseErr := json.Unmarshal([]byte(tc.tag), &tag)
			tag.Id = bson.NewObjectId()
			insertErr := DaoTag.Insert(tag)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoTag.Delete(tag)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoTag.FindByName(tc.tagName) {
				t.Fatalf("ERROR: tag %v was found and not deleted", tc.tagName)
			}
		})
	}
}

func TestDbTagsStruct_Update(t *testing.T) {
	DaoTag.Connect()

	tt := []struct {
		name           string
		user           string
		tagName        string
		updatedTagName string
	}{
		{
			name:           "Update Tag in DataBase",
			user:           "{\"name\":\"test\"}",
			tagName:        "insertTest",
			updatedTagName: "updatedTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var tag model.Tag
			parseErr := json.Unmarshal([]byte(tc.user), &tag)
			tag.Id = bson.NewObjectId()
			insertErr := DaoTag.Insert(tag)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			parseUpdatedErr := json.Unmarshal([]byte(tc.user), &tag)
			if parseUpdatedErr != nil {
				t.Fatalf(parseUpdatedErr.Error())
			}

			tag.Name = tc.updatedTagName
			err := DaoTag.Update(tag)
			if err != nil {
				t.Fatalf(err.Error())
			}
			defer DaoTag.Delete(tag)

			if DaoTag.FindByName(tc.tagName) {
				t.Fatalf("ERROR: tag %v was no updated", tc.tagName)
			}
			if !DaoTag.FindByName(tc.updatedTagName) {
				t.Fatalf("ERROR: tag %v not found", tc.updatedTagName)
			}
		})
	}
}
