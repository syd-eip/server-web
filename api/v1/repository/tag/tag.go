// This package represents all the functions to handle the database, the collection tags and the connection to them.
package tag

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/syd-eip/server-web/api/v1/model"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and tags collection.
type DbTagsStruct struct{}

// Represents the connection session to the tags collection.
var dbTags *mgo.Database

// Represents the collection tags' name.
var collectionTags = os.Getenv(constdb.CollectionTags)

// Description :
//		Connects to the "syd" database.
func (m *DbTagsStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(mongoTimeout) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbTags = session.DB(mongocred.SydDb)
}

// Description :
//		Find all tags in the tags collection.
// Returns :
//		[]model.Tag :
//			Array of tags returned if one or more tag(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) FindAll() ([]model.Tag, error) {
	var tags []model.Tag
	err := dbTags.C(collectionTags).Find(bson.M{}).All(&tags)
	return tags, err
}

// Description :
//		Find a tag thanks to its "id" in the tags collection.
// Parameters :
//		id string :
//			Id of the tag to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the tag is found.
//			- false if not.
func (m *DbTagsStruct) FindById(id string) bool {
	var tag model.Tag

	if !bson.IsObjectIdHex(id) {
		return false
	}
	err := dbTags.C(collectionTags).FindId(bson.ObjectIdHex(id)).One(&tag)
	if err == nil || tag.Name != "" {
		return true
	}
	return false
}

// Description :
//		Find a tag thanks to its "id" in the tags collection and return it.
// Parameters :
//		id string :
//			Id of the tag to find.
// Returns :
//		model.Tag :
//			Tag returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) FindByIdSendBack(id string) (model.Tag, error) {
	var tagFound model.Tag
	if !bson.IsObjectIdHex(id) {
		err := errors.New("invalid input to ObjectIdHex: " + id)
		return tagFound, err
	}
	err := dbTags.C(collectionTags).FindId(bson.ObjectIdHex(id)).One(&tagFound)
	return tagFound, err
}

// Description :
//		Find a tag thanks to his "name" in the tags collection.
// Parameters :
//		name string :
//			name of the tag to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the tag is found.
//			- false if not.
func (m *DbTagsStruct) FindByName(tagName string) bool {
	var tagFound model.Tag
	err := dbTags.C(collectionTags).Find(bson.M{"name": tagName}).One(&tagFound)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find all tags names in the tags collection.
// Parameters :
//		name string :
//			name of the tag to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the tag is found.
//			- false if not.
func (m *DbTagsStruct) FindAllNames() ([]string, error) {
	var names []string

	err := dbTags.C(collectionTags).Find(nil).Distinct("name", &names)
	return names, err
}

// Description :
//		Find a tag thanks to his "name" in the tags collection and return it.
// Parameters :
//		Name string :
//			Name of the tag to find.
// Returns :
//		model.Tag :
//			Tag returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) FindByNameSendBack(tagName string) (model.Tag, error) {
	var tag model.Tag
	err := dbTags.C(collectionTags).Find(bson.M{"name": tagName}).One(&tag)
	return tag, err
}

// Description :
//		Add an event in the counter for statistics.
// Parameters :
//		Name string :
//			Name of the tag to find.
// Returns :
//		model.Tag :
//			Tag returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) AddEvent(tagName string) bool {
	var tagFound model.Tag
	err := dbTags.C(collectionTags).Find(bson.M{"name": tagName}).One(&tagFound)

	if err != nil {
		return false
	}
	err = dbTags.C(collectionTags).Update(bson.D{{"name", tagFound.Name}},
		bson.D{{"$set", bson.D{{"totalEvents", tagFound.TotalEvent + 1},
			{"actualEvents", tagFound.ActualEvent + 1}}},
		},
	)
	return err != nil
}

// Description :
//		Delete an event in the counter for statistics.
// Parameters :
//		Name string :
//			Name of the tag to find.
// Returns :
//		model.Tag :
//			Tag returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) DeleteEvent(tagName string) bool {
	var tagFound model.Tag
	err := dbTags.C(collectionTags).Find(bson.M{"name": tagName}).One(&tagFound)

	if err != nil {
		return false
	}
	err = dbTags.C(collectionTags).Update(bson.D{{"name", tagFound.Name}},
		bson.D{{"$set", bson.D{{"totalEvents", tagFound.TotalEvent - 1},
			{"actualEvents", tagFound.ActualEvent - 1}}},
		},
	)
	return err != nil
}

// Description :
//		Insert an element in the tags collection in the "syd" database.
// Parameters :
//		tag model.Tag :
//			Tag to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) Insert(tag model.Tag) error {
	tag.CreationDate = time.Now().Format(time.RFC3339)
	tag.UpdateDate = tag.CreationDate

	err := dbTags.C(collectionTags).Insert(&tag)
	return err
}

// Description :
//		Delete an element from the tags collection in the "syd" database.
// Parameters :
//		tag model.Tag :
//			Tag to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) Delete(tag model.Tag) error {
	err := dbTags.C(collectionTags).Remove(bson.M{"_id": tag.Id})
	return err
}

// Description :
//		Update an element in the tags collection in the "syd" database.
// Parameters :
//		tag model.Tag :
//			Tag to update in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbTagsStruct) Update(tag model.Tag) error {
	tag.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbTags.C(collectionTags).UpdateId(tag.Id, &tag)
	return err
}
