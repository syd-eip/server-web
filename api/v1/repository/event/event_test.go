package event

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gopkg.in/mgo.v2/bson"
	"testing"
)

var DaoEvent = DbEventsStruct{}

func TestDbEventsStruct_FindAll(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name     string
		nbEvents int
	}{
		{
			name:     "Find events by default in database",
			nbEvents: 6,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			eventFound, findErr := DaoEvent.FindAll()
			if findErr != nil {
				t.Fatalf(findErr.Error())
			}
			if len(eventFound) != tc.nbEvents {
				t.Fatalf("ERROR: expected %v events in event database  _  got %v", tc.nbEvents, len(eventFound))
			}
		})
	}
}

func TestDbEventsStruct_FindAllByUserId(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name     string
		userId   string
		status   bool
		nbEvents int
	}{
		{
			name:     "user_exist",
			userId:   "5cef9f62c205a965e9693448",
			nbEvents: 6,
			status:   true,
		},
		{
			name:     "user_not_exist",
			userId:   "5cf27b0cd918e33dc31902eA",
			nbEvents: 0,
			status:   false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			events, err := DaoEvent.FindAllByUserId(tc.userId)
			if err != nil {
				t.Fatalf("ERROR: unexpected error %v", err.Error())
			}
			if len(events) != tc.nbEvents {
				t.Fatalf("ERROR: expected %v event from this user in database  _  got %v", tc.nbEvents, len(events))
			}
		})
	}
}

func TestDbEventsStruct_FindById(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name     string
		eventId  string
		status   bool
		nbEvents int
	}{
		{
			name:    "event_exist",
			eventId: "5d8f1753d918e34d6f23ba87",
			status:  true,
		},
		{
			name:    "event_not_exist",
			eventId: "5cf27b0cd918e33dc31902eA",
			status:  false,
		},
		{
			name:    "not an ObjectIdhex",
			eventId: "FAIL",
			status:  false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isEventFound := DaoEvent.FindById(tc.eventId)
			if isEventFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isEventFound)
			}
		})
	}
}

func TestDbEventsStruct_FindByIdSendBack(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name    string
		eventId string
		status  bool
	}{
		{
			name:    "event_exist",
			eventId: "5d8f1753d918e34d6f23ba87",
			status:  true,
		},
		{
			name:    "event_not_exist",
			eventId: "5cf27b0cd918e33dc31902eC",
			status:  false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			event, err := DaoEvent.FindByIdSendBack(tc.eventId)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", event.Id.Hex())
			}
			if tc.status == true && event.Id.Hex() != tc.eventId {
				t.Fatalf("ERROR: expected eventID %v  _  got %v", tc.eventId, event.Id.Hex())
			}
		})
	}
}

func TestDbEventsStruct_FindByName(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name      string
		eventName string
		status    bool
		nbEvents  int
	}{
		{
			name:      "event_exist",
			eventName: "test",
			status:    true,
		},
		{
			name:      "event_not_exist",
			eventName: "FAIL",
			status:    false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			isEventFound := DaoEvent.FindByName(tc.eventName)
			if isEventFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isEventFound)
			}
		})
	}
}

func TestDbEventsStruct_FindByEventnameSendBackEvent(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name      string
		eventName string
		status    bool
	}{
		{
			name:      "event_exist",
			eventName: "test",
			status:    true,
		},
		{
			name:      "event_not_exist",
			eventName: "FAIL",
			status:    false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			event, err := DaoEvent.FindByNameSendBack(tc.eventName)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", event.Information.Name)
			}
			if tc.status == true && event.Information.Name != tc.eventName {
				t.Fatalf("ERROR: expected event name %v  _  got %v", tc.eventName, event.Information.Name)
			}
		})
	}
}

func TestDbEventsStruct_Insert(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name      string
		event     string
		eventName string
	}{
		{
			name:      "Insert Event in Database",
			event:     "{\"information\":{\"name\":\"insertTest\"}}",
			eventName: "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var event model.Event
			parseErr := json.Unmarshal([]byte(tc.event), &event)
			event.Id = bson.NewObjectId()
			event.UserId = bson.NewObjectId()
			event.AssociationId = bson.NewObjectId()
			insertErr := DaoEvent.Insert(event)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoEvent.Delete(event)

			if !DaoEvent.FindByName(tc.eventName) {
				t.Fatalf("ERROR: event %v not found", tc.eventName)

			}
		})
	}
}

func TestDbEventsStruct_Delete(t *testing.T) {
	DaoEvent.Connect()
	tt := []struct {
		name      string
		event     string
		eventName string
	}{
		{
			name:      "Delete Event in Database",
			event:     "{\"information\":{\"name\":\"insertTest\"}}",
			eventName: "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var event model.Event
			parseErr := json.Unmarshal([]byte(tc.event), &event)
			event.Id = bson.NewObjectId()
			event.UserId = bson.NewObjectId()
			event.AssociationId = bson.NewObjectId()
			insertErr := DaoEvent.Insert(event)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoEvent.Delete(event)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoEvent.FindByName(tc.eventName) {
				t.Fatalf("ERROR: event %v was found and not deleted", tc.eventName)
			}
		})
	}
}

func TestDbEventsStruct_Update(t *testing.T) {
	DaoEvent.Connect()

	tt := []struct {
		name             string
		event            string
		eventName        string
		updatedEventName string
	}{
		{
			name:             "Update Event in DataBase",
			event:            "{\"name\":\"Test\"}",
			eventName:        "insertTest",
			updatedEventName: "updatedTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var event model.Event
			parseErr := json.Unmarshal([]byte(tc.event), &event)
			event.Id = bson.NewObjectId()
			event.UserId = bson.NewObjectId()
			event.AssociationId = bson.NewObjectId()
			insertErr := DaoEvent.Insert(event)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			parseUpdatedErr := json.Unmarshal([]byte(tc.event), &event)
			if parseUpdatedErr != nil {
				t.Fatalf(parseUpdatedErr.Error())
			}

			event.Information.Name = tc.updatedEventName
			err := DaoEvent.Update(event)
			if err != nil {
				t.Fatalf(err.Error())
			}
			defer DaoEvent.Delete(event)

			if DaoEvent.FindByName(tc.eventName) {
				t.Fatalf("ERROR: event %v was no updated", tc.eventName)
			}
			if !DaoEvent.FindByName(tc.updatedEventName) {
				t.Fatalf("ERROR: event %v not found", tc.updatedEventName)
			}
		})
	}
}
