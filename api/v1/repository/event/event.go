// This package represents all the functions to handle the database, the collection events and the connection to them.
package event

import (
	"errors"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/ahmetb/go-linq"
	"gitlab.com/syd-eip/server-web/api/v1/model"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and events collection.
type DbEventsStruct struct{}

// Represents the connection session to the events collection.
var dbEvents *mgo.Database

// Represents the collection events' name.
var collectionEvents = os.Getenv(constdb.CollectionEvents)

// Description :
//		Connects to the "syd" database.
func (m *DbEventsStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(mongoTimeout) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbEvents = session.DB(mongocred.SydDb)
}

// Description :
//		Find all events in the events collection.
// Returns :
//		[]model.Event :
//			Array of events returned if one or more event(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindAll() ([]model.Event, error) {
	var events []model.Event
	err := dbEvents.C(collectionEvents).Find(bson.M{}).All(&events)
	return events, err
}

// Description :
//		Find all events created by specific user in the events collection thanks the userId.
// Parameters :
//		userId bson.ObjectId :
//			Id of the user.
// Returns :
//		[]model.Event :
//			Array of events returned if one or more event(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindAllByUserId(userId string) ([]model.Event, error) {
	var events []model.Event

	if !bson.IsObjectIdHex(userId) {
		err := errors.New("invalid input to ObjectIdHex: " + userId)
		return events, err
	}

	err := dbEvents.C(collectionEvents).Find(bson.M{"userId": bson.ObjectIdHex(userId)}).All(&events)
	return events, err
}

// Description :
//		Find all events if they belong to this tag.
// Parameters :
//		tagName string :
//			Name of the tag.
// Returns :
//		[]model.Event :
//			Array of events returned if one or more event(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindAllByTag(tagName string) ([]model.Event, error) {
	var events []model.Event

	err := dbEvents.C(collectionEvents).Find(bson.M{"information.tags": tagName}).All(&events)

	return events, err
}

// Description :
//		Find all events if they belong to all those tags.
// Parameters :
//		tagNames []string :
//			Names of the tags.
// Returns :
//		[]model.Event :
//			Array of events returned if one or more event(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindAllByTags(tagNames []string) ([]model.Event, error) {
	var events []model.Event

	var AndQuery []map[string]interface{}
	for i := 0; i < len(tagNames); i++ {
		currentCondition := bson.M{"information.tags": tagNames[i]}
		AndQuery = append(AndQuery, currentCondition)
	}

	err := dbEvents.C(collectionEvents).Find(bson.M{"$and": AndQuery}).All(&events)

	if len(events) == 0 {
		err = errors.New("not found")
	}
	return events, err
}

// Description :
//		Find events that may please the authenticated user the most.
// Parameters :
//		user model.User :
//			User used to make the research.
// Returns :
//		[]model.Event :
//			Array of events returned if one or more event(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindDiscoverEvents(user model.User) ([]model.Event, error) {
	var events []model.Event

	var OrQuery []map[string]interface{}
	for i := 0; i < len(user.Favorites.Tags); i++ {
		currentCondition := bson.M{"information.tags": user.Favorites.Tags[i]}
		OrQuery = append(OrQuery, currentCondition)
	}

	err := dbEvents.C(collectionEvents).Find(bson.M{"$or": OrQuery}).Sort("information.name").All(&events)

	if len(events) == 0 {
		err = errors.New("not found")
	}

	linq.From(events).SortT(func(event1 model.Event, event2 model.Event) bool {
		var rs1 []string
		linq.From(event1.Information.Tags).IntersectByT(linq.From(user.Favorites.Tags), func(str string) string {
			return str
		}).ToSlice(&rs1)
		var rs2 []string
		linq.From(event2.Information.Tags).IntersectByT(linq.From(user.Favorites.Tags), func(str string) string {
			return str
		}).ToSlice(&rs2)
		return len(rs1) > len(rs2)
	}).ToSlice(&events)

	return events, err
}

// Description :
//		Find a event thanks to his "id" in the events collection.
// Parameters :
//		id string :
//			Id of the event to findId.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the event is found.
//			- false if not.
func (m *DbEventsStruct) FindById(id string) bool {
	var event model.Event

	if !bson.IsObjectIdHex(id) {
		return false
	}

	err := dbEvents.C(collectionEvents).FindId(bson.ObjectIdHex(id)).One(&event)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a event thanks to his "id" in the events collection.
// Parameters :
//		id string :
//			Id of the event to findId.
// Returns :
//		model.Event :
//			Event returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindByIdSendBack(id string) (model.Event, error) {
	var event model.Event

	if !bson.IsObjectIdHex(id) {
		err := errors.New("invalid input to ObjectIdHex: " + id)
		return event, err
	}
	err := dbEvents.C(collectionEvents).FindId(bson.ObjectIdHex(id)).One(&event)
	return event, err
}

// Description :
//		Find a event thanks to his "name" in the events collection.
// Parameters :
//		name string :
//			name of the event to findId.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the event is found.
//			- false if not.
func (m *DbEventsStruct) FindByName(eventName string) bool {
	var event model.Event
	err := dbEvents.C(collectionEvents).Find(bson.M{"information.name": eventName}).One(&event)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a event thanks to his "name" in the events collection.
// Parameters :
//		Name string :
//			Name of the event to findId.
// Returns :
//		model.Event :
//			Event returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) FindByNameSendBack(eventName string) (model.Event, error) {
	var event model.Event
	err := dbEvents.C(collectionEvents).Find(bson.M{"information.name": eventName}).One(&event)
	return event, err
}

// Description :
//		Insert an element in the events collection in the "syd" database.
// Parameters :
//		event model.Event :
//			Event to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) Insert(event model.Event) error {
	event.CreationDate = time.Now().Format(time.RFC3339)

	event.UpdateDate = event.CreationDate
	err := dbEvents.C(collectionEvents).Insert(&event)
	return err
}

// Description :
//		Delete an element from the events collection in the "syd" database.
// Parameters :
//		event model.Event :
//			Event to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) Delete(event model.Event) error {
	err := dbEvents.C(collectionEvents).Remove(bson.M{"_id": event.Id})
	return err
}

// Description :
//		Update an element in the events collection in the "syd" database.
// Parameters :
//		event model.Event :
//			Event to update in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbEventsStruct) Update(event model.Event) error {
	event.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbEvents.C(collectionEvents).UpdateId(event.Id, event)
	return err
}
