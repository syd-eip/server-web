// This package provides all the functions and methods to interact with the syd database.
package repository

import (
	"os"

	constantsDb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
)

// Represents the SYD's MongoDB database address
var MongoAddr = os.Getenv(constantsDb.MongoAddr)

// Represents the SYD's MongoDB database name
var SydDb = os.Getenv(constantsDb.SydDatabase)
