package jwtToken

import (
	"encoding/json"
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gopkg.in/mgo.v2/bson"
	"testing"
)

var DaoJwtToken = DbJwtTokensStruct{}

func TestDbJwtTokensStruct_FindAll(t *testing.T) {
	DaoJwtToken.Connect()

	tt := []struct {
		name        string
		nbJwtTokens int
	}{
		{
			name:        "Find jwtTokens by default in database",
			nbJwtTokens: 2,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			jwtTokensFound, findErr := DaoJwtToken.FindAll()
			if findErr != nil {
				t.Fatalf(findErr.Error())
			}

			if len(jwtTokensFound) != tc.nbJwtTokens {
				t.Fatalf("ERROR: expected %v jwtTokens in jwtTokens database  _  got %v", tc.nbJwtTokens, len(jwtTokensFound))
			}
		})
	}
}

func TestDbJwtTokensStruct_FindByToken(t *testing.T) {
	DaoJwtToken.Connect()

	tt := []struct {
		token         string
		jwtTokenToken string
		status        bool
		nbJwtTokens   int
	}{
		{
			token:         "jwtToken_exist",
			jwtTokenToken: "Test",
			status:        true,
		},
		{
			token:         "jwtToken_not_exist",
			jwtTokenToken: "FAIL",
			status:        false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.token, func(t *testing.T) {
			isJwtTokenFound := DaoJwtToken.FindByToken(tc.jwtTokenToken)
			if isJwtTokenFound != tc.status {
				t.Fatalf("ERROR: expected status %v   _  got %v", tc.status, isJwtTokenFound)
			}
		})
	}
}

func TestDbJwtTokensStruct_FindByTokenSendBackJwtToken(t *testing.T) {
	DaoJwtToken.Connect()

	tt := []struct {
		name   string
		token  string
		status bool
	}{
		{
			name:   "jwtToken_exist",
			token:  "Test",
			status: true,
		},
		{
			name:   "jwtToken_not_exist",
			token:  "FAIL",
			status: false,
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			jwtToken, err := DaoJwtToken.FindByTokenSendBack(tc.token)
			if err != nil && tc.status == true {
				t.Fatalf("ERROR: error occured : %v", err.Error())
			}
			if err == nil && tc.status == false {
				t.Fatalf("ERROR: test was not supposed to found name : %v", jwtToken.Token)
			}
			if tc.status == true && jwtToken.Token != tc.token {
				t.Fatalf("ERROR: expected jwtTokenId %v  _  got %v", tc.token, jwtToken.Token)
			}
		})
	}
}

func TestDbJwtTokensStruct_Insert(t *testing.T) {
	DaoJwtToken.Connect()

	tt := []struct {
		name     string
		jwtToken string
		token    string
	}{
		{
			name:     "Insert JwtToken in Database",
			jwtToken: "{\"name\":\"insertTest\"}",
			token:    "insertTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var jwtToken model.JwtToken
			parseErr := json.Unmarshal([]byte(tc.jwtToken), &jwtToken)
			jwtToken.Id = bson.NewObjectId()
			jwtToken.Token = tc.token
			insertErr := DaoJwtToken.Insert(jwtToken)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}
			defer DaoJwtToken.Delete(jwtToken)

			if !DaoJwtToken.FindByToken(tc.token) {
				t.Fatalf("ERROR: jwtToken %v not found", tc.token)
			}
		})
	}
}

func TestDbJwtTokensStruct_Delete(t *testing.T) {
	DaoJwtToken.Connect()
	tt := []struct {
		name     string
		jwtToken string
		token    string
	}{
		{
			name:     "Delete JwtToken in Database",
			jwtToken: "{\"name\":\"deleteTest\"}",
			token:    "deleteTest",
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			var jwtToken model.JwtToken
			parseErr := json.Unmarshal([]byte(tc.jwtToken), &jwtToken)
			jwtToken.Id = bson.NewObjectId()
			jwtToken.Token = tc.token
			insertErr := DaoJwtToken.Insert(jwtToken)
			if insertErr != nil {
				t.Fatalf(insertErr.Error())
			} else if parseErr != nil {
				t.Fatalf(parseErr.Error())
			}

			err := DaoJwtToken.Delete(jwtToken)
			if err != nil {
				t.Fatalf(err.Error())
			} else if DaoJwtToken.FindByToken(tc.token) {
				t.Fatalf("ERROR: jwtToken %v was found and not deleted", tc.token)
			}
		})
	}
}
