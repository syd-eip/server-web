// This package represents all the functions to handle the database, the collection jwtTokens and the connection to them.
package jwtToken

import (
	"gitlab.com/syd-eip/server-web/api/v1/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
	"strconv"
	"time"

	constdb "gitlab.com/syd-eip/server-web/api/v1/constant/database"
	mongocred "gitlab.com/syd-eip/server-web/api/v1/repository"
)

// Represents the structure for the connection to the "syd" database and jwtTokens collection.
type DbJwtTokensStruct struct{}

// Represents the connection session to the jwtTokens collection.
var dbJwtTokens *mgo.Database

// Represents the collection jwtTokens' name.
var collectionJwtTokens = os.Getenv(constdb.CollectionJwtTokens)

// Description :
//		Connects to the "syd" database.
func (m *DbJwtTokensStruct) Connect() {
	mongoTimeout, err := strconv.Atoi(os.Getenv(constdb.TimeoutDatabase))
	if err != nil {
		log.Fatal(err)
	}
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:    []string{mongocred.MongoAddr},
		Timeout:  time.Duration(mongoTimeout) * time.Second,
		Database: os.Getenv(constdb.SydDatabase),
		Username: os.Getenv(constdb.SuperAdminUsernameSydDatabase),
		Password: os.Getenv(constdb.SuperAdminPasswordSydDatabase),
	}
	session, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		log.Fatal(err)
	}
	dbJwtTokens = session.DB(mongocred.SydDb)
}

// Description :
//		Find all jwtTokens in the jwtTokens collection.
// Returns :
//		[]model.JwtToken :
//			Array of jwtTokens returned if one or more jwtToken(s) is/are found.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbJwtTokensStruct) FindAll() ([]model.JwtToken, error) {
	var jwtTokens []model.JwtToken
	err := dbJwtTokens.C(collectionJwtTokens).Find(bson.M{}).All(&jwtTokens)
	return jwtTokens, err
}

// Description :
//		Find a jwtToken thanks to his "name" in the jwtTokens collection.
// Parameters :
//		name string :
//			name of the jwtToken to find.
// Returns :
//		bool :
//			This boolean indicates the status of the verification :
//			- true if the jwtToken is found.
//			- false if not.
func (m *DbJwtTokensStruct) FindByToken(token string) bool {
	var jwtToken model.JwtToken
	err := dbJwtTokens.C(collectionJwtTokens).Find(bson.M{"token": token}).One(&jwtToken)
	if err == nil {
		return true
	}
	return false
}

// Description :
//		Find a jwtToken thanks to his "name" in the jwtTokens collection and return it.
// Parameters :
//		Token string :
//			Token of the jwtToken to find.
// Returns :
//		model.JwtToken :
//			JwtToken returned if found. Null if not.
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbJwtTokensStruct) FindByTokenSendBack(token string) (model.JwtToken, error) {
	var jwtToken model.JwtToken
	err := dbJwtTokens.C(collectionJwtTokens).Find(bson.M{"token": token}).One(&jwtToken)
	return jwtToken, err
}

// Description :
//		Insert an element in the jwtTokens collection in the "syd" database.
// Parameters :
//		jwtToken model.JwtToken :
//			JwtToken to insert in the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbJwtTokensStruct) Insert(jwtToken model.JwtToken) error {
	jwtToken.CreationDate = time.Now().Format(time.RFC3339)
	jwtToken.UpdateDate = jwtToken.CreationDate

	err := dbJwtTokens.C(collectionJwtTokens).Insert(&jwtToken)
	return err
}

// Description :
//		Delete an element from the jwtTokens collection in the "syd" database.
// Parameters :
//		jwtToken model.JwtToken :
//			JwtToken to delete from the collection.
// Returns :
//		error :
//			Error throws in case of a fail from the research in the database.
func (m *DbJwtTokensStruct) Delete(jwtToken model.JwtToken) error {
	jwtToken.UpdateDate = time.Now().Format(time.RFC3339)

	err := dbJwtTokens.C(collectionJwtTokens).Remove(bson.M{"token": jwtToken.Token})
	return err
}
