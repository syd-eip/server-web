﻿# SYD API for Developpers

## Install GO

##### Download Go

First, follow the **[official go downloads page](https://golang.org/dl/)** and find the URL for the current binary release's tarball.
Then, run this command:

```sh
curl -O https://dl.google.com/go/go1.10.2.linux-amd64.tar.gz
```
Of course, the link will change if you're not downloading the same version.

##### Check Go tarball corruption

Once you've download Go, it's better to verify if your download is not corrupted.
To check that, use this command:

```sh
sha256sum go1.10*.tar.gz
```

Then, check the output with the **SHA256 Checksum** value on the **[official go downloads page](https://golang.org/dl/).**
If it's the same, then you're good to continue !
(If not, try to download it again or search on Google !)

##### Extract Go tarball

```sh
tar xvf go1.10.2.linux-amd64.tar.gz
```

##### Change Go's owner and group to root

```sh
sudo chown -R root:root ./go
```

##### Move Go to its final location

```sh
sudo mv go /usr/local
```

***NB: Although /usr/local/go is the officially-recommended location, some users may prefer or require different paths.***

##### Set Go Path

**Tell Go where to look for its files :**
Open profile file:

```sh
nano ~/.profile
```

**If Go is in its official location (/usr/local/go), add these lines at the end of this file:**

```sh
...
...
export GOPATH=$HOME/work
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

**If Go is in a custom location, add these lines at the end of this file:**

```sh
...
...
export GOROOT=$HOME/go
export GOPATH=$HOME/work
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
```

Next, don't forget to refresh your profile:

```sh
source ~/.profile
```

###### **THAT'S IT ! YOU'RE GOOD TO GO !... hum sorry... TO CODE !**

## Download SYD API project

Go to your Go path, then run these commands:

```sh
mkdir src/gitlab.com/syd-eip
git clone git@gitlab.com:syd-eip/server-web.git
```

Then, install all the dependencies:

```sh
cd src/gitlab.com/syd-eip/server-web/api
go get
```

## Export SYD API environments variables:

In your *.bashrc*, or in your *.zshrc*, or whatever shell you use : export all the variables necessary for the SYD API to work.

Variables to export:

```sh
### BEGIN SYD ENV VARIABLES ###

#ENVIRONMENT
DEV="true" # Defines if you're in development environment (true) or in production (false).

#API PORT SERVER
SERVER_PORT=":7070" # Defines if the server port.

#SSL VARIABLES
SSL_STATUS="false" # Defines if the API uses https (true) or http (false)
SSL_CERT="./path/to/ssl_cert.crt" # Defines the path of the SSL.crt certificate: useful if the SSL_STATUS is true.
SSL_KEY="./path/to/ssl_key.key" # Defines the path of the SSL.key certificate: useful if SSL_STATUS is true.

#SIGNING KEY
SIGNING_KEY="SigningKey" # Defines the signing key used to generate the user token.

#COOKIE STATUS
COOKIE_HTTP_VALUE_TOKEN_USER="true" # Defines if the cookie for the user's token is in HTTP ONLY (true).
COOKIE_SECURE_VALUE_TOKEN_USER="false" # Defines if the cookie for the user's token is secure for https (true) or http (false).

#DEFAULT ROLE USER
USER_DEFAULT_ROLE="GUEST" # Defines the default user's role during the sign up.

#MONGO DATABASE CONFIGURATION
MONGO_ADDR="localhost:27017" # Defines the MongoDB database port.
SYD_DB="syd" # Defines the name of the SYD database.
TIMEOUT_DB="30" # Defines the MongoDB timeout.
SUPER_ADMIN_USERNAME_SYD_DB="SuperAdminUsername" # Defines the username of the Super Admin of the SYD database.
SUPER_ADMIN_PASSWORD_SYD_DB="SuperAdminPassword" # Defines the password of the Super Admin of the SYD database.
COLLECTION_USERS="users" # Defines the name of the users collection.
COLLECTION_EVENTS="events" # Defines the name of the events collection.
COLLECTION_TAGS="tags" # Defines the name of the tags collection.
COLLECTION_ASSOCIATIONS="associations" # Defines the name of the associations collection.
COLLECTION_IMAGES="images" # Defines the name of the images collection.
COLLECTION_JWT_TOKENS=jwtTokens # Defines the name of the JWT tokens collection.

#STATIC FOLDER PATH
STATIC_FOLDER_PATH="./path/to/static/folder" # Defines path to the folder containing the static files.
DOCUMENTATION_FOLDER_PATH="./path/to/documentation/folder" # Defines path to the folder containing the documentation.

#SUPPORT MAIL CREDENTIALS
SYD_MAIL_ADDRESS="sydassistance@gmail.com"
SYD_MAIL_PASSWORD="password"

### END SYD ENV VARIABLES ###
```

## Launch SYD API

Navigate to the root of the SYD project, and run the following command:
```sh
go run api/syd_api.go
```

## Unit Tests
For every function you create, don't forget to write some unit tests.
Unit tests determine whether some **individual units of source code are fit to use**. They can be sets of one or more modules with associated control data, usage procedures and operating procedures.
If you write unit tests for every feature you implement, you reduce the probaility of breaking some other features.

To do that, simply create a file named with ***_test*** at the end. Here an example :

Original file :
**user_birthday.go**

Test file :
**user_birthday_test.go**

In this file, import the ***testing*** package, then write your unit tests like so :
```sh
package name_of_the_package
  
import "testing"

func TestNameOfTheFunctionToTest(t *testing.T) {
	//Code necessary to test the function
}
```
Of course, you can and must (if it's possible) write multiple tests for every feature.

## Documentation

#### Write Documentation
On each package, write the documentation of the package just above its name:
```sh
// Description of the package
package name_of_the_package
```

For every function, write documentation like this:
```sh
// Description :
//		Description of the function.
// Parameters :
//		parameter1 :
//			Description of the parameter1.
//		parameter2 :
//			Description of the parameter2.
// Returns :
//		returned_variable1 :
//			This is a very long description of the returned variable number 1,
//			you can break a line to make a longer description.
//		returned_variable2 :
//			This is a short description.
```

#### Look at the documentations
To look at the API documentation :
- **Directly generate the documentation**:
	- Run this command:
		```sh
		godoc -http=localhost:6060
		```
	- Then, follow this link: ***http://localhost:6060***.

To look at the routes documentation :<br>
- Install ***pretty-swag*** : ***https://github.com/twskj/pretty-swag?fbclid=IwAR2QJVSmXlCpYi9LVJSJkEZFcvunTvwOPGgejOl8j7edhvgpZqnQDfDWSok***
- Go in the "documentation" folder, and run this command :<br>
		```
 		pretty-swag -i documentation.json -o documentation.html
 		```<br>
 		This command will generate a **documentation.html** file, based on the **documentation.json** file located in the "documentation" folder.<br>
 		The **documentation.json** file is generated from ***swagger***, where we write our documentation.
- Launch the API like explained in the step : **Launch SYD API**.
- Sign in with a **SUPER_ADMIN** account => ***POST http://localhost:7070/v1/user/sign/in***.
- Then, navigate to : ***http://localhost:7070/v1/documentation/documentation.html***.

## Create Git Branch
For every feature you want to implement, fix, revert... in the API, you have to create a branch.
Simply run this command :
```sh
git checkout -b [NAME_OF_THE_BRANCH]
```
The name of the branch has to be very **explicit**.
Here are some examples :
**Implement a feature:**
```sh
git checkout -b feature-user_password_hashing
```
**Fix a feature:**
```sh
git checkout -b fix-user_password_hashing
```
**Revert a feature:**
```sh
git checkout -b revert-user_password_hashing
```
**Release an update:**
```sh
git checkout -b release-1.13
```

## Git commit format

The first line of the commit message must contains the tags depending on the commit's feature.
The tags hierarchy must be ascendant, that means from the most general to the most precise.

Example 1 :
```sh
git commit -m "[API] [CONTROLLER] [USER] [SIGN] [IN] Implement generate token function when user signs in."
```
Example 2 :
```sh
git commit -m "[CI] [GITLAB] Add SSH connection to the server in gitlabci.yml."
```

## Merge request on the dev branch

If you want to merge a branch into dev branch, first make sure you push all your commits on the branch itself.
Next, run the following command :

```sh
git push origin dev
```

This command will prohibit you to push and display a link to make a merge request on your GitLab account.
Follow this link and complete the merge request. This will notify the maintainers of the repository to evaluate your merge request.
He will then accept or refuse your merge request.

## What's happening when you push on dev or master ?
When you push on the dev or master branch and the merge request has been accepted by a maintainer, a series of actions are executed to make the **Continuous Integration (CI)**.

**Through GitlabCI:**
- All the go dependencies are installed on the GitlabCI environment.
- Then, we export all the values necessary for the API to work.
- Next, we import some MongoDB collections (like users or events) on GitlabCI to provide some data to execute the unit tests.
- All the unit tests of the API are then run.
- Finally :
	- If you push on the **dev** branch : everything is deployed on the **devlopment** environment of the server.
	- If you push on the **master** branch : everything is deployed on the **production** environment of the server.

**Deployment explained :**
- We connect with **SSH** connection to the server.
- We **checkout** the right branch *(dev or master)*.
- We **pull** the modifications.
- **Stop** the API container.
- *IF WE ARE IN DEV :* generate the new ***documentation.html*** file with **pretty-swag**.
- **Build** the modified container.
- And finally **run** the container.
*(We also **clear** the old images and containers)*

## Configure Goland IDE by Jetbrains

**Jetbrains** provides an amazing tool to code in Go called **GoLand IDE**.
It's an amazing tool allowing you to code faster and better ! We love it and that's why we talk about it !

If you are on **GoLand IDE** don't forget to set :
- Your **$GOPATH** (File -> Settings -> Go -> GOPATH).
- Your **$GOROOT** (File -> Settings -> Go -> GOROOT).
- Your build configurations. In the top right corner, there is a drop down menu, click on **Edit Configurations**:
- ***Run API configuration***
	- Click on the **+**.
	- Add a **Go Build** Configuration.
	- Set **Run kind** to be : ***File***.
	- Set **Files** to be : ***.../path/to/the/project/syd-eip/server-web/api/syd_api.go***.
	- Check the **Run after build** check box.
	- Set **Working directory** to be : ***.../path/to/the/project/syd-eip/server-web***.
	- Set **Environment**, click on it and add every envrionment variable set in the ***Export SYD API environments variables*** category above.
- ***Run API Tests configuration***
	- Click on the **+**.
	- Add a **Go Test** Configuration.
	- Set **Test framework** to be : ***gotest***.
	- Set **Test kind** to be : ***Directory***.
	- Set **Directory** to be : ***.../path/to/the/project/syd-eip/server-web***.
	- Set **Working directory** to be : ***.../path/to/the/project/syd-eip/server-web***.
	- Set **Go tool arguments** to be : ***-i***.
	- Set **Environment**, click on it and add every envrionment variable set in the ***Export SYD API environments variables*** category above.

***NB : We aren't not sponsored by them, we use our student status, giving us a license to get the Ultimate Edition.***
