variables:
  PACKAGE_PATH: /go/src/gitlab.com/syd-eip/server-web
  SSH_PASS: '$SSH_DEPLOY_KEY'

services:
  - mongo

stages:
  - install-go-dep
  - tests-api
  - deploy-dev
  - deploy-prod

.anchors:
  - &inject-gopath
      mkdir -p $(dirname ${PACKAGE_PATH})
      && ln -s ${CI_PROJECT_DIR} ${PACKAGE_PATH}
      && cd ${PACKAGE_PATH}
      && cd api

install-go-dep:
  stage: install-go-dep
  image: golang:1.13.4-alpine3.10
  before_script:
    - apk add --no-cache curl git
    - curl -sSL https://github.com/golang/dep/releases/download/v0.5.1/dep-linux-amd64 -o /go/bin/dep
    - chmod +x /go/bin/dep
    - *inject-gopath
  script:
    - dep ensure -v -vendor-only
  only:
    - dev
    - master

tests-api:
  stage: tests-api
  dependencies:
    - install-go-dep
  image: golang:1.13.4-alpine3.10
  before_script:
    - *inject-gopath
    - export CGO_ENABLED=0
    - export DEV=true
    - export SERVER_PORT=:7070
    - export SSL_STATUS=false
    - export SIGNING_KEY=gitlabcitests
    - export COOKIE_HTTP_VALUE_TOKEN_USER=true
    - export COOKIE_SECURE_VALUE_TOKEN_USER=true
    - export USER_DEFAULT_ROLE=USER
    - export MONGO_ADDR=mongo:27017
    - export TIMEOUT_DB=30
    - export SUPER_ADMIN_USERNAME_DB=admin
    - export SUPER_ADMIN_PASSWORD_DB=admin
    - export SYD_DB=syd
    - export COLLECTION_USERS=users
    - export COLLECTION_EVENTS=events
    - export COLLECTION_TAGS=tags
    - export COLLECTION_ASSOCIATIONS=associations
    - export COLLECTION_IMAGES=images
    - export COLLECTION_JWT_TOKENS=jwtTokens
  script:
    - echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/main' >> /etc/apk/repositories
    - echo 'http://dl-cdn.alpinelinux.org/alpine/v3.6/community' >> /etc/apk/repositories
    - apk update
    - apk add mongodb mongodb-tools
    - cd ..
    - mongoimport --host mongo:27017 --db syd --collection users --type json --file ./mongo-seed/init_users.json --jsonArray
    - mongoimport --host mongo:27017 --db syd --collection events --type json --file ./mongo-seed/init_events.json --jsonArray
    - mongoimport --host mongo:27017 --db syd --collection tags --type json --file ./mongo-seed/init_tags.json --jsonArray
    - mongoimport --host mongo:27017 --db syd --collection associations --type json --file ./mongo-seed/init_associations.json --jsonArray
    - mongoimport --host mongo:27017 --db syd --collection jwtTokens --type json --file ./mongo-seed/init_jwt_tokens.json --jsonArray
    - cd api
    - go test -v -cover ./...
  only:
    - dev
    - master

deploy-dev:
  stage: deploy-dev
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - mkdir -p ~/.ssh
    - echo "$SSH_DEPLOY_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - eval $(ssh-agent -s)
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
    - ssh-keyscan gitlab.com | sort -u - ~/.ssh/known_hosts -o ~/.ssh/known_hosts
    - rm -rf .git
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh syd@51.77.213.61 "
      cd ./repositories/server-web/ &&
      git checkout dev &&
      git pull &&
      docker-compose -f docker-compose.dev.yml stop &&
      echo y | docker container prune &&
      docker rmi api-dev &&
      echo y | docker image prune &&
      docker-compose -f docker-compose.dev.yml build &&
      docker-compose -f docker-compose.dev.yml up -d &&
      echo y | docker image prune
      "
  only:
    - dev

deploy-prod:
  stage: deploy-prod
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - mkdir -p ~/.ssh
    - echo "$SSH_DEPLOY_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 700 ~/.ssh/id_rsa
    - eval $(ssh-agent -s)
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -H 'gitlab.com' >> ~/.ssh/known_hosts
    - ssh-keyscan gitlab.com | sort -u - ~/.ssh/known_hosts -o ~/.ssh/known_hosts
    - rm -rf .git
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    - ssh syd@51.77.213.61 "
      cd ./repositories/server-web/ &&
      git checkout master &&
      git pull &&
      docker-compose -f docker-compose.prod.yml stop &&
      echo y | docker container prune &&
      docker rmi api-prod &&
      echo y | docker image prune &&
      docker-compose -f docker-compose.prod.yml build &&
      docker-compose -f docker-compose.prod.yml up -d &&
      echo y | docker image prune
      "
  only:
    - master